package com.goodangcode.goodang;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;


/**
 * @author GoodangCode
 */
public class SplashScreenActivityTest
{
    private SplashScreenActivity activity;

    @Before
    public void setUp() throws Exception
    {
        activity = new SplashScreenActivity();
    }

    @Test
    public void onCreate() throws Exception
    {
        assertNotNull(activity);
    }
}