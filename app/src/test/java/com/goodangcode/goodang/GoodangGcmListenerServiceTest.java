package com.goodangcode.goodang;


import android.content.Intent;
import android.os.Bundle;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;
import org.robolectric.shadows.ShadowLog;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.robolectric.Shadows.shadowOf;


/**
 * @author GoodangCode
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class GoodangGcmListenerServiceTest
{
    private GoodangGcmListenerService gcmListenerService;
    @Before
    public void setUp() throws Exception
    {
        ShadowLog.stream = System.out;
        gcmListenerService = Robolectric.setupService(GoodangGcmListenerService.class);
    }

    @Test
    public void getMessageFromServer()
    {
        Bundle data = new Bundle();

        String json = "$message";
        data.putString("message",json);
        gcmListenerService.onMessageReceived("/topics/update", data);
        ShadowApplication shadowApplication = shadowOf(RuntimeEnvironment.application);
        assertNotNull(shadowApplication);
    }

    @Test
    public void registerGCMListener()
    {
        Intent intent = new Intent("com.google.android.c2dm.intent.RECEIVE");
        ShadowApplication application = ShadowApplication.getInstance();
        assertTrue("GCM Listener not registered ",
                application.hasReceiverForIntent(intent));
    }
}
