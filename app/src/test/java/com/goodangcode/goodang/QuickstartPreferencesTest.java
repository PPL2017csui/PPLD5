package com.goodangcode.goodang;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author GoodangCode
 */
public class QuickstartPreferencesTest
{
    private QuickstartPreferences quickstartPreferences;

    @Before
    public void setUp() throws Exception
    {
        quickstartPreferences = new QuickstartPreferences();
    }

    @Test
    public void initialize() throws Exception
    {
        assertEquals("sentTokenToServer", quickstartPreferences.SENT_TOKEN_TO_SERVER);
        assertEquals("registrationComplete", quickstartPreferences.REGISTRATION_COMPLETE);
        assertEquals("registrationError", quickstartPreferences.REGISTRATION_ERROR);
    }
}