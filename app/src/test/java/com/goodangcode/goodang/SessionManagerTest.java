package com.goodangcode.goodang;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.matchers.Null;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import java.util.HashMap;

import static org.junit.Assert.*;
import static org.robolectric.Shadows.shadowOf;

/**
 * Created by GoodangCode on 17/05/2017.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class SessionManagerTest {
    private SessionManager instance;

    private String username;
    private String name;
    private String kode;
    private String isadmin;
    private String kodeGudang;

    @Before
    public void setUp() throws Exception
    {
        instance = SessionManager.getInstance(RuntimeEnvironment.application.getApplicationContext());

        username = "usernameTest";
        name = "nameTest";
        kode = "kodeTest";
        isadmin = "isadminTest";
        kodeGudang = "kodeGudangTest";

    }


    @Test
    public void getInstanceTest() throws Exception {
        assertNotNull(instance);
    }

    @Test
    public void createLoginSessionTest() throws Exception {
        instance.createLoginSession(username, name, kode, isadmin, kodeGudang);
        HashMap<String, String> user = instance.getUserDetails();
        assertEquals(user.get("name"), name);
        assertEquals(user.get("username"), username);
        assertEquals(user.get("kode"), kode);
        assertEquals(user.get("isadmin"), isadmin);
        assertEquals(user.get("kodeGudang"), kodeGudang);
    }

    @Test
    public void getUserDetailsTest() throws Exception {
        assertNotNull(instance.getUserDetails());
    }

    @Test
    public void checkLoginTest() throws Exception {
        instance.checkLogin();
        ShadowApplication shadowApplication = shadowOf(RuntimeEnvironment.application);
        assertNotNull(shadowApplication);
    }

    @Test
    public void logoutUserTest() throws Exception {
        instance.logoutUser();
        ShadowApplication shadowApplication = shadowOf(RuntimeEnvironment.application);
        assertNotNull(shadowApplication);

        HashMap<String, String> user = instance.getUserDetails();
        assertEquals(user.get("name"), null);
        assertEquals(user.get("username"), null);
        assertEquals(user.get("serialCode"), null);
        assertEquals(user.get("isadmin"), null);
        assertEquals(user.get("kodeGudang"), null);
    }

    /*@Test
    public void isLoggedInTest() throws Exception {
        assertFalse(instance.isLoggedIn());
    }
    */

    @Test
    public void isadminTest() throws Exception {
        instance.createLoginSession(username, name, kode, isadmin, kodeGudang);
        assertEquals(instance.isadmin(), isadmin);
    }

    @Test
    public void getNamaPenggunaTest() throws Exception {
        instance.createLoginSession(username, name, kode, isadmin, kodeGudang);
        assertEquals(instance.getNamaPengguna(), name);
    }

    @Test
    public void getGudangCodeTest() throws Exception {
        instance.createLoginSession(username, name, kode, isadmin, kodeGudang);
        assertEquals(instance.getGudangCode(), kodeGudang);
    }

}
