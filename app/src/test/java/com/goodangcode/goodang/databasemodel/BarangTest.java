package com.goodangcode.goodang.databasemodel;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;


/**
 * @author GoodangCode
 */
public class BarangTest
{
    private Barang barang;

    @Before
    public void setUp() throws Exception
    {
        barang = new Barang();
        barang = new Barang("AAAX12347", "Maspion Colan Pancaguna Hello Kitty", "panci", 1);
    }

    @Test
    public void getKode() throws Exception
    {
        assertEquals("AAAX12347", barang.getKode());
    }

    @Test
    public void setKode() throws Exception
    {
        barang.setKode("AAAX12348");
        assertEquals("AAAX12348", barang.getKode());
    }

    @Test
    public void getNama() throws Exception
    {
        assertEquals("Maspion Colan Pancaguna Hello Kitty", barang.getNama());
    }

    @Test
    public void setNama() throws Exception
    {
        barang.setNama("Boboko Dutch Oven Gendhis");
        assertEquals("Boboko Dutch Oven Gendhis", barang.getNama());
    }

    @Test
    public void getTipe() throws Exception
    {
        assertEquals("panci", barang.getTipe());
    }

    @Test
    public void setTipe() throws Exception
    {
        barang.setTipe("sepatu");
        assertEquals("sepatu", barang.getTipe());
    }

    @Test
    public void getIsBeraturan() throws Exception
    {
        assertTrue(1 == barang.getIsBeraturan());
    }

    @Test
    public void setIsBeraturan() throws Exception
    {
        barang.setIsBeraturan(0);
        assertTrue(0 == barang.getIsBeraturan());
    }

    @Test
    public void equals() throws Exception
    {
        Barang barang2 = new Barang("AAAX12347", "Maspion Colan Pancaguna Hello Kitty", "panci", 1);
        assertEquals(barang, barang2);
    }

    @Test
    public void newArray() throws Exception
    {
        Barang[] newArray = barang.CREATOR.newArray(1);
        assertTrue(1 == newArray.length);
    }

    @Test
    public void describeContents() throws Exception
    {
        assertEquals(0, barang.describeContents());
    }
}
