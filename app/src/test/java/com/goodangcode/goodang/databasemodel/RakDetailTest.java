package com.goodangcode.goodang.databasemodel;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


/**
 * @author GoodangCode
 */
public class RakDetailTest
{
    private RakDetail rakDetail;

    @Before
    public void setUp() throws Exception
    {
        rakDetail = new RakDetail("A01-01", "A", "01", "01");
    }

    @Test
    public void getKode() throws Exception
    {
        assertEquals(rakDetail.getKode(), "A01-01");
    }

    @Test
    public void setKode() throws Exception
    {
        rakDetail.setKode("A02-02");
        assertEquals(rakDetail.getKode(), "A02-02");
    }

    @Test
    public void getDigitAlfabet() throws Exception
    {
        assertEquals(rakDetail.getDigitAlfabet(), "A");
    }

    @Test
    public void setDigitAlfabet() throws Exception
    {
        rakDetail.setDigitAlfabet("B");
        assertEquals(rakDetail.getDigitAlfabet(), "B");
    }

    @Test
    public void getDigitVertikal() throws Exception
    {
        assertEquals(rakDetail.getDigitVertikal(), "01");
    }

    @Test
    public void setDigitVertikal() throws Exception
    {
        rakDetail.setDigitVertikal("02");
        assertEquals(rakDetail.getDigitVertikal(), "02");
    }

    @Test
    public void getDigitHorizontal() throws Exception
    {
        assertEquals(rakDetail.getDigitHorizontal(), "01");
    }

    @Test
    public void setDigitHorizontal() throws Exception
    {
        rakDetail.setDigitHorizontal("02");
        assertEquals(rakDetail.getDigitHorizontal(), "02");
    }
}
