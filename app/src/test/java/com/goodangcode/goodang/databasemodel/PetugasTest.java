package com.goodangcode.goodang.databasemodel;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * @author GoodangCode
 */
public class PetugasTest
{
    private Petugas petugas;

    @Before
    public void setUp() throws Exception
    {
        petugas = new Petugas(1, "Tora Sudiro", 1, "torey44", "ebcdic", "G001");
    }

    @Test
    public void getKode() throws Exception
    {
        assertTrue(1 == petugas.getKode());
    }

    @Test
    public void setKode() throws Exception
    {
        petugas.setKode(2);
        assertTrue(2 == petugas.getKode());
    }

    @Test
    public void getNama() throws Exception
    {
        assertEquals("Tora Sudiro", petugas.getNama());
    }

    @Test
    public void setNama() throws Exception
    {
        petugas.setNama("Aditya Santoso");
        assertEquals("Aditya Santoso", petugas.getNama());
    }

    @Test
    public void getUsername() throws Exception
    {
        assertEquals("torey44", petugas.getUsername());
    }

    @Test
    public void setNamaPengguna() throws Exception
    {
        petugas.setUsername("santoso11");
        assertEquals("santoso11", petugas.getUsername());
    }

    @Test
    public void getKataSandi() throws Exception
    {
        assertEquals("ebcdic", petugas.getKataSandi());
    }

    @Test
    public void setKataSandi() throws Exception
    {
        petugas.setKataSandi("passkupassmu");
        assertEquals("passkupassmu", petugas.getKataSandi());
    }

    @Test
    public void equals() throws Exception
    {
        Petugas petugas2 = new Petugas(1, "Tora Sudiro", 1, "torey44", "ebcdic", "G001");

        assertEquals(petugas, petugas2);
    }
}
