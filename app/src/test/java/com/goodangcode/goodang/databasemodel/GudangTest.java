package com.goodangcode.goodang.databasemodel;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


/**
 * @author GoodangCode
 */
public class GudangTest
{
    private Gudang gudang;

    @Before
    public void setUp() throws Exception
    {
        gudang = new Gudang("G001", "Depok");
    }

    @Test
    public void getKode() throws Exception
    {
        assertEquals("G001", gudang.getKode());
    }

    @Test
    public void setKode() throws Exception
    {
        gudang.setKode("G002");
        assertEquals("G002", gudang.getKode());
    }

    @Test
    public void getLokasi() throws Exception
    {
        assertEquals("Depok", gudang.getLokasi());
    }

    @Test
    public void setLokasi() throws Exception
    {
        gudang.setLokasi("Jakarta");
        assertEquals("Jakarta", gudang.getLokasi());
    }

    @Test
    public void equals() throws Exception
    {
        Gudang gudang2 = new Gudang("G001", "Depok");

        assertEquals(gudang, gudang2);
    }
}
