package com.goodangcode.goodang.databasemodel;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * @author GoodangCode
 */
public class PemesananTest
{
    private Pemesanan pemesanan;

    @Before
    public void setUp() throws Exception
    {
        pemesanan = new Pemesanan();

        pemesanan = new Pemesanan("A20170511000002825-AAMY12346-001", 1, "G001", "AAMY12346",
                "Rahmawati/Arisan ONLINE2-TETANGGA-375-Urutan 1", "Jl. Taman Margasatwa No. 12, Warung Buncit",
                "Kecamatan A");

        pemesanan = new Pemesanan("A20170511000002825-AAMY12346-001", 1, "G001", "AAMY12346", "A01-01",
                "Rahmawati/Arisan ONLINE2-TETANGGA-375-Urutan 1", "Jl. Taman Margasatwa No. 12, Warung Buncit",
                "Kecamatan A", "2017-04-17 17:17:17", "2017-04-19 17:17:17");
    }

    @Test
    public void getKode() throws Exception
    {
        assertEquals("A20170511000002825-AAMY12346-001", pemesanan.getKode());
    }

    @Test
    public void setKode() throws Exception
    {
        pemesanan.setKode("A20170511000002825-AAMY12346-002");
        assertEquals("A20170511000002825-AAMY12346-002", pemesanan.getKode());
    }

    @Test
    public void getKodePetugas() throws Exception
    {
        assertTrue(1 == pemesanan.getKodePetugas());
    }

    @Test
    public void setKodePetugas() throws Exception
    {
        pemesanan.setKodePetugas(2);
        assertTrue(2 == pemesanan.getKodePetugas());
    }

    @Test
    public void getKodeGudang() throws Exception
    {
        assertEquals("G001", pemesanan.getKodeGudang());
    }

    @Test
    public void setKodeGudang() throws Exception
    {
        pemesanan.setKodeGudang("G002");
        assertEquals("G002", pemesanan.getKodeGudang());
    }

    @Test
    public void getKodeBarang() throws Exception
    {
        assertEquals("AAMY12346", pemesanan.getKodeBarang());
    }

    @Test
    public void setKodeBarang() throws Exception
    {
        pemesanan.setKodeBarang("AANX12346");
        assertEquals("AANX12346", pemesanan.getKodeBarang());
    }

    @Test
    public void getKodeRak() throws Exception
    {
        assertEquals("A01-01", pemesanan.getKodeRak());
    }

    @Test
    public void setKodeRak() throws Exception
    {
        pemesanan.setKodeRak("A02-02");
        assertEquals("A02-02", pemesanan.getKodeRak());
    }

    @Test
    public void getPenerima() throws Exception
    {
        assertEquals("Rahmawati/Arisan ONLINE2-TETANGGA-375-Urutan 1", pemesanan.getPenerima());
    }

    @Test
    public void setPenerima() throws Exception
    {
        pemesanan.setPenerima("Eko Purwanto/Arisan ONLINE-KELUARGA-2037-Urutan 1");
        assertEquals("Eko Purwanto/Arisan ONLINE-KELUARGA-2037-Urutan 1", pemesanan.getPenerima());
    }

    @Test
    public void getAlamat() throws Exception
    {
        assertEquals("Jl. Taman Margasatwa No. 12, Warung Buncit", pemesanan.getAlamat());
    }

    @Test
    public void setAlamat() throws Exception
    {
        pemesanan.setAlamat("Jl. Tebet Raya No. 84, Tebet");
        assertEquals("Jl. Tebet Raya No. 84, Tebet", pemesanan.getAlamat());
    }

    @Test
    public void getKecamatan() throws Exception
    {
        assertEquals("Kecamatan A", pemesanan.getKecamatan());
    }

    @Test
    public void setKecamatan() throws Exception
    {
        pemesanan.setKecamatan("Kecamatan B");
        assertEquals("Kecamatan B", pemesanan.getKecamatan());
    }

    @Test
    public void getTanggalMasuk() throws Exception
    {
        assertEquals("2017-04-17 17:17:17", pemesanan.getTanggalMasuk());
    }

    @Test
    public void setTanggalMasuk() throws Exception
    {
        pemesanan.setTanggalMasuk("2017-04-17 18:18:18");
        assertEquals("2017-04-17 18:18:18", pemesanan.getTanggalMasuk());
    }

    @Test
    public void getTanggalKeluar() throws Exception
    {
        assertEquals("2017-04-19 17:17:17", pemesanan.getTanggalKeluar());
    }

    @Test
    public void setTanggalKeluar() throws Exception
    {
        pemesanan.setTanggalKeluar("2017-04-19 19:19:19");
        assertEquals("2017-04-19 19:19:19", pemesanan.getTanggalKeluar());
    }

    @Test
    public void getSinkronisasi()
    {
        assertTrue(pemesanan.getSinkronisasi() == 0);
    }

    @Test
    public void setSinkronisasi()
    {
        pemesanan.setSinkronisasi(1);
        assertTrue(pemesanan.getSinkronisasi() == 1);
    }

    @Test
    public void getTerakhirDiubah()
    {
        assertEquals("2017-04-17 17:17:17", pemesanan.getTerakhirDiubah());
    }

    @Test
    public void setTerakhirDiubah()
    {
        pemesanan.setTerakhirDiubah("2017-04-17 18:18:18");
        assertEquals("2017-04-17 18:18:18", pemesanan.getTerakhirDiubah());
    }

    @Test
    public void equals() throws Exception
    {
        Pemesanan pemesanan2 = new Pemesanan("A20170511000002825-AAMY12346-001", 1, "G001", "AAMY12346", "A01-01",
                "Rahmawati/Arisan ONLINE2-TETANGGA-375-Urutan 1", "Jl. Taman Margasatwa No. 12, Warung Buncit",
                "Kecamatan A", "2017-04-17 17:17:17", "2017-04-19 17:17:17");

        assertEquals(pemesanan, pemesanan2);
    }

    @Test
    public void newArray() throws Exception
    {
        Pemesanan[] newArray = pemesanan.CREATOR.newArray(1);
        Assert.assertTrue(1 == newArray.length);
    }

    @Test
    public void describeContents() throws Exception
    {
        assertEquals(0, pemesanan.describeContents());
    }
}
