package com.goodangcode.goodang.databasemodel;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


/**
 * @author GoodangCode
 */
public class RakTest
{
    private Rak rak;

    @Before
    public void setUp() throws Exception
    {
        rak = new Rak();
        rak = new Rak("A01-01", "G001", "ac", 0);
    }

    @Test
    public void getKode() throws Exception
    {
        assertEquals("A01-01", rak.getKode());
    }

    @Test
    public void setKode() throws Exception
    {
        rak.setKode("A02-02");
        assertEquals("A02-02", rak.getKode());
    }

    @Test
    public void getKodeGudang() throws Exception
    {
        assertEquals("G001", rak.getKodeGudang());
    }

    @Test
    public void setKodeGudang() throws Exception
    {
        rak.setKodeGudang("G002");
        assertEquals("G002", rak.getKodeGudang());
    }

    @Test
    public void getTipe() throws Exception
    {
        assertEquals("ac", rak.getTipe());
    }

    @Test
    public void setTipe() throws Exception
    {
        rak.setTipe("sepatu");
        assertEquals("sepatu", rak.getTipe());
    }

    @Test
    public void getStatusPenuh() throws Exception
    {
        assertEquals(0, rak.getStatusPenuh());
    }

    @Test
    public void setStatusPenuh() throws Exception
    {
        rak.setStatusPenuh(1);
        assertEquals(1, rak.getStatusPenuh());
    }

    @Test
    public void getSinkronisasi() throws Exception
    {
        assertEquals(0, rak.getSinkronisasi());
    }

    @Test
    public void setSinkronisasi() throws Exception
    {
        rak.setSinkronisasi(1);
        assertEquals(1, rak.getSinkronisasi());
    }

    @Test
    public void equals() throws Exception
    {
        Rak rak2 = new Rak("A01-01", "G001", "ac", 0);
        assertEquals(rak, rak2);
    }

    @Test
    public void newArray() throws Exception
    {
        Rak[] newArray = rak.CREATOR.newArray(1);
        Assert.assertTrue(1 == newArray.length);
    }

    @Test
    public void describeContents() throws Exception
    {
        assertEquals(0, rak.describeContents());
    }
}
