package com.goodangcode.goodang;

import com.goodangcode.goodang.databasemodel.Barang;
import com.goodangcode.goodang.databasemodel.Pemesanan;
import com.goodangcode.goodang.databasemodel.Rak;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;


/**
 * @author GoodangCode
 */
public class RowItemTest
{
    private Pemesanan pemesanan;
    private Barang barang;
    private Rak rak;

    private RowItem rowItem;

    @Before
    public void setUp() throws Exception
    {
        pemesanan = new Pemesanan("A20170511000002825-AAAX12347-001", 1, "G001", "AAAX12347", "A01-01",
                "Rahmawati/Arisan ONLINE2-TETANGGA-375-Urutan 1", "Jl. Taman Margasatwa No. 12, Warung Buncit",
                "Kecamatan A", "2017-04-17 17:17:17", null);
        barang = new Barang("AAAX12347", "Maspion Colan Pancaguna Hello Kitty", "panci", 1);
        rak = new Rak("A01-01", "G001", "panci", 0);

        rowItem = new RowItem(barang);
        rowItem = new RowItem(pemesanan, barang);
    }

    @Test
    public void getPemesanan() throws Exception
    {
        assertTrue(pemesanan.equals(rowItem.getPemesanan()));
    }

    @Test
    public void getBarang() throws Exception
    {
        assertTrue(barang.equals(rowItem.getBarang()));
    }

    @Test
    public void getRak() throws Exception {
        rowItem = new RowItem(rak);
        assertTrue(rak.equals(rowItem.getRak()));
    }
}