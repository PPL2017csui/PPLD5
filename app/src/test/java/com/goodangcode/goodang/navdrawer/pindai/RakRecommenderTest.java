package com.goodangcode.goodang.navdrawer.pindai;

import com.goodangcode.goodang.BuildConfig;
import com.goodangcode.goodang.MainActivity;
import com.goodangcode.goodang.databasehelper.DatabaseController;
import com.goodangcode.goodang.databasemodel.Barang;
import com.goodangcode.goodang.databasemodel.Gudang;
import com.goodangcode.goodang.databasemodel.Pemesanan;
import com.goodangcode.goodang.databasemodel.Petugas;
import com.goodangcode.goodang.databasemodel.Rak;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


/**
 * @author GoodangCode
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class RakRecommenderTest
{
    private MainActivity activity;
    private RakRecommender rakRecommender;

    private Rak rakPanci1;
    private Rak rakPanci2;

    @Before
    public void setUp() throws Exception
    {
//        activity = Robolectric.setupActivity(MainActivity.class);

        // DatabaseController db = DatabaseController.getInstance(activity);

        // Barang barang = new Barang("AAAX12347", "Maspion Colan Pancaguna Hello Kitty", "panci", 1);
        // Gudang gudang = new Gudang("G001", "Depok");
        // Pemesanan pemesanan = new Pemesanan("A20170511000002825-AAAX12347-001", 1, "G001", "AAAX12347",
        //         "Rahmawati/Arisan ONLINE2-TETANGGA-375-Urutan 1", "Jl. Taman Margasatwa No. 12, Warung Buncit",
        //         "Kecamatan A");
        // Petugas petugas = new Petugas(1, "Tora Sudiro", 1, "torey44", "ebcdic", "G001");
        // db.addBarang(barang);
        // db.addGudang(gudang);
        // db.addPemesanan(pemesanan);
        // db.addPetugas(petugas);

        // rakPanci1 = new Rak("A01-01", "G001", "panci", 0);
        // rakPanci2 = new Rak("A01-02", "G001", "panci", 0);
        // db.addRak(rakPanci1);
        // db.addRak(rakPanci2);

        // db.close();

        // rakRecommender = new RakRecommender(activity, pemesanan);
    }

    @Test
    public void checkActivity() throws Exception
    {
//        assertNotNull(activity);
        assertTrue(true);
    }


    // @Test
    // public void recommendRak() throws Exception
    // {
    //     assertEquals(rakPanci1, rakRecommender.recommendRak(null, ""));
    //     assertEquals(rakPanci2, rakRecommender.recommendRak(rakPanci1, "After"));
    //     assertEquals(rakPanci1, rakRecommender.recommendRak(rakPanci2, "Before"));
    //     assertEquals(rakPanci2, rakRecommender.recommendRak(rakPanci1, "Before"));
    // }

    // @Test
    // public void recommendRakKecamatanNull() throws Exception
    // {
    //     DatabaseController db = DatabaseController.getInstance(activity);

    //     Barang barang = new Barang("AAAX12348", "Koko Juragan", "pakaian", 0);
    //     Pemesanan pemesanan = new Pemesanan("A20170511000002825-AAAX12348-002", 1, "G001", "AAAX12348",
    //             "Rahmawati/Arisan ONLINE2-TETANGGA-375-Urutan 1", "Jl. Taman Margasatwa No. 12, Warung Buncit",
    //             "Kecamatan A");
    //     db.addBarang(barang);
    //     db.addPemesanan(pemesanan);

    //     Rak rakKecamatanA = new Rak("A01-03", "G001", "Kecamatan A", 1);
    //     db.addRak(rakKecamatanA);
    //     db.close();
    //     rakRecommender = new RakRecommender(activity, pemesanan);

    //     assertNull(rakRecommender.recommendRak(null, ""));
    // }

    // @Test
    // public void recommendRakKecamatanNotNull() throws Exception
    // {
    //     DatabaseController db = DatabaseController.getInstance(activity);

    //     Barang barang = new Barang("AAAX12348", "Koko Juragan", "pakaian", 0);
    //     Pemesanan pemesanan = new Pemesanan("A20170511000002825-AAAX12348-002", 1, "G001", "AAAX12348",
    //             "Rahmawati/Arisan ONLINE2-TETANGGA-375-Urutan 1", "Jl. Taman Margasatwa No. 12, Warung Buncit",
    //             "Kecamatan A");
    //     db.addBarang(barang);
    //     db.addPemesanan(pemesanan);

    //     Rak rakKecamatanA = new Rak("A01-03", "G001", "Kecamatan A", 0);
    //     db.addRak(rakKecamatanA);
    //     db.close();

    //     rakRecommender = new RakRecommender(activity, pemesanan);

    //     assertNotNull(rakRecommender.recommendRak(null, ""));
    // }

    // @Test
    // public void isExistRak() throws Exception
    // {
    //     assertTrue(rakRecommender.isExistRak("A01-01"));
    // }

    // @Test
    // public void isFitRak() throws Exception
    // {
    //     rakRecommender.isExistRak(rakPanci1.getKode());
    //     assertTrue(rakRecommender.isFitRak());
    // }
}