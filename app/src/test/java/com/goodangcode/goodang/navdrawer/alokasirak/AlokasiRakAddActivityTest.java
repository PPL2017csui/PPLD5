package com.goodangcode.goodang.navdrawer.alokasirak;

import android.widget.Button;
import android.widget.EditText;

import com.goodangcode.goodang.BuildConfig;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


/**
 * @author GoodangCode
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class AlokasiRakAddActivityTest
{
    private AlokasiRakAddActivity alokasiRakAddActivity;

    private EditText editTextKodeRak;
    private EditText editTextTipeRak;

    private Button buttonAddSimpan;

    @Before
    public void setUp() throws Exception
    {
        alokasiRakAddActivity = Robolectric.setupActivity(AlokasiRakAddActivity.class);

        editTextKodeRak = alokasiRakAddActivity.editTextKodeRak;
        editTextTipeRak = alokasiRakAddActivity.editTextTipeRak;

        buttonAddSimpan = alokasiRakAddActivity.buttonAddSimpan;
    }

    @Test
    public void onCreate() throws Exception
    {
        assertNotNull(alokasiRakAddActivity);
    }

    @Test
    public void addListenerOnButton() throws Exception
    {
        buttonAddSimpan.performClick();
    }

    @Test
    public void checkInputValueKodeNull() throws Exception
    {
        String str = "";
        editTextKodeRak.setText(str);
        alokasiRakAddActivity.checkInputValue();
        assertTrue(str.equals(editTextKodeRak.getText().toString()));
    }

    @Test
    public void checkInputValueKodeNotNull() throws Exception
    {
        String str = "Kode Not Null";
        editTextKodeRak.setText(str);
        alokasiRakAddActivity.checkInputValue();
        assertTrue(str.equals(editTextKodeRak.getText().toString()));
    }

    @Test
    public void checkInputValueTipeNull() throws Exception
    {
        String str = "";
        editTextTipeRak.setText(str);
        alokasiRakAddActivity.checkInputValue();
        assertTrue(str.equals(editTextTipeRak.getText().toString()));
    }

    @Test
    public void checkInputValueTipeNotNull() throws Exception
    {
        String str = "Tipe Not Null";
        editTextTipeRak.setText(str);
        alokasiRakAddActivity.checkInputValue();
        assertTrue(str.equals(editTextTipeRak.getText().toString()));
    }
}