package com.goodangcode.goodang.navdrawer.pindai;

import com.goodangcode.goodang.BuildConfig;
import com.goodangcode.goodang.MainActivity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil;

import static org.junit.Assert.assertNotNull;


/**
 * @author GoodangCode
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class BarangMasukFragmentTest
{
    private MainActivity activity;
    private BarangMasukFragment barangMasukFragment;

    @Before
    public void setUp() throws Exception
    {
        activity = Robolectric.setupActivity(MainActivity.class);
        barangMasukFragment = new BarangMasukFragment();
        SupportFragmentTestUtil.startFragment(barangMasukFragment, activity.getClass());
    }

    @Test
    public void onCreateView() throws Exception
    {
        assertNotNull(barangMasukFragment.onCreateView(activity.getLayoutInflater(), null, null));
    }
}