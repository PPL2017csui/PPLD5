package com.goodangcode.goodang;

import android.content.Intent;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.robolectric.Shadows.shadowOf;


/**
 * @author GoodangCode
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class GcmTokenRefreshListenerServiceTest
{
    private GCMTokenRefreshListenerService gcmTokenRefreshListenerService;

    @Before
    public void setUp() throws Exception
    {
        gcmTokenRefreshListenerService = Robolectric.setupService(GCMTokenRefreshListenerService.class);
    }

    @Test
    public void tokenRefresh()
    {
        gcmTokenRefreshListenerService.onTokenRefresh();
        ShadowApplication shadowApplication = shadowOf(RuntimeEnvironment.application);
        Intent intent = shadowApplication.getNextStartedService();
        assertNotNull(intent);
        assertEquals(intent.getComponent().getClassName(),
                RegistrationIntentService.class.getName());
    }

}
