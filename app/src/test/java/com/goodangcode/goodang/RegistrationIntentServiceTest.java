package com.goodangcode.goodang;

import android.app.Application;
import android.content.Intent;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import static org.junit.Assert.assertNotNull;
import static org.robolectric.Shadows.shadowOf;


/**
 * @author GoodangCode
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class RegistrationIntentServiceTest
{
    private RegistrationIntentService registrationIntentService;

    @Before
    public void setUp() throws Exception
    {
        registrationIntentService = Robolectric.setupService(RegistrationIntentService.class);
    }

    @Test
    public void registerToken()
    {
        Application application = RuntimeEnvironment.application;

        Intent intent =  new Intent(application, RegistrationIntentService.class);

//        registrationIntentService.onHandleIntent(intent);
        ShadowApplication shadowApplication = shadowOf(RuntimeEnvironment.application);
        assertNotNull(shadowApplication);
    }
}
