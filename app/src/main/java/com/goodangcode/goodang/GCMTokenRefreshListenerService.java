package com.goodangcode.goodang;

import android.content.Intent;

import com.google.android.gms.iid.InstanceIDListenerService;


/**
 * @author GoodangCode
 */
public class GCMTokenRefreshListenerService extends InstanceIDListenerService
{
    @Override
    public void onTokenRefresh()
    {
        Intent intent = new Intent(this, RegistrationIntentService.class);
        startService(intent);
    }
}
