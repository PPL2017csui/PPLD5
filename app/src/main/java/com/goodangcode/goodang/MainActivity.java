package com.goodangcode.goodang;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.goodangcode.goodang.databasehelper.DatabaseController;
import com.goodangcode.goodang.databasemodel.Gudang;
import com.goodangcode.goodang.login.LoginActivity;
import com.goodangcode.goodang.navdrawer.AlokasiRak;
import com.goodangcode.goodang.navdrawer.DaftarBarang;
import com.goodangcode.goodang.navdrawer.Pindai;
import com.goodangcode.goodang.navdrawer.RiwayatBarang;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import io.fabric.sdk.android.Fabric;


/**
 * Main Activity - the entire lifecycle of an activity.
 *
 * @author GoodangCode
 */
public class MainActivity extends AppCompatActivity
{
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private NavigationView navDrawer;
    private ActionBarDrawerToggle drawerToggle;
    private TextView namaGudangView;
    private TextView namaPenggunaView;
    private AlertDialog.Builder alert;
    private boolean doubleBackToExitPressedOnce;

    /**
     * Method is called when the activity is first created - normal static set up.
     *
     * @param savedInstanceState Initialization of the activity.
     */
    private BroadcastReceiver mGcmRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        doubleBackToExitPressedOnce = false;

        mGcmRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                if(intent.getAction().endsWith(QuickstartPreferences.REGISTRATION_COMPLETE)){
                    String token = intent.getStringExtra("token");
                    Toast.makeText(getApplicationContext(), "GCM token: " + token,
                            Toast.LENGTH_LONG).show();
                }
                else if(intent.getAction().endsWith(QuickstartPreferences.REGISTRATION_ERROR)){
                    Toast.makeText(getApplicationContext(), "GCM Registration error!",
                            Toast.LENGTH_LONG).show();
                }
            }
        };

        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable
                (getApplicationContext());
        if(ConnectionResult.SUCCESS != resultCode){
            if(GooglePlayServicesUtil.isUserRecoverableError(resultCode)){
                Toast.makeText(getApplicationContext(), "Google Play Service is not " +
                        "enabled/install in this device!", Toast.LENGTH_LONG).show();
                GooglePlayServicesUtil.showErrorNotification(resultCode, getApplicationContext());
            }
            else {
                Toast.makeText(getApplicationContext(), "This device doesn't support Google Play!" +
                        "Service!", Toast.LENGTH_LONG).show();
            }
        }
        else {
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = setupDrawerToggle();
        drawerLayout.addDrawerListener(drawerToggle);

        navDrawer = (NavigationView) findViewById(R.id.navigation_view);

        View headerView = navDrawer.getHeaderView(0);
        namaGudangView = (TextView)headerView.findViewById(R.id.nama_gudang);
        Gudang namaGudang = DatabaseController.getInstance(getApplicationContext()).getGudang(SessionManager.getInstance(getApplicationContext()).getGudangCode());
        if(namaGudang != null){
            namaGudangView.setText(namaGudang.getKode()+" - "+ namaGudang.getLokasi());
        }

        namaPenggunaView = (TextView)headerView.findViewById(R.id.nama_user);
        namaPenggunaView.setText(SessionManager.getInstance(getApplicationContext()).getNamaPengguna());

        setupDrawerContent(navDrawer);

        if(savedInstanceState == null) {
            try {
                doFragment(Pindai.class);
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Method is called when setup the content of drawer.
     *
     * @param navDrawer Navigation view.
     */
    public void setupDrawerContent(NavigationView navDrawer)
    {
        navDrawer.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                /**
                 * Method is called whenever a navigation item in action bar is selected.
                 *
                 * @param menuItem The menu item that was selected.
                 * @return true if the event was handled, false otherwise.
                 */
                @Override
                public boolean onNavigationItemSelected(MenuItem menuItem)
                {
                    selectDrawerItem(menuItem);
                    return true;
                }
            }
        );
    }

    /**
     * Method is called when user select the menu item property.
     *
     * @param menuItem The menu item that was selected.
     */
    public void selectDrawerItem(MenuItem menuItem)
    {
        try {
            switch(menuItem.getItemId()) {
                case R.id.nav_pindai:
                    doFragment(Pindai.class);
                    break;
                case R.id.nav_daftar:
                    doFragment(DaftarBarang.class);
                    break;
                case R.id.nav_riwayat:
                    doFragment(RiwayatBarang.class);
                    break;
                case R.id.nav_alokasi:
                    doFragment(AlokasiRak.class);
                    break;
                case R.id.nav_sinkronisasi:
                    konfirmasiSinkronisasi();
                    break;
                case R.id.nav_logout:
                    SessionManager.getInstance(getApplicationContext()).logoutUser();
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    finish();
                    startActivity(intent);
                    break;
                default:
                    doFragment(Pindai.class);
                    break;
            }
        }
        catch(Exception e) {
            Toast.makeText(MainActivity.this, R.string.error_CLASS02,
                    Toast.LENGTH_LONG).show();
        }

        menuItem.setChecked(true);
        setTitle(menuItem.getTitle());
        drawerLayout.closeDrawers();
    }

    /**
     * Method is called whenever an item in your options menu is selected.
     *
     * @param item The menu item that was selected.
     * @return Return false to allow normal menu processing to proceed, true to consume it here.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Method is called when user want to setup drawer toggle.
     *
     * @return toggle of action bar drawer.
     */
    private ActionBarDrawerToggle setupDrawerToggle()
    {
        return new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.drawer_open,  R.string.drawer_close);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        try {
            if(resultCode == RESULT_OK && data != null) {
                if((requestCode == getResources().getInteger(R.integer.alokasi_rak) ||
                     requestCode == getResources().getInteger(R.integer.alokasi_rak_add))) {
                    doFragment(AlokasiRak.class);
                }
                else if(requestCode == getResources().getInteger(R.integer.daftar_barang_detail)) {
                    doFragment(DaftarBarang.class);
                }
                else if(requestCode == getResources().getInteger(R.integer.detail_pemesanan)) {
                    doFragment(RiwayatBarang.class);
                }
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed()
    {
        if(doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        doubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.info_exit, Toast.LENGTH_LONG).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run()
            {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    /**
     * Method is called when user select the menu item property.
     *
     * @param fragmentClass The fragment class that was done.
     */
    public void doFragment(Class fragmentClass) throws Exception
    {
        Fragment fragment = null;

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commitAllowingStateLoss();
    }

    /**
     * Method is called when activity start-up is complete.
     *
     * @param savedInstanceState If the activity is being re-initialized after previously being shut down,
     *                           then this Bundle contains the data it most recently supplied in onSaveInstanceState(Bundle).
     *                           Otherwise it is null.
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState)
    {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    /**
     * Method is called by the system when the device configuration changes while the activity is running.
     *
     * @param newConfig The new device configuration.
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    private void konfirmasiSinkronisasi() {
        alert = new AlertDialog.Builder(this);
        alert.setCancelable(false);

        alert.setTitle(R.string.dialog_title_add);
        alert.setMessage(R.string.dialog_message_sinkronisasi_data);

        alert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton)
            {
                DatabaseController db = DatabaseController.getInstance(MainActivity.this);
                db.syncDatabase(getApplicationContext());
                db.close();

                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                intent.putExtras(bundle);
                setResult(RESULT_OK, intent);

            }
        });

        alert.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton)
            {

            }
        });

        alert.show();
    }

    /**
     * Method is called when application is forced.
     *
     * @param view The view that was crashed.
     */
    public void forceCrash(View view)
    {
        throw new RuntimeException("This is a crash");
    }
}
