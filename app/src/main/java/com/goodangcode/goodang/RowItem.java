package com.goodangcode.goodang;

import android.app.Activity;
import android.content.Context;

import com.goodangcode.goodang.databasehelper.DatabaseController;
import com.goodangcode.goodang.databasemodel.Barang;
import com.goodangcode.goodang.databasemodel.Pemesanan;
import com.goodangcode.goodang.databasemodel.Rak;


/**
 * Class represents the row item.
 *
 * @author GoodangCode
 */
public class RowItem
{
    private Pemesanan pemesanan;
    private Barang barang;
    private Rak rak;

    /**
     * There is a form of the constructor that are called when the view of row item is created.
     *
     * @param barang Initialization of barang.
     */
    public RowItem(Barang barang)
    {
        this(null, barang);
    }

    /**
     * There is a form of the constructor that are called when the view of row item is created.
     *
     * @param pemesanan Initialization of pemesanan.
     */
    public RowItem(Pemesanan pemesanan)
    {
        this(pemesanan, null);
    }

    /**
     * There is a form of the constructor that are called when the view of row item is created.
     *
     * @param pemesanan Initialization of pemesanan.
     * @param barang Initialization of barang.
     */
    public RowItem(Pemesanan pemesanan, Barang barang)
    {
        this.pemesanan = pemesanan;
        this.barang = barang;
    }

    /**
     * There is a form of the constructor that are called when the view of row item is created.
     *
     * @param rak Initialization of rak.
     */
    public RowItem(Rak rak)
    {
        this.rak = rak;
    }

    /**
     * Method is called when getting pemesanan.
     *
     * @return Return object that represents pemesanan.
     */
    public Pemesanan getPemesanan()
    {
        return pemesanan;
    }

    /**
     * Method is called when getting barang.
     *
     * @return Return object that represents barang.
     */
    public Barang getBarang()
    {
        return barang;
    }


    /**
     * Method is called when getting count of barang.
     *
     * @param context Context which is running.
     * @return Return count of barang.
     */
    public int countBarang(Context context)
    {
        Activity activity = (Activity) context;
        DatabaseController db = DatabaseController.getInstance(activity);
        int count = db.getBarangCount(getBarang().getKode());
        db.close();

        return count;
    }

    /**
     * Method is called when getting rak.
     *
     * @return Return object that represents rak.
     */
    public Rak getRak()
    {
        return rak;
    }
}
