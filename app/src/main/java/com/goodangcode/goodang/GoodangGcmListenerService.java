package com.goodangcode.goodang;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.goodangcode.goodang.databasehelper.DatabaseController;
import com.goodangcode.goodang.databasemodel.Barang;
import com.goodangcode.goodang.databasemodel.Gudang;
import com.goodangcode.goodang.databasemodel.Pemesanan;
import com.goodangcode.goodang.databasemodel.Petugas;
import com.goodangcode.goodang.databasemodel.Rak;
import com.google.android.gms.gcm.GcmListenerService;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author GoodangCode
 */
public class GoodangGcmListenerService extends GcmListenerService
{
    private static final String TAG = "MyGcmListenerService";

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    @Override
    public void onMessageReceived(String from, Bundle data)
    {
        String message = data.getString("message");
        String konten="";
        String gudang="";
        String metode="";
        JSONObject pesan = null;
        try {
            pesan = new JSONObject(message);
            metode = pesan.getString("metode");
            if(metode.contains("rak") || metode.contains("pemesanan")){
                gudang = pesan.getString("gudang");
            }
        } catch (JSONException e) {
            metode = message;
        }
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Message: " + message);
        SessionManager session = SessionManager.getInstance(getApplicationContext());
        if(from.startsWith("/topics/update") && session.isLoggedIn()) {
            if(metode.contains("Update")) {
                if (metode.contains("rak")&& session.getGudangCode().equals(gudang)) {
                    //update rak yg kode gudangnya tertentu
                    try {
                        String rak = pesan.getString("rak");
                        konten = "Rak : " + rak;
                        DatabaseController.getInstance(getApplicationContext()).syncRak(rak,gudang);
                        DatabaseController.getInstance(getApplicationContext()).close();
                    } catch (JSONException e){

                    }
                } else if (metode.contains("pemesanan")&& session.getGudangCode().equals(gudang)) {
                    //update pemesanan yg kode gudangnya tertentu
                    try {
                        String pemesanan = pesan.getString("pemesanan");
                        konten = "Pemesanan : " + pemesanan;
                        DatabaseController.getInstance(getApplicationContext()).syncPemesanan(pemesanan,gudang);
                        DatabaseController.getInstance(getApplicationContext()).close();
                    } catch (JSONException e){

                    }
                } else if (metode.contains("barang")) {
                    //update barang yg kode gudangnya tertentu
                    try {
                        String barang = pesan.getString("barang");
                        konten = "Barang : " + barang;
                        DatabaseController.getInstance(getApplicationContext()).syncBarang(barang);
                        DatabaseController.getInstance(getApplicationContext()).close();
                    } catch (JSONException e){

                    }
                } else if (metode.contains("gudang")) {
                    //update gudang yg kode gudangnya tertentu
                    try {
                        gudang = pesan.getString("gudang");
                        konten = "Gudang : " + gudang;
                        DatabaseController.getInstance(getApplicationContext()).syncGudang(gudang);
                        DatabaseController.getInstance(getApplicationContext()).close();
                    } catch (JSONException e){

                    }
                } else if (metode.contains("petugas")) {
                    //update petugas yg kode gudangnya tertentu
                    try {
                        String petugas = pesan.getString("petugas");
                        konten = "Petugas : " + petugas;
                        DatabaseController.getInstance(getApplicationContext()).syncPetugas(petugas);
                        DatabaseController.getInstance(getApplicationContext()).close();
                    } catch (JSONException e){

                    }
                }
            }
            else if (metode.contains("Delete")){
                if (metode.contains("rak")&& session.getGudangCode().equals(gudang)) {
                    //update rak yg kode gudangnya tertentu
                    try {
                        String rak = pesan.getString("rak");
                        konten = "Rak : " + rak;
                        DatabaseController db = DatabaseController.getInstance(getApplicationContext());
                        Rak cekRak = db.getRak(rak);
                        if(cekRak != null){
                            db.deleteRak(cekRak);
                        }
                        db.getInstance(getApplicationContext()).close();
                    } catch (JSONException e){

                    }

                } else if (metode.contains("pemesanan")&& session.getGudangCode().equals(gudang)) {
                    //update pemesanan yg kode gudangnya tertentu
                    try {
                        String pemesanan = pesan.getString("pemesanan");
                        konten = "Pemesanan : " + pemesanan;
                        DatabaseController db = DatabaseController.getInstance(getApplicationContext());
                        Pemesanan cekPemesanan = db.getPemesanan(pemesanan);
                        if(cekPemesanan != null){
                            db.deletePemesanan(cekPemesanan);
                        }
                        db.getInstance(getApplicationContext()).close();
                    } catch (JSONException e){

                    }
                } else if (metode.contains("barang")) {
                    //update barang yg kode gudangnya tertentu
                    try {
                        String barang = pesan.getString("barang");
                        konten = "Barang : " + barang;
                        DatabaseController db = DatabaseController.getInstance(getApplicationContext());
                        Barang cekBarang = db.getBarang(barang);
                        if(cekBarang != null){
                            db.deleteBarang(cekBarang);
                        }
                        db.getInstance(getApplicationContext()).close();
                    } catch (JSONException e){

                    }
                } else if (metode.contains("gudang")) {
                    //update gudang yg kode gudangnya tertentu
                    try {
                        gudang = pesan.getString("gudang");
                        konten = "Gudang : " + gudang;
                        DatabaseController db = DatabaseController.getInstance(getApplicationContext());
                        Gudang cekGudang = db.getGudang(gudang);
                        if(cekGudang != null){
                            db.deleteGudang(cekGudang);
                        }
                        db.getInstance(getApplicationContext()).close();
                    } catch (JSONException e){

                    }
                } else if (metode.contains("petugas")) {
                    //update petugas yg kode gudangnya tertentu
                    try {
                        String petugas = pesan.getString("petugas");
                        konten = "Petugas : " + petugas;
                        DatabaseController db = DatabaseController.getInstance(getApplicationContext());
                        Petugas cekPetugas = db.getPetugas(Integer.parseInt(petugas));
                        if(cekPetugas != null){
                            db.deletePetugas(cekPetugas);
                        }
                        db.getInstance(getApplicationContext()).close();
                    } catch (JSONException e){

                    }
                }
            }
        }
        Log.d("cek konten =",konten);
        if(session.isLoggedIn() && session.getGudangCode().equals(gudang)){
            sendNotification(metode, konten);
        }
    }

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     * @param konten
     */
    private void sendNotification(String message, String konten)
    {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_logo)
                .setContentTitle(message)
                .setContentText(konten)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}