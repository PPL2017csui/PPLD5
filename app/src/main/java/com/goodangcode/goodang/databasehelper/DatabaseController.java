package com.goodangcode.goodang.databasehelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.goodangcode.goodang.MySingleton;
import com.goodangcode.goodang.SessionManager;
import com.goodangcode.goodang.databasemodel.Barang;
import com.goodangcode.goodang.databasemodel.Gudang;
import com.goodangcode.goodang.databasemodel.Pemesanan;
import com.goodangcode.goodang.databasemodel.Petugas;
import com.goodangcode.goodang.databasemodel.Rak;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Class represents the database controller.
 *
 * @author GoodangCode
 */
public class DatabaseController extends SQLiteOpenHelper
{
    private static SQLiteDatabase db;
    private static DatabaseController instance;
    private static Context context;
    public static final String SERVER_URL = "https://goodang-update-staging.herokuapp.com";

    // Logcat Tag
    private static final String LOG = DatabaseController.class.getName();

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Goodang";

    // Table Names
    public static final String TABLE_BARANG = "barang";
    public static final String TABLE_GUDANG = "gudang";
    public static final String TABLE_PEMESANAN = "pemesanan";
    public static final String TABLE_PETUGAS = "petugas";
    public static final String TABLE_RAK = "rak";

    // Common Column Names
    private static final String KEY_KODE = "kode";
    private static final String KEY_NAMA = "nama";
    private static final String KEY_TIPE = "tipe";
    private static final String KEY_KODE_GUDANG = "kode_gudang";

    // GUDANG Table - Column Names
    private static final String KEY_LOKASI = "lokasi";

    // BARANG Table - Column Names
    private static final String KEY_IS_BERATURAN = "is_beraturan";

    // PETUGAS Table - Column Names
    private static final String KEY_IS_ADMIN = "isadmin";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_KATA_SANDI = "kata_sandi";

    // RAK Table - Column Names
    private static final String KEY_STATUS_PENUH = "status_penuh";

    // PEMESANAN Table - Column Names
    private static final String KEY_KODE_PETUGAS = "kode_petugas";
    private static final String KEY_KODE_BARANG = "kode_barang";
    private static final String KEY_KODE_RAK = "kode_rak";
    private static final String KEY_PENERIMA = "penerima";
    private static final String KEY_ALAMAT = "alamat";
    private static final String KEY_KECAMATAN = "kecamatan";
    private static final String KEY_TANGGAL_MASUK = "tanggal_masuk";
    private static final String KEY_TANGGAL_KELUAR = "tanggal_keluar";
    private static final String KEY_SINKRONISASI = "sinkronisasi";
    private static final String KEY_TERAKHIR_DIUBAH = "terakhir_diubah";

    // Synchronize status
    public static final int SYNC_STATUS_OK = 1;
    public static final int SYNC_STATUS_FAILED = 0;

    private static final String CREATE_TABLE_BARANG =
            "CREATE TABLE " + TABLE_BARANG + "( " +
                    KEY_KODE + " CHARACTER VARYING(15) PRIMARY KEY, " +
                    KEY_NAMA + " CHARACTER VARYING(255) NOT NULL, " +
                    KEY_TIPE + " CHARACTER VARYING(40) NOT NULL, " +
                    KEY_IS_BERATURAN + " INT NOT NULL" +
                    ");";

    private static final String CREATE_TABLE_GUDANG =
            "CREATE TABLE " + TABLE_GUDANG + "( " +
                    KEY_KODE + " CHARACTER VARYING(255) PRIMARY KEY, " +
                    KEY_LOKASI + " CHARACTER VARYING(255) NOT NULL" +
                    ");";

    private static final String CREATE_TABLE_PEMESANAN =
            "CREATE TABLE " + TABLE_PEMESANAN + "( " +
                    KEY_KODE + " CHARACTER VARYING(40) NOT NULL, " +
                    KEY_KODE_PETUGAS + " SERIAL NOT NULL, " +
                    KEY_KODE_GUDANG + " CHARACTER VARYING(255) NOT NULL, " +
                    KEY_KODE_BARANG + " CHARACTER VARYING(15) NOT NULL, " +
                    KEY_KODE_RAK + " CHARACTER VARYING(10) NOT NULL, " +
                    KEY_PENERIMA + " CHARACTER VARYING(255) NOT NULL, " +
                    KEY_ALAMAT + " CHARACTER VARYING(255) NOT NULL, " +
                    KEY_KECAMATAN + " CHARACTER VARYING(255) NOT NULL, " +
                    KEY_TANGGAL_MASUK + " TIMESTAMP WITH TIME ZONE NOT NULL, " +
                    KEY_TANGGAL_KELUAR + " TIMESTAMP WITH TIME ZONE, " +
                    KEY_SINKRONISASI + " INT NOT NULL, " +
                    KEY_TERAKHIR_DIUBAH + " TIMESTAMP WITH TIME ZONE NOT NULL, " +
                    "CONSTRAINT Pemesanan_pk PRIMARY KEY (" + KEY_KODE + "," + KEY_KODE_PETUGAS + ")" +
                    ");";

    private static final String CREATE_TABLE_PETUGAS =
            "CREATE TABLE " + TABLE_PETUGAS + "( " +
                    KEY_KODE + " SERIAL PRIMARY KEY, " +
                    KEY_NAMA + " CHARACTER VARYING(100) NOT NULL, " +
                    KEY_IS_ADMIN + " INT NOT NULL, " +
                    KEY_USERNAME + " CHARACTER VARYING(255) NOT NULL UNIQUE, " +
                    KEY_KATA_SANDI + " CHARACTER VARYING(255) NOT NULL, " +
                    KEY_KODE_GUDANG + " CHARACTER VARYING(255) NOT NULL" +
                    ");";

    private static final String CREATE_TABLE_RAK =
            "CREATE TABLE " + TABLE_RAK + "( " +
                    KEY_KODE + " CHARACTER VARYING(10) NOT NULL, " +
                    KEY_KODE_GUDANG + " CHARACTER VARYING(255) NOT NULL, " +
                    KEY_TIPE + " CHARACTER VARYING(40) NOT NULL, " +
                    KEY_STATUS_PENUH + " INT NOT NULL, " +
                    KEY_SINKRONISASI + " INT NOT NULL, " +
                    "CONSTRAINT Rak_pk PRIMARY KEY (" + KEY_KODE + "," + KEY_KODE_GUDANG + ")" +
                    ");";


    /**
     * Constructor of DatabaseController
     *
     * @param context
     * @param name
     * @param factory
     * @param version
     */
    public DatabaseController(Context context, String name, SQLiteDatabase.CursorFactory factory, int version)
    {
        super(context, name, factory, version);
        this.context = context;
    }

    /**
     * Method is called when the activity is first created - normal static set up,
     * to create the database design table.
     *
     * @param db Initialization of the database SQLite.
     */
    @Override
    public void onCreate(SQLiteDatabase db)
    {
        // Creating Required Tables
        db.execSQL(CREATE_TABLE_BARANG);
        db.execSQL(CREATE_TABLE_GUDANG);
        db.execSQL(CREATE_TABLE_PEMESANAN);
        db.execSQL(CREATE_TABLE_PETUGAS);
        db.execSQL(CREATE_TABLE_RAK);
    }

    /**
     * Method is called when user want to upgrade the old version of database
     * to the new version of database.
     *
     * @param db The database that was upgraded.
     * @param oldVersion Old version number of the database.
     * @param newVersion New version number of the database.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        // On Upgrade Drop Older Tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BARANG);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GUDANG);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PEMESANAN);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PETUGAS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RAK);

        // Create New Tables
        onCreate(db);
    }

    /**
     * Method to insert pemesanan data into SQLite database.
     *
     * @param pemesanan Pemesanan data that was inserted.
     * @return Return status of adding process.
     */
    public long addPemesanan(Pemesanan pemesanan)
    {
        db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_KODE, pemesanan.getKode());
        values.put(KEY_KODE_PETUGAS, pemesanan.getKodePetugas());
        values.put(KEY_KODE_GUDANG, pemesanan.getKodeGudang());
        values.put(KEY_KODE_BARANG, pemesanan.getKodeBarang());
        values.put(KEY_KODE_RAK, pemesanan.getKodeRak());
        values.put(KEY_PENERIMA, pemesanan.getPenerima());
        values.put(KEY_ALAMAT, pemesanan.getAlamat());
        values.put(KEY_KECAMATAN, pemesanan.getKecamatan());
        values.put(KEY_TANGGAL_MASUK, pemesanan.getTanggalMasuk());
        values.put(KEY_TANGGAL_KELUAR, pemesanan.getTanggalKeluar());
        values.put(KEY_SINKRONISASI, pemesanan.getSinkronisasi());
        values.put(KEY_TERAKHIR_DIUBAH, pemesanan.getTerakhirDiubah());

        return db.insert(TABLE_PEMESANAN, null, values);
    }

    /**
     * Method to insert barang data into SQLite database.
     *
     * @param barang Barang data that was inserted.
     * @return Return status of adding process.
     */
    public long addBarang(Barang barang)
    {
        db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_KODE, barang.getKode());
        values.put(KEY_NAMA,barang.getNama());
        values.put(KEY_TIPE, barang.getTipe());
        values.put(KEY_IS_BERATURAN, barang.getIsBeraturan());

        return db.insert(TABLE_BARANG, null, values);
    }

    /**
     * Method to insert rak data into SQLite database.
     *
     * @param rak Rak data that was inserted.
     * @return Return status of adding process.
     */
    public long addRak(Rak rak)
    {
        db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_KODE, rak.getKode());
        values.put(KEY_KODE_GUDANG, rak.getKodeGudang());
        values.put(KEY_TIPE, rak.getTipe());
        values.put(KEY_STATUS_PENUH, rak.getStatusPenuh());
        values.put(KEY_SINKRONISASI, rak.getSinkronisasi());

        return db.insert(TABLE_RAK, null, values);
    }

    /**
     * Method to insert gudang data into SQLite database.
     *
     * @param gudang Gudang data that was inserted.
     * @return Return status of adding process.
     */
    public long addGudang(Gudang gudang)
    {
        db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_KODE, gudang.getKode());
        values.put(KEY_LOKASI, gudang.getLokasi());

        return db.insert(TABLE_GUDANG, null, values);
    }

    /**
     * Method to insert petugas data into SQLite database.
     *
     * @param petugas Petugas data that was inserted.
     * @return Return status of adding process.
     */
    public long addPetugas(Petugas petugas)
    {
        db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_KODE, petugas.getKode());
        values.put(KEY_NAMA, petugas.getNama());
        values.put(KEY_IS_ADMIN, petugas.getIsAdmin());
        values.put(KEY_USERNAME,petugas.getUsername());
        values.put(KEY_KATA_SANDI, petugas.getKataSandi());
        values.put(KEY_KODE_GUDANG, petugas.getKodeGudang());

        return db.insert(TABLE_PETUGAS, null, values);
    }

    /**
     * Method to retrieve pemesanan data with specific kode and kode gudang.
     *
     * @param kode Kode pemesanan of pemesanan data.
     * @return Return specific pemesanan data.
     */
    public Pemesanan getPemesanan(String kode)
    {
        String selectQuery;

        db = this.getReadableDatabase();
        if(SessionManager.getInstance(context).isLoggedIn()) {
            String kodeGudang = getKodeGudang();
            selectQuery =
                    "SELECT * FROM " + TABLE_PEMESANAN +
                            " WHERE " + KEY_KODE + " = \"" + kode + "\" AND " +
                            KEY_KODE_GUDANG + " = \"" + kodeGudang + "\";";
        }
        else {
            selectQuery =
                    "SELECT * FROM " + TABLE_PEMESANAN +
                            " WHERE " + KEY_KODE + " = \"" + kode + "\" ;";
        }

//        Log.d(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        if(c != null && c.getCount() > 0) {
            c.moveToFirst();
        }

        if(c.getCount() == 0) {
            return null;
        }

        Pemesanan pemesanan = new Pemesanan();
        pemesanan.setKode(c.getString(c.getColumnIndex(KEY_KODE)));
        pemesanan.setKodePetugas(c.getInt(c.getColumnIndex(KEY_KODE_PETUGAS)));
        pemesanan.setKodeGudang(c.getString(c.getColumnIndex(KEY_KODE_GUDANG)));
        pemesanan.setKodeBarang(c.getString(c.getColumnIndex(KEY_KODE_BARANG)));
        pemesanan.setKodeRak(c.getString(c.getColumnIndex(KEY_KODE_RAK)));
        pemesanan.setPenerima(c.getString(c.getColumnIndex(KEY_PENERIMA)));
        pemesanan.setAlamat(c.getString(c.getColumnIndex(KEY_ALAMAT)));
        pemesanan.setKecamatan(c.getString(c.getColumnIndex(KEY_KECAMATAN)));
        pemesanan.setTanggalMasuk(c.getString(c.getColumnIndex(KEY_TANGGAL_MASUK)));
        pemesanan.setTanggalKeluar(c.getString(c.getColumnIndex(KEY_TANGGAL_KELUAR)));
        pemesanan.setSinkronisasi(c.getInt(c.getColumnIndex(KEY_SINKRONISASI)));
        pemesanan.setTerakhirDiubah(c.getString(c.getColumnIndex(KEY_TERAKHIR_DIUBAH)));
        c.close();
        return pemesanan;
    }

    /**
     * Method to retrieve barang data with specific kode.
     *
     * @param kode Kode of barang data.
     * @return Return specific barang data.
     */
    public Barang getBarang (String kode)
    {
        db = this.getReadableDatabase();

        String selectQuery =
                "SELECT * FROM " + TABLE_BARANG +
                        " WHERE " + KEY_KODE + " = \"" + kode + "\";";

//        Log.d(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        if(c != null && c.getCount() > 0) {
            c.moveToFirst();
        }

        if(c.getCount() == 0) {
            return null;
        }

        String nama = c.getString(c.getColumnIndex(KEY_NAMA));
        String tipe = c.getString(c.getColumnIndex(KEY_TIPE));
        int isBeraturan = c.getInt(c.getColumnIndex(KEY_IS_BERATURAN));
        c.close();
        Barang barang = new Barang(kode, nama, tipe, isBeraturan);
        return barang;
    }

    /**
     * Method to retrieve rak data with specific kode.
     *
     * @param kode Kode of rak data.
     * @return Return rak data.
     */
    public Rak getRak(String kode)
    {
        String selectQuery="";
        String kodeGudang="";

        db = this.getReadableDatabase();

        if(SessionManager.getInstance(context).isLoggedIn()){
            kodeGudang = getKodeGudang();
            selectQuery =
                    "SELECT * FROM " + TABLE_RAK +
                            " WHERE " + KEY_KODE + " = \"" + kode + "\" AND " +
                            KEY_KODE_GUDANG + " = \"" + kodeGudang + "\";";
        }
        else{
            selectQuery =
                    "SELECT * FROM " + TABLE_RAK +
                            " WHERE " + KEY_KODE + " = \"" + kode + "\";";
        }

//        Log.d(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        if(c != null && c.getCount() > 0) {
            c.moveToFirst();
        }

        if(c.getCount() == 0) {
            return null;
        }

        String tipe = c.getString(c.getColumnIndex(KEY_TIPE));
        int statusPenuh = c.getInt(c.getColumnIndex(KEY_STATUS_PENUH));

        if(kodeGudang.equals("")){
            kodeGudang = c.getString(c.getColumnIndex(KEY_KODE_GUDANG));
        }
        c.close();
        Rak rak = new Rak(kode, kodeGudang, tipe, statusPenuh);
        return rak;
    }

    /**
     * Method to retrieve gudang data with specific kode.
     *
     * @param kode Kode of gudang data.
     * @return Return gudang data.
     */
    public Gudang getGudang(String kode)
    {
        db = this.getReadableDatabase();

        String selectQuery =
                "SELECT * FROM " + TABLE_GUDANG +
                        " WHERE " + KEY_KODE + " = \"" + kode + "\";";

//        Log.d(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        if(c != null && c.getCount() > 0) {
            c.moveToFirst();
        }

        if(c.getCount() == 0) {
            return null;
        }

        String lokasi = c.getString(c.getColumnIndex(KEY_LOKASI));
        c.close();
        Gudang gudang = new Gudang(kode, lokasi);
        return gudang;
    }

    /**
     * Method to retrieve kode petugas.
     *
     * @return Return kode petugas.
     */
    public int getKodePetugas()
    {
        return Integer.parseInt(SessionManager.getInstance(context).getUserDetails().get("kode"));
    }

    /**
     * Method to retrieve kode gudang with specific kode petugas.
     *
     * @return Return petugas data.
     */
    public String getKodeGudang()
    {
        int kodePetugas = getKodePetugas();

        db = this.getReadableDatabase();

        String selectQuery =
                "SELECT * FROM " + TABLE_PETUGAS +
                        " WHERE " + KEY_KODE + " = " + kodePetugas + ";";

//        Log.d(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        if(c != null && c.getCount() > 0) {
            c.moveToFirst();
        }

        if(c.getCount() == 0) {
            return null;
        }

        String kodeGudang = c.getString(c.getColumnIndex(KEY_KODE_GUDANG));
        c.close();
        return kodeGudang;
    }

    /**
     * Method to retrieve petugas data with specific kode.
     *
     * @param kode Kode petugas.
     * @return Return petugas data.
     */
    public Petugas getPetugas(int kode)
    {
        db = this.getReadableDatabase();

        String selectQuery =
                "SELECT * FROM " + TABLE_PETUGAS +
                        " WHERE " + KEY_KODE + " = " + kode + ";";

//        Log.d(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        if(c != null && c.getCount() > 0) {
            c.moveToFirst();
        }

        if(c.getCount() == 0) {
            return null;
        }

        String nama = c.getString(c.getColumnIndex(KEY_NAMA));
        int isAdmin = c.getInt(c.getColumnIndex(KEY_IS_ADMIN));
        String username = c.getString(c.getColumnIndex(KEY_USERNAME));
        String kataSandi = c.getString(c.getColumnIndex(KEY_KATA_SANDI));
        String kodeGudang = c.getString(c.getColumnIndex(KEY_KODE_GUDANG));

        c.close();
        Petugas petugas = new Petugas(kode, nama, isAdmin, username, kataSandi, kodeGudang);
        return petugas;
    }

    /**
     * Method to count barang data.
     *
     * @return Return count of barang data.
     */
    public int countBarang()
    {
        db = this.getReadableDatabase();

        String selectQuery = "SELECT * FROM " + TABLE_BARANG;

//        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);
        int banyakBarang = c.getCount();
        c.close();
        return banyakBarang;
    }

    /**
     * Method to count gudang data.
     *
     * @return Return count of gudang data
     */
    public int countGudang()
    {
        db = this.getReadableDatabase();

        String selectQuery = "SELECT * FROM " + TABLE_GUDANG;

//        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);
        int banyakGudang = c.getCount();
        c.close();
        return banyakGudang;
    }

    /**
     * Method to count pemesanan data.
     *
     * @return Return count of pemesanan data.
     */
    public int countPemesanan()
    {
        db = this.getReadableDatabase();

        String selectQuery = "SELECT * FROM " + TABLE_PEMESANAN;

//        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);
        int banyakPemesanan = c.getCount();
        c.close();
        return banyakPemesanan;
    }

    /**
     * Method to count petugas data.
     *
     * @return Return count of petugas data
     */
    public int countPetugas()
    {
        db = this.getReadableDatabase();

        String selectQuery = "SELECT * FROM " + TABLE_PETUGAS;

//        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);
        int banyakPetugas = c.getCount();
        c.close();
        return banyakPetugas;
    }

    /**
     * Method to count rak data.
     *
     * @return Return count of rak data.
     */
    public int countRak()
    {
        db = this.getReadableDatabase();

        String selectQuery = "SELECT * FROM " + TABLE_RAK;

//        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);
        int banyakRak = c.getCount();
        c.close();
        return banyakRak;
    }

    /**
     * Method to retrieve pemesanan data with specific kode barang.
     *
     * @param kodeBarang
     * @return
     */
    public List<Pemesanan> getPemesananBarang(String kodeBarang)
    {
        String kodeGudang = getKodeGudang();

        SQLiteDatabase db = this.getReadableDatabase();

        List<Pemesanan> listPemesanan = new ArrayList<Pemesanan>();
        String selectQuery =
                "SELECT * FROM " + TABLE_PEMESANAN +
                        " WHERE " + KEY_KODE_BARANG + " = \"" + kodeBarang + "\" AND " +
                        KEY_KODE_GUDANG + " = \"" + kodeGudang + "\" AND " +
                        KEY_TANGGAL_KELUAR + " IS NULL;";

//        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        // Looping through all rows and adding to list
        if(c.moveToFirst()) {
            do {
                Pemesanan pemesanan = new Pemesanan();
                pemesanan.setKode(c.getString(c.getColumnIndex(KEY_KODE)));
                pemesanan.setKodePetugas(c.getInt(c.getColumnIndex(KEY_KODE_PETUGAS)));
                pemesanan.setKodeGudang(c.getString(c.getColumnIndex(KEY_KODE_GUDANG)));
                pemesanan.setKodeBarang(c.getString(c.getColumnIndex(KEY_KODE_BARANG)));
                pemesanan.setKodeRak(c.getString(c.getColumnIndex(KEY_KODE_RAK)));
                pemesanan.setPenerima(c.getString(c.getColumnIndex(KEY_PENERIMA)));
                pemesanan.setAlamat(c.getString(c.getColumnIndex(KEY_ALAMAT)));
                pemesanan.setKecamatan(c.getString(c.getColumnIndex(KEY_KECAMATAN)));
                pemesanan.setTanggalMasuk(c.getString(c.getColumnIndex(KEY_TANGGAL_MASUK)));
                pemesanan.setTanggalKeluar(c.getString(c.getColumnIndex(KEY_TANGGAL_KELUAR)));
                pemesanan.setSinkronisasi(c.getInt(c.getColumnIndex(KEY_SINKRONISASI)));
                pemesanan.setTerakhirDiubah(c.getString(c.getColumnIndex(KEY_TERAKHIR_DIUBAH)));

                listPemesanan.add(pemesanan);
            }
            while(c.moveToNext());
        }
        c.close();
        return listPemesanan;
    }

    /**
     * Method to count barang data in inventory with specific kode.
     *
     * @return Return count of specific barang data in inventory.
     */
    public int getBarangCount(String kode)
    {
        String kodeGudang = getKodeGudang();

        String countQuery =
                "SELECT * FROM " + TABLE_PEMESANAN +
                        " WHERE " + KEY_KODE_BARANG + " = \"" + kode + "\" AND " +
                        KEY_KODE_GUDANG + " = \"" + kodeGudang + "\" AND " +
                        KEY_TANGGAL_KELUAR + " IS NULL;";

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();

        return count;
    }

    /**
     * Method to count barang in rak data with specific kode.
     *
     * @return Return count barang in rak of specific rak data.
     */
    public int getBarangRakCount(String kode)
    {
        String kodeGudang = getKodeGudang();

        String countQuery =
                "SELECT * FROM " + TABLE_PEMESANAN +
                        " WHERE " + KEY_KODE_GUDANG + " = \"" + kodeGudang + "\" AND " +
                        KEY_KODE_RAK + " = \"" + kode + "\" AND " +
                        KEY_TANGGAL_KELUAR + " IS NULL;";

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();

        return count;
    }

    /**
     * Method to count pemesanan data with specific kode.
     *
     * @return Return count of specific pemesanan data.
     */
    public int getPemesananCount(String kode)
    {
        String kodeGudang = getKodeGudang();

        String countQuery =
                "SELECT * FROM " + TABLE_PEMESANAN +
                        " WHERE " + KEY_KODE + " = \"" + kode + "\" AND " +
                        KEY_KODE_GUDANG + " = \"" + kodeGudang + "\";";

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();

        return count;
    }

    /**
     * Method to retrieve all barang data in inventory.
     *
     * @return Return all barang data in inventory.
     */
    public List<Barang> getAllBarang()
    {
        String kodeGudang = getKodeGudang();

        SQLiteDatabase db = this.getReadableDatabase();

        List<Barang> listBarang = new ArrayList<Barang>();
        String selectQuery =
                "SELECT *" +
                        " FROM " + TABLE_BARANG + " b" +
                        " WHERE " + KEY_KODE +
                        " IN ( SELECT " + KEY_KODE_BARANG +
                        " FROM " + TABLE_PEMESANAN + " p" +
                        " WHERE p." + KEY_KODE_BARANG + " = b." + KEY_KODE + " AND " +
                        "p." + KEY_KODE_GUDANG + " = \"" + kodeGudang + "\");";

        Cursor c = db.rawQuery(selectQuery, null);
        // Looping through all rows and adding to list
        if(c.moveToFirst()) {
            do {
                Barang barang = new Barang();
                barang.setKode(c.getString(c.getColumnIndex(KEY_KODE)));
                barang.setNama(c.getString(c.getColumnIndex(KEY_NAMA)));
                barang.setTipe(c.getString(c.getColumnIndex(KEY_TIPE)));
                barang.setIsBeraturan(c.getInt(c.getColumnIndex(KEY_IS_BERATURAN)));

                listBarang.add(barang);
            }
            while(c.moveToNext());
        }

        return listBarang;
    }

    /**
     * Method to retrieve all barang data in inventory.
     *
     * @return Return all barang data in inventory.
     */
    public List<Barang> getAllBarangRak(String kodeRak)
    {
        String kodeGudang = getKodeGudang();

        db = this.getReadableDatabase();

        List<Barang> listBarang = new ArrayList<Barang>();
        String selectQuery =
                "SELECT *" +
                        " FROM " + TABLE_BARANG + " b" +
                        " WHERE " + KEY_KODE +
                        " IN ( SELECT " + KEY_KODE_BARANG +
                        " FROM " + TABLE_PEMESANAN + " p" +
                        " WHERE p." + KEY_KODE_BARANG + " = b." + KEY_KODE + " AND " +
                        "p." + KEY_KODE_GUDANG + " = \"" + kodeGudang + "\" AND " +
                        "p." + KEY_KODE_RAK + " = \"" + kodeRak + "\" AND " +
                        "p." + KEY_TANGGAL_KELUAR + " IS NULL);";

        Cursor c = db.rawQuery(selectQuery, null);

        // Looping through all rows and adding to list
        if(c.moveToFirst()) {
            do {
                Barang barang = new Barang();
                barang.setKode(c.getString(c.getColumnIndex(KEY_KODE)));
                barang.setNama(c.getString(c.getColumnIndex(KEY_NAMA)));
                barang.setTipe(c.getString(c.getColumnIndex(KEY_TIPE)));
                barang.setIsBeraturan(c.getInt(c.getColumnIndex(KEY_IS_BERATURAN)));

                listBarang.add(barang);
            }
            while(c.moveToNext());
        }

        db = this.getWritableDatabase();

        return listBarang;
    }

    /**
     * Method to retrieve all pemesanan data in database.
     *
     * @return Return list of all pemesanan data.
     */
    public List<Pemesanan> getAllPemesanan()
    {
        String kodeGudang = getKodeGudang();

        db = this.getReadableDatabase();

        List<Pemesanan> listPemesanan = new ArrayList<Pemesanan>();
        String selectQuery =
                "SELECT * FROM " + TABLE_PEMESANAN +
                        " WHERE " + KEY_KODE_GUDANG + " = \"" + kodeGudang + "\";";

//        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        // Looping through all rows and adding to list
        if(c.moveToFirst()) {
            do {
                Pemesanan pemesanan = new Pemesanan();
                pemesanan.setKode(c.getString(c.getColumnIndex(KEY_KODE)));
                pemesanan.setKodePetugas(c.getInt(c.getColumnIndex(KEY_KODE_PETUGAS)));
                pemesanan.setKodeGudang(c.getString(c.getColumnIndex(KEY_KODE_GUDANG)));
                pemesanan.setKodeBarang(c.getString(c.getColumnIndex(KEY_KODE_BARANG)));
                pemesanan.setKodeRak(c.getString(c.getColumnIndex(KEY_KODE_RAK)));
                pemesanan.setPenerima(c.getString(c.getColumnIndex(KEY_PENERIMA)));
                pemesanan.setAlamat(c.getString(c.getColumnIndex(KEY_ALAMAT)));
                pemesanan.setKecamatan(c.getString(c.getColumnIndex(KEY_KECAMATAN)));
                pemesanan.setTanggalMasuk(c.getString(c.getColumnIndex(KEY_TANGGAL_MASUK)));
                pemesanan.setTanggalKeluar(c.getString(c.getColumnIndex(KEY_TANGGAL_KELUAR)));
                pemesanan.setSinkronisasi(c.getInt(c.getColumnIndex(KEY_SINKRONISASI)));
                pemesanan.setTerakhirDiubah(c.getString(c.getColumnIndex(KEY_TERAKHIR_DIUBAH)));

                listPemesanan.add(pemesanan);
            }
            while(c.moveToNext());
        }

        db = this.getWritableDatabase();

        return listPemesanan;
    }

    /**
     * Method to retrieve all pemesanan data in a rak.
     *
     * @return Return all pemesanan data in a rak.
     */
    public List<Pemesanan> getAllPemesananRak(String kodeRak)
    {
        String kodeGudang = getKodeGudang();

        db = this.getReadableDatabase();

        List<Pemesanan> listPemesanan = new ArrayList<Pemesanan>();
        String selectQuery =
                "SELECT * FROM " + TABLE_PEMESANAN +
                        " WHERE " + KEY_KODE_GUDANG + " = \"" + kodeGudang + "\" AND " +
                        KEY_KODE_RAK + " = \"" + kodeRak + "\" AND " +
                        KEY_TANGGAL_KELUAR + " IS NULL;";

        Cursor c = db.rawQuery(selectQuery, null);

        // Looping through all rows and adding to list
        if(c.moveToFirst()) {
            do {
                Pemesanan pemesanan = new Pemesanan();
                pemesanan.setKode(c.getString(c.getColumnIndex(KEY_KODE)));
                pemesanan.setKodePetugas(c.getInt(c.getColumnIndex(KEY_KODE_PETUGAS)));
                pemesanan.setKodeGudang(c.getString(c.getColumnIndex(KEY_KODE_GUDANG)));
                pemesanan.setKodeBarang(c.getString(c.getColumnIndex(KEY_KODE_BARANG)));
                pemesanan.setKodeRak(c.getString(c.getColumnIndex(KEY_KODE_RAK)));
                pemesanan.setPenerima(c.getString(c.getColumnIndex(KEY_PENERIMA)));
                pemesanan.setAlamat(c.getString(c.getColumnIndex(KEY_ALAMAT)));
                pemesanan.setKecamatan(c.getString(c.getColumnIndex(KEY_KECAMATAN)));
                pemesanan.setTanggalMasuk(c.getString(c.getColumnIndex(KEY_TANGGAL_MASUK)));
                pemesanan.setTanggalKeluar(c.getString(c.getColumnIndex(KEY_TANGGAL_KELUAR)));
                pemesanan.setSinkronisasi(c.getInt(c.getColumnIndex(KEY_SINKRONISASI)));
                pemesanan.setTerakhirDiubah(c.getString(c.getColumnIndex(KEY_TERAKHIR_DIUBAH)));

                listPemesanan.add(pemesanan);
            }
            while(c.moveToNext());
        }

        db = this.getWritableDatabase();

        return listPemesanan;
    }

    /**
     * Method to retrieve all rak data in database.
     *
     * @return Return list of all rak data.
     */
    public List<Rak> getAllRak()
    {
        String kodeGudang = getKodeGudang();

        db = this.getReadableDatabase();

        List<Rak> listRak = new ArrayList<Rak>();
        String selectQuery =
                "SELECT * FROM " + TABLE_RAK +
                        " WHERE " + KEY_KODE_GUDANG + " = \"" + kodeGudang + "\";";

//        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        // Looping through all rows and adding to list
        if(c.moveToFirst()) {
            do {
                Rak rak = new Rak();
                rak.setKode(c.getString(c.getColumnIndex(KEY_KODE)));
                rak.setKodeGudang(c.getString(c.getColumnIndex(KEY_KODE_GUDANG)));
                rak.setTipe(c.getString(c.getColumnIndex(KEY_TIPE)));
                rak.setStatusPenuh(c.getInt(c.getColumnIndex(KEY_STATUS_PENUH)));
                rak.setSinkronisasi(c.getInt(c.getColumnIndex(KEY_SINKRONISASI)));

                listRak.add(rak);
            }
            while(c.moveToNext());
        }

        db = this.getWritableDatabase();

        return listRak;
    }

    /**
     * Method to update pemesanan on the database sqlite.
     *
     * @param pemesanan Pemesanan data that was updated.
     * @return Return status of update process.
     */
    public int updatePemesanan(Pemesanan pemesanan)
    {
        db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_KODE_GUDANG, pemesanan.getKodeGudang());
        values.put(KEY_KODE_BARANG, pemesanan.getKodeBarang());
        values.put(KEY_KODE_RAK, pemesanan.getKodeRak());
        values.put(KEY_PENERIMA, pemesanan.getPenerima());
        values.put(KEY_ALAMAT, pemesanan.getAlamat());
        values.put(KEY_KECAMATAN, pemesanan.getKecamatan());
        values.put(KEY_TANGGAL_MASUK, pemesanan.getTanggalMasuk());
        values.put(KEY_TANGGAL_KELUAR, pemesanan.getTanggalKeluar());
        values.put(KEY_SINKRONISASI, pemesanan.getSinkronisasi());
        values.put(KEY_TERAKHIR_DIUBAH, pemesanan.getTerakhirDiubah());
//        Log.e(LOG, "Masuk tanggal keluar: " + (pemesanan.getTanggalKeluar() == (null)));
        // Updating Row
        return db.update(TABLE_PEMESANAN, values, KEY_KODE + " = ? AND " + KEY_KODE_PETUGAS + " = ?",
                new String[] { String.valueOf(pemesanan.getKode()),
                        String.valueOf(pemesanan.getKodePetugas()) });
    }

    /**
     * Method to get database instance.
     *
     * @param context
     * @return Return instance of the database.
     */
    public static synchronized DatabaseController getInstance(Context context)
    {
        if (instance == null) {
            instance = new DatabaseController(context, DATABASE_NAME, null, DATABASE_VERSION);
            db = instance.getWritableDatabase();
        }
        return instance;
    }

    /**
     *  Method to close opened database instance.
     */
    @Override
    public synchronized void close()
    {
        if (instance != null)
            db.close();
    }

    /**
     * Method to syncronize the database.
     *
     * @param context
     */
    public void syncDatabase(final Context context)
    {
        StringRequest stringRequestAll = new StringRequest(Request.Method.GET,
                SERVER_URL + "/all",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response)
                    {
                        JSONObject jsonObject = null;

                        try {
                            jsonObject = new JSONObject(response);
                            String Response = jsonObject.getString("response");

                            if (Response.equals("OK")) {
                                syncPemesanan(jsonObject.getJSONArray("pemesanan"));
                                syncGudang(jsonObject.getJSONArray("gudang"));
                                syncRak(jsonObject.getJSONArray("rak"));
                                syncBarang(jsonObject.getJSONArray("barang"));
                                syncPetugas(jsonObject.getJSONArray("petugas"));
                            }
                            else {
                                Log.e(LOG, "Pemesanan response is " + Response);
                            }
                        } catch (JSONException e) {
//                            Log.e(LOG, e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
//                        Log.e(LOG, error.toString());
                    }
                }
        );
        MySingleton.getInstance(context).addToRequestQue(stringRequestAll);
    }

    /**
     * Method to syncronize barang data.
     *
     * @param listBarang
     */
    private void syncBarang(JSONArray listBarang) throws JSONException
    {
        for(int i = 0; i < listBarang.length(); i++) {
            JSONObject barang = listBarang.getJSONObject(i);

            String kodeBarang = barang.getString(KEY_KODE);
            String namaBarang = barang.getString(KEY_NAMA);
            String tipeBarang = barang.getString(KEY_TIPE);
            int isBeraturan = barang.getInt(KEY_IS_BERATURAN);

            Barang dataBarang = new Barang(kodeBarang, namaBarang, tipeBarang, isBeraturan);
            Barang cekBarang = getBarang(kodeBarang);

            if(cekBarang == null){
                addBarang(dataBarang);
            }
            else {
                updateBarang(dataBarang);
            }
        }
    }

    /**
     * Method to update the specific barang data.
     *
     * @param barang Barang data that was updated.
     * @return Return status of update process.
     */
    public int updateBarang(Barang barang)
    {
        db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAMA, barang.getNama());
        values.put(KEY_TIPE, barang.getTipe());
        values.put(KEY_IS_BERATURAN, barang.getIsBeraturan());

        return db.update(TABLE_BARANG, values, KEY_KODE + " = ?",
                new String[] { String.valueOf(barang.getKode()) });
    }

    /**
     * Method to syncronize rak data.
     *
     * @param listRak
     */
    private void syncRak(JSONArray listRak) throws JSONException
    {
        for(int i = 0; i < listRak.length(); i++) {
            JSONObject rak = listRak.getJSONObject(i);

            String kodeRak = rak.getString(KEY_KODE);
            String kodeGudang = rak.getString(KEY_KODE_GUDANG);
            String tipeRak = rak.getString(KEY_TIPE);
            int statusPenuh = rak.getInt(KEY_STATUS_PENUH);

            Rak dataRak = new Rak(kodeRak, kodeGudang, tipeRak, statusPenuh);
            Rak cekRak = getRak(kodeRak);

            dataRak.setSinkronisasi(SYNC_STATUS_OK);
            if(cekRak == null) {
                Log.d(LOG, "data add " + addRak(dataRak));
            }
            else {
                Log.d(LOG, "data update " + updateRak(dataRak));
            }
        }
    }

    /**
     * Method to update specified rak data on database.
     *
     * @param rak
     * @return Return status of update process.
     */
    public int updateRak(Rak rak)
    {
        db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TIPE, rak.getTipe());
        values.put(KEY_STATUS_PENUH, rak.getStatusPenuh());
        values.put(KEY_SINKRONISASI, rak.getSinkronisasi());

        // Updating Row
        return db.update(TABLE_RAK, values, KEY_KODE + " = ? AND " + KEY_KODE_GUDANG + " = ?",
                new String[] { String.valueOf(rak.getKode()) ,
                        String.valueOf(rak.getKodeGudang())});
    }

    /**
     * Method to delete specified rak data on database.
     *
     * @param rak
     * @return Return status of update process.
     */
    public int deleteRak(Rak rak)
    {
        db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TIPE, rak.getTipe());
        values.put(KEY_STATUS_PENUH, rak.getStatusPenuh());
        values.put(KEY_SINKRONISASI, rak.getSinkronisasi());

        return db.delete(TABLE_RAK, KEY_KODE + " = ? AND " + KEY_KODE_GUDANG + " = ?",
                new String[] { String.valueOf(rak.getKode()) ,
                        String.valueOf(rak.getKodeGudang())});
    }

    /**
     * Method to update data gudang on SQLite.
     *
     * @param listGudang Updated data gudang.
     * @throws JSONException
     */
    private void syncGudang(JSONArray listGudang) throws JSONException
    {
        for(int i = 0; i < listGudang.length(); i++) {
            JSONObject gudang = listGudang.getJSONObject(i);

            String kodeGudang = gudang.getString(KEY_KODE);
            String lokasiGudang = gudang.getString(KEY_LOKASI);

            Gudang dataGudang = new Gudang(kodeGudang, lokasiGudang);
            Gudang cekGudang = getGudang(kodeGudang);

            if(cekGudang == null){
                addGudang(dataGudang);
            }
            else {
                updateGudang(dataGudang);
            }
        }
    }

    /**
     * Method to update specific data gudang on SQLite.
     *
     * @param gudang Updated specified data gudang.
     * @return Return status of update process.
     */
    public int updateGudang(Gudang gudang)
    {
        db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_LOKASI, gudang.getLokasi());

        // Updating Row
        return db.update(TABLE_GUDANG, values, KEY_KODE + " = ?",
                new String[] { String.valueOf(gudang.getKode()) });
    }

    /**
     * Method to update data petugas on SQLite.
     *
     * @param listPetugas Updated data petugas.
     * @throws JSONException
     */
    private void syncPetugas(JSONArray listPetugas) throws JSONException
    {
        for(int i = 0; i < listPetugas.length(); i++) {
            JSONObject petugas = listPetugas.getJSONObject(i);

            int kodePetugas = petugas.getInt(KEY_KODE);
            String namaPetugas = petugas.getString(KEY_NAMA);
            int isAdmin = petugas.getInt(KEY_IS_ADMIN);
            String username = petugas.getString(KEY_USERNAME);
            String kataSandi = petugas.getString(KEY_KATA_SANDI);
            String kodeGudang = petugas.getString(KEY_KODE_GUDANG);

            Petugas dataPetugas = new Petugas(kodePetugas, namaPetugas, isAdmin, username, kataSandi, kodeGudang);
            Petugas cekPetugas = getPetugas(kodePetugas);

            if(cekPetugas == null){
                addPetugas(dataPetugas);
            }
            else {
                updatePetugas(dataPetugas);
            }
        }
    }

    /**
     * Method to update specific data petugas on SQLite.
     *
     * @param petugas Updated specified data petugas.
     * @return Return status of update process.
     */
    public int updatePetugas(Petugas petugas)
    {
        db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAMA, petugas.getNama());
        values.put(KEY_IS_ADMIN, petugas.getIsAdmin());
        values.put(KEY_USERNAME, petugas.getUsername());
        values.put(KEY_KATA_SANDI, petugas.getKataSandi());
        values.put(KEY_KODE_GUDANG, petugas.getKodeGudang());

        // Updating Row
        return db.update(TABLE_PETUGAS, values, KEY_KODE + " = ?",
                new String[] { String.valueOf(petugas.getKode()) });
    }

    /**
     * Method to syncronize pemesanan data.
     *
     * @param listPemesanan
     * @throws JSONException
     */
    private void syncPemesanan(JSONArray listPemesanan) throws JSONException
    {
        for(int i = 0; i < listPemesanan.length(); i++) {
            JSONObject pemesanan = listPemesanan.getJSONObject(i);

            String kodePemesanan = pemesanan.getString(KEY_KODE);
            int kodePetugas = pemesanan.getInt(KEY_KODE_PETUGAS);
            String kodeGudang = pemesanan.getString(KEY_KODE_GUDANG);
            String kodeBarang = pemesanan.getString(KEY_KODE_BARANG);
            String kodeRak = pemesanan.getString(KEY_KODE_RAK);
            String penerima = pemesanan.getString(KEY_PENERIMA);
            String alamat = pemesanan.getString(KEY_ALAMAT);
            String kecamatan = pemesanan.getString(KEY_KECAMATAN);
            String tanggalMasuk = pemesanan.getString(KEY_TANGGAL_MASUK);
            String tanggalKeluar = pemesanan.getString(KEY_TANGGAL_KELUAR);
            String terakhirDiubah = pemesanan.getString(KEY_TERAKHIR_DIUBAH);

            Pemesanan dataPemesanan;

            if(tanggalKeluar.equals("null")){
                dataPemesanan = new Pemesanan(kodePemesanan, kodePetugas, kodeGudang, kodeBarang, kodeRak,
                        penerima, alamat, kecamatan, tanggalMasuk, null);
            }
            else{
                dataPemesanan = new Pemesanan(kodePemesanan, kodePetugas, kodeGudang, kodeBarang, kodeRak,
                        penerima, alamat, kecamatan, tanggalMasuk, tanggalKeluar);
            }

            Pemesanan cekPemesanan = getPemesanan(kodePemesanan);

            Log.d(LOG, "tanggal keluar: " + tanggalKeluar);

            dataPemesanan.setSinkronisasi(SYNC_STATUS_OK);
            if (cekPemesanan == null) {
                addPemesanan(dataPemesanan);
            }
            else {
                Log.d(LOG, terakhirDiubah + " > " + cekPemesanan.getTerakhirDiubah() + " = " +
                        terakhirDiubah.compareTo(cekPemesanan.getTerakhirDiubah()));
                if(terakhirDiubah.compareTo(cekPemesanan.getTerakhirDiubah()) > 0){
                    updatePemesanan(dataPemesanan);
                }
            }
        }
    }

    /**
     * Get specific rak data from server
     *
     * @param rak
     * @param gudang
     */
    public void syncRak(String rak, String gudang) {
        StringRequest stringRequestRak = new StringRequest(Request.Method.GET,
                SERVER_URL + "/rak/"+rak+"/"+gudang,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response)
                    {
                        JSONObject jsonObject = null;

                        try {
                            jsonObject = new JSONObject(response);
                            String Response = jsonObject.getString("response");

                            if (Response.equals("OK")) {
                                JSONObject dataRak = jsonObject.getJSONObject("rak");
                                JSONArray listRak = new JSONArray();
                                listRak.put(dataRak);
                                syncRak(listRak);
                            }
                            else {
                                Log.e(LOG, "Rak response is " + Response);
                            }
                        } catch (JSONException e) {
//                            Log.e(LOG, e.toString());
//                            Log.e(LOG, e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
//                        Log.e(LOG, error.toString());
//                        Log.e(LOG, error.getMessage());
                    }
                }
        );
        MySingleton.getInstance(context).addToRequestQue(stringRequestRak);
    }

    /**
     * Get specific pemesanan data from server
     * @param pemesanan
     * @param gudang
     */
    public void syncPemesanan(String pemesanan, String gudang) {
        StringRequest stringRequestPemesanan = new StringRequest(Request.Method.GET,
                SERVER_URL + "/pemesanan/"+pemesanan+"/"+gudang,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response)
                    {
                        JSONObject jsonObject = null;

                        try {
                            jsonObject = new JSONObject(response);
                            String Response = jsonObject.getString("response");

                            if (Response.equals("OK")) {
                                JSONObject dataPemesanan = jsonObject.getJSONObject("pemesanan");
                                JSONArray listPemesanan = new JSONArray();
                                listPemesanan.put(dataPemesanan);
                                syncPemesanan(listPemesanan);
                            }
                            else {
                                Log.e(LOG, "Pemesanan response is " + Response);
                            }
                        } catch (JSONException e) {
//                            Log.e(LOG, e.toString());
//                            Log.e(LOG, e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
//                        Log.e(LOG, error.toString());
//                        Log.e(LOG, error.getMessage());
                    }
                }
        );
        MySingleton.getInstance(context).addToRequestQue(stringRequestPemesanan);
    }

    /**
     * Get sepcific barang data from server
     * @param barang
     */
    public void syncBarang(String barang) {
        StringRequest stringRequestBarang = new StringRequest(Request.Method.GET,
                SERVER_URL + "/barang/"+barang,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response)
                    {
                        JSONObject jsonObject = null;

                        try {
                            jsonObject = new JSONObject(response);
                            String Response = jsonObject.getString("response");

                            if (Response.equals("OK")) {
                                JSONObject dataBarang = jsonObject.getJSONObject("barang");
                                JSONArray listBarang = new JSONArray();
                                listBarang.put(dataBarang);
                                syncBarang(listBarang);
                            }
                            else {
                                Log.e(LOG, "Barang response is " + Response);
                            }
                        } catch (JSONException e) {
//                            Log.e(LOG, e.toString());
//                            Log.e(LOG, e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
//                Log.e(LOG, error.toString());
//                Log.e(LOG, error.getMessage());
            }
        }
        );
        MySingleton.getInstance(context).addToRequestQue(stringRequestBarang);
    }

    /**
     * Get specific gudang data from server
     * @param gudang
     */
    public void syncGudang(String gudang) {
        StringRequest stringRequestGudang = new StringRequest(Request.Method.GET,
                SERVER_URL + "/gudang/"+gudang,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response)
                    {
                        JSONObject jsonObject = null;

                        try {
                            jsonObject = new JSONObject(response);
                            String Response = jsonObject.getString("response");

                            if (Response.equals("OK")) {
                                JSONObject dataGudang = jsonObject.getJSONObject("gudang");
                                JSONArray listGudang = new JSONArray();
                                listGudang.put(dataGudang);
                                syncGudang(listGudang);
                            }
                            else {
                                Log.e(LOG, "Gudang response is " + Response);
                            }
                        } catch (JSONException e) {
//                            Log.e(LOG, e.toString());
//                            Log.e(LOG, e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
//                        Log.e(LOG, error.toString());
//                        Log.e(LOG, error.getMessage());
                    }
                }
        );
        MySingleton.getInstance(context).addToRequestQue(stringRequestGudang);
    }

    /**
     * Get specific petugas data from server
     * @param petugas
     */
    public void syncPetugas(String petugas) {
        StringRequest stringRequestPetugas = new StringRequest(Request.Method.GET,
                SERVER_URL + "/petugas/"+petugas,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response)
                    {
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            String Response = jsonObject.getString("response");

                            if (Response.equals("OK")) {
                                JSONObject dataPetugas = jsonObject.getJSONObject("petugas");
                                JSONArray listPetugas = new JSONArray();
                                listPetugas.put(dataPetugas);
                                syncPetugas(listPetugas);
                            }
                            else {
                                Log.e(LOG, "Petugas response is " + Response);
                            }
                        } catch (JSONException e) {
//                            Log.e(LOG, e.toString());
//                            Log.e(LOG, e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
//                Log.e(LOG, error.toString());
//                Log.e(LOG, error.getMessage());
            }
        }
        );
        MySingleton.getInstance(context).addToRequestQue(stringRequestPetugas);
    }

    /**
     * Delete pemesanan data on sqlite
     * @param pemesanan
     * @return status of delete process
     */
    public int deletePemesanan(Pemesanan pemesanan) {
        db = this.getWritableDatabase();

        return db.delete(TABLE_PEMESANAN, KEY_KODE + " = ? AND " + KEY_KODE_PETUGAS + " = ?",
                new String[] { String.valueOf(pemesanan.getKode()),
                        String.valueOf(pemesanan.getKodePetugas()) });
    }

    /**
     * Delete barang data on sqlite
     * @param barang
     * @return status of delete process
     */
    public int deleteBarang(Barang barang) {
        db = this.getWritableDatabase();

        return db.delete(TABLE_BARANG, KEY_KODE + " = ?",
                new String[] { String.valueOf(barang.getKode()) });
    }

    /**
     * Delete gudang data on sqlite
     * @param gudang
     * @return status of delete process
     */
    public int deleteGudang(Gudang gudang) {
        db = this.getWritableDatabase();

        return db.delete(TABLE_GUDANG, KEY_KODE + " = ?",
                new String[] { String.valueOf(gudang.getKode()) });
    }

    /**
     * Delete petugas data on sqlite
     * @param petugas
     * @return status of delete process
     */
    public int deletePetugas(Petugas petugas) {
        db = this.getWritableDatabase();

        return db.delete(TABLE_PETUGAS, KEY_KODE + " = ?",
                new String[] { String.valueOf(petugas.getKode()) });
    }
}