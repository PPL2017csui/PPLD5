package com.goodangcode.goodang;

import android.app.Activity;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.goodangcode.goodang.databasehelper.DatabaseController;
import com.goodangcode.goodang.databasemodel.Pemesanan;
import com.goodangcode.goodang.databasemodel.Rak;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author GoodangCode
 */
public class Syncronization
{
    private Activity activity;

    private Pemesanan pemesanan;
    private Rak rak;

    public Syncronization(Activity activity, Pemesanan pemesanan, String type)
    {
        this.activity = activity;
        this.pemesanan = pemesanan;

        if(NetworkMonitor.checkNetworkConnection(activity)) {
            savePemesananToServer(type);
        }
    }

    public Syncronization(Activity activity, Rak rak, String type)
    {
        this.activity = activity;
        this.rak = rak;

        if(NetworkMonitor.checkNetworkConnection(activity)) {
            if(type.equals("Delete")) {
                deleteRakInServer();
            }
            else {
                saveRakToServer();
            }
        }
    }

    /**
     * Method is called when saving pemesanan data to server.
     */
    private void savePemesananToServer(String type)
    {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("kode_pemesanan", pemesanan.getKode());
            jsonObject.put("kode_petugas", pemesanan.getKodePetugas());
            jsonObject.put("kode_gudang", pemesanan.getKodeGudang());
            jsonObject.put("kode_barang", pemesanan.getKodeBarang());
            jsonObject.put("kode_rak", pemesanan.getKodeRak());
            jsonObject.put("penerima", pemesanan.getPenerima());
            jsonObject.put("alamat", pemesanan.getAlamat());
            jsonObject.put("kecamatan", pemesanan.getKecamatan());
            jsonObject.put("tanggal_masuk", pemesanan.getTanggalMasuk());
            jsonObject.put("terakhir_diubah", pemesanan.getTerakhirDiubah());

            if(type.equals("In")) {
                jsonObject.put("tanggal_keluar",JSONObject.NULL);
            }
            else if(type.equals("Out")) {
                jsonObject.put("tanggal_keluar", pemesanan.getTanggalKeluar());
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }

        final String requestBody = jsonObject.toString();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                DatabaseController.SERVER_URL+"/pemesanan",
                new Response.Listener<String>()
                {
                    /**
                     * Called when a response is received.
                     *
                     * @param response
                     */
                    @Override
                    public void onResponse(String response)
                    {
//                        Toast.makeText(activity, response, Toast.LENGTH_LONG).show();

                        if(response.equals("200")) {
                            pemesanan.setSinkronisasi(DatabaseController.SYNC_STATUS_OK);
                            DatabaseController db = DatabaseController.getInstance(activity);
                            db.updatePemesanan(pemesanan);
                            db.close();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    /**
                     * Callback method that an error has been occurred with the provided error code and optional user-readable message.
                     *
                     * @param error
                     */
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Toast.makeText(activity, error.toString(), Toast.LENGTH_LONG);
                        error.printStackTrace();
                    }
                }
        )
        {
            /**
             * Gets the content type of body.
             *
             * @return Returns the body content type.
             */
            @Override
            public String getBodyContentType()
            {
                return "application/json; charset=utf-8";
            }

            /**
             * Gets the raw body.
             *
             * @return Returns the raw POST or PUT body to be sent.
             * @throws AuthFailureError
             */
            @Override
            public byte[] getBody() throws AuthFailureError
            {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                }
                catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }

            /**
             * Subclasses must implement this to parse the raw network response and return an appropriate response type.
             *
             * @param response Response from the network.
             * @return Returns the parsed response, or null in the case of an error.
             */
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response)
            {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                }
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            /**
             * Gets a list of extra HTTP headers to go along with this request. Can throw AuthFailureError
             * as authentication may be required to provide these values.
             *
             * @return Returns a list of extra HTTP headers to go along with this request.
             * @throws AuthFailureError
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };

        MySingleton.getInstance(activity).addToRequestQue(stringRequest);
    }

    /**
     * Method is called when saving rak data to server.
     */
    private void saveRakToServer()
    {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("kode_rak", rak.getKode());
            jsonObject.put("kode_gudang", rak.getKodeGudang());
            jsonObject.put("tipe_rak", rak.getTipe());
            jsonObject.put("status_penuh", rak.getStatusPenuh());
        }
        catch (JSONException e) {
            e.printStackTrace();
        }

        final String requestBody = jsonObject.toString();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                DatabaseController.SERVER_URL+"/rak",
                new Response.Listener<String>()
                {
                    /**
                     * Called when a response is received.
                     *
                     * @param response
                     */
                    @Override
                    public void onResponse(String response)
                    {
                        if (response.equals("200")) {
                            rak.setSinkronisasi(DatabaseController.SYNC_STATUS_OK);

                            DatabaseController db = DatabaseController.getInstance(activity);
                            db.updateRak(rak);
                            db.close();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    /**
                     * Callback method that an error has been occurred with the provided error code and optional user-readable message.
                     *
                     * @param error
                     */
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Toast.makeText(activity, error.toString(), Toast.LENGTH_LONG);
                        error.printStackTrace();
                    }
                }
        )
        {
            /**
             * Gets the content type of body.
             *
             * @return Returns the body content type.
             */
            @Override
            public String getBodyContentType()
            {
                return "application/json; charset=utf-8";
            }

            /**
             * Gets the raw body.
             *
             * @return Returns the raw POST or PUT body to be sent.
             * @throws AuthFailureError
             */
            @Override
            public byte[] getBody() throws AuthFailureError
            {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                }
                catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }

            /**
             * Subclasses must implement this to parse the raw network response and return an appropriate response type.
             *
             * @param response Response from the network.
             * @return Returns the parsed response, or null in the case of an error.
             */
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response)
            {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                }
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            /**
             * Gets a list of extra HTTP headers to go along with this request. Can throw AuthFailureError
             * as authentication may be required to provide these values.
             *
             * @return Returns a list of extra HTTP headers to go along with this request.
             * @throws AuthFailureError
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };

        MySingleton.getInstance(activity).addToRequestQue(stringRequest);
    }

    /**
     * Method is called when deleting rak data in server.
     */
    private void deleteRakInServer()
    {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("kode_rak", rak.getKode());
            jsonObject.put("kode_gudang", rak.getKodeGudang());
            jsonObject.put("tipe_rak", rak.getTipe());
            jsonObject.put("status_penuh", rak.getStatusPenuh());
        }
        catch (JSONException e) {
            e.printStackTrace();
        }

        final String requestBody = jsonObject.toString();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                DatabaseController.SERVER_URL+"/rakDelete",
                new Response.Listener<String>()
                {
                    /**
                     * Called when a response is received.
                     *
                     * @param response
                     */
                    @Override
                    public void onResponse(String response)
                    {
                        if (response.equals("200")) {
                            rak.setSinkronisasi(DatabaseController.SYNC_STATUS_OK);

                            DatabaseController db = DatabaseController.getInstance(activity);
                            db.updateRak(rak);
                            db.close();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    /**
                     * Callback method that an error has been occurred with the provided error code and optional user-readable message.
                     *
                     * @param error
                     */
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Toast.makeText(activity, error.toString(), Toast.LENGTH_LONG);
                        error.printStackTrace();
                    }
                }
        )
        {
            /**
             * Gets the content type of body.
             *
             * @return Returns the body content type.
             */
            @Override
            public String getBodyContentType()
            {
                return "application/json; charset=utf-8";
            }

            /**
             * Gets the raw body.
             *
             * @return Returns the raw POST or PUT body to be sent.
             * @throws AuthFailureError
             */
            @Override
            public byte[] getBody() throws AuthFailureError
            {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                }
                catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }

            /**
             * Subclasses must implement this to parse the raw network response and return an appropriate response type.
             *
             * @param response Response from the network.
             * @return Returns the parsed response, or null in the case of an error.
             */
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response)
            {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                }
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            /**
             * Gets a list of extra HTTP headers to go along with this request. Can throw AuthFailureError
             * as authentication may be required to provide these values.
             *
             * @return Returns a list of extra HTTP headers to go along with this request.
             * @throws AuthFailureError
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };

        MySingleton.getInstance(activity).addToRequestQue(stringRequest);
    }
}
