package com.goodangcode.goodang;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.goodangcode.goodang.databasehelper.DatabaseController;
import com.goodangcode.goodang.databasemodel.Pemesanan;
import com.goodangcode.goodang.databasemodel.Rak;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author GoodangCode
 */
public class NetworkMonitor extends BroadcastReceiver
{
    @Override
    public void onReceive(final Context context, Intent intent)
    {
        if(checkNetworkConnection(context) && SessionManager.getInstance(context).isLoggedIn()) {
            List< Pemesanan> listPemesanan = DatabaseController.getInstance(context).getAllPemesanan();
            List<Rak> listRak = DatabaseController.getInstance(context).getAllRak();
            int countRak = listRak.size();
            int countBarang = DatabaseController.getInstance(context).countBarang() ;
            int countGudang = DatabaseController.getInstance(context).countGudang();
            int countPetugas = DatabaseController.getInstance(context).countPetugas();

            Toast.makeText(context, "[On process Sync]\n" +
                    "Total pemesanan: " + listPemesanan.size() + "\n" +
                    "Total rak: " + countRak + "\n" +
                    "(barang: " + countBarang + ", " +
                    "gudang: " + countGudang + ", " +
                    "petugas: " + countPetugas + ")", Toast.LENGTH_LONG).show();
            for(final Pemesanan pemesanan: listPemesanan) {
                int sync_status = pemesanan.getSinkronisasi();
                if(sync_status == DatabaseController.SYNC_STATUS_FAILED){
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("kode_pemesanan", pemesanan.getKode());
                        jsonObject.put("kode_petugas", pemesanan.getKodePetugas());
                        jsonObject.put("kode_gudang", pemesanan.getKodeGudang());
                        jsonObject.put("kode_barang", pemesanan.getKodeBarang());
                        jsonObject.put("kode_rak", pemesanan.getKodeRak());
                        jsonObject.put("penerima", pemesanan.getPenerima());
                        jsonObject.put("alamat", pemesanan.getAlamat());
                        jsonObject.put("kecamatan", pemesanan.getKecamatan());
                        jsonObject.put("tanggal_masuk", pemesanan.getTanggalMasuk());
                        jsonObject.put("tanggal_keluar", JSONObject.NULL);
                        jsonObject.put("terakhir_diubah", pemesanan.getTerakhirDiubah());
                        Toast.makeText(context, "Masukin ke server pemesanan: "+pemesanan.getKode(), Toast.LENGTH_LONG).show();
                    }
                    catch (JSONException e) {
                        Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
                    }

                    final String requestBody = jsonObject.toString();

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, DatabaseController.SERVER_URL+"/pemesanan",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response)
                                {
                                    if(response.equals("200")) {
                                        pemesanan.setSinkronisasi(DatabaseController.SYNC_STATUS_OK);
                                        DatabaseController.getInstance(context).updatePemesanan(pemesanan);
                                    }
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error)
                        {
                            Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
                            error.printStackTrace();
                        }
                    }
                    ) {
                        @Override
                        public String getBodyContentType()
                        {
                            return "application/json; charset=utf-8";
                        }

                        @Override
                        public byte[] getBody() throws AuthFailureError
                        {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }

                        @Override
                        protected Response<String> parseNetworkResponse(NetworkResponse response)
                        {
                            String responseString = "";
                            if (response != null) {
                                responseString = String.valueOf(response.statusCode);
                            }
                            return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                        }

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError
                        {
                            Map<String,String> params = new HashMap<String, String>();
                            params.put("Content-Type","application/x-www-form-urlencoded");
                            return params;
                        }
                    };

                    MySingleton.getInstance(context).addToRequestQue(stringRequest);
                }
            }

            for(final Rak rak: listRak) {
                int sync_status = rak.getSinkronisasi();
                if(sync_status == DatabaseController.SYNC_STATUS_FAILED){
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("kode_rak", rak.getKode());
                        jsonObject.put("kode_gudang", rak.getKodeGudang());
                        jsonObject.put("tipe_rak", rak.getTipe());
                        jsonObject.put("status_penuh", rak.getStatusPenuh());
                        Toast.makeText(context, "Masukin ke server rak: " + rak.getKode(), Toast.LENGTH_LONG).show();
                    }
                    catch (JSONException e) {
                        Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
                    }

                    final String requestBody = jsonObject.toString();

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, DatabaseController.SERVER_URL+"/rak",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response)
                                {
                                    if(response.equals("200")) {
                                        rak.setSinkronisasi(DatabaseController.SYNC_STATUS_OK);
                                        DatabaseController.getInstance(context).updateRak(rak);
                                    }
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error)
                        {
                            Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
                            error.printStackTrace();
                        }
                    }
                    ) {
                        @Override
                        public String getBodyContentType()
                        {
                            return "application/json; charset=utf-8";
                        }

                        @Override
                        public byte[] getBody() throws AuthFailureError
                        {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }

                        @Override
                        protected Response<String> parseNetworkResponse(NetworkResponse response)
                        {
                            String responseString = "";
                            if (response != null) {
                                responseString = String.valueOf(response.statusCode);
                            }
                            return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                        }

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError
                        {
                            Map<String,String> params = new HashMap<String, String>();
                            params.put("Content-Type","application/x-www-form-urlencoded");
                            return params;
                        }
                    };

                    MySingleton.getInstance(context).addToRequestQue(stringRequest);
                }
            }
        }
        else if(!checkNetworkConnection(context)){
            Toast.makeText(context,"Internet mati",Toast.LENGTH_LONG).show();
        }

        DatabaseController.getInstance(context).close();
    }

    public String getDateTime(Date date)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        return dateFormat.format(date);
    }

    public static boolean checkNetworkConnection(Context context)
    {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager == null ? null : connectivityManager.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }
}
