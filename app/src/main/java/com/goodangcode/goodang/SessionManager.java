package com.goodangcode.goodang;

import java.util.HashMap;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.goodangcode.goodang.login.LoginActivity;

/**
 * Created by GoodangCode on 17/05/2017.
 */

public class SessionManager {
    // Shared Preferences
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private static SessionManager mSess;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "LoginPref";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";

    // User name (make variable public to access from outside)
    public static final String KEY_USERNAME = "username";

    // name (make variable public to access from outside)
    public static final String KEY_NAME = "name";

    // code serial (make variable public to access from outside)
    public static final String KEY_SERIAL_CODE = "kode";

    // is admin (make variable public to access from outside)
    public static final String KEY_IS_ADMIN = "isadmin";

    // kode gudang (make variable public to access from outside)
    public static final String KEY_KODE_GUDANG = "kodeGudang";


    /**
     * Constructor of SessionManager
     * @param context context of application
     */
    public SessionManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Get instance of SessionManager
     * @param context context of application
     * @return mSess SessionManager
     */
    public static SessionManager getInstance(Context context) {
        if (mSess == null) {
            mSess = new SessionManager(context.getApplicationContext());
        }
        return mSess;
    }

    /**
     * Create login session
     *
     * @param username username of user
     * @param name name of user
     * @param serialCode serial code of user
     * @param isadmin flag if user is admin
     * @param kodeGudang code of Gudang where user's work
     */
    public void createLoginSession(String username, String name, String serialCode, String isadmin, String kodeGudang ){
        editor.putBoolean(IS_LOGIN, true);

        editor.putString(KEY_USERNAME, username);
        editor.putString(KEY_NAME, name);
        editor.putString(KEY_KODE_GUDANG, kodeGudang);
        editor.putString(KEY_IS_ADMIN, isadmin);
        editor.putString(KEY_SERIAL_CODE, serialCode);
        editor.commit();
    }

    /**
     * Get the user's data whose login
     *
     * @return value of user's login data
     */
    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        // user name
        user.put(KEY_NAME, pref.getString(KEY_NAME, null));
        user.put(KEY_USERNAME, pref.getString(KEY_USERNAME, null));
        user.put(KEY_IS_ADMIN, pref.getString(KEY_IS_ADMIN, null));
        user.put(KEY_KODE_GUDANG, pref.getString(KEY_KODE_GUDANG, null));
        user.put(KEY_SERIAL_CODE, pref.getString(KEY_SERIAL_CODE, null));
        // return user
        return user;
    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     */
    public void checkLogin(){
        // Check login status
        if(!this.isLoggedIn()){
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, LoginActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);
        }
    }

    /**
     * Clear session details
     */
    public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();
    }

    /**
     * Quick check for login
     *
     */
    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }

    public String isadmin(){
        return pref.getString(KEY_IS_ADMIN, null);
    }

    public String getNamaPengguna(){
        return pref.getString(KEY_NAME, null);
    }

    public String getGudangCode(){
        return pref.getString(KEY_KODE_GUDANG, null);
    }
}
