package com.goodangcode.goodang.navdrawer.alokasirak;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.goodangcode.goodang.R;
import com.goodangcode.goodang.Syncronization;
import com.goodangcode.goodang.databasehelper.DatabaseController;
import com.goodangcode.goodang.databasemodel.Rak;


/**
 * Alokasi Rak Edit - the things about editing alokasi rak information.
 *
 * @author GoodangCode
 */
public class AlokasiRakEditActivity extends AppCompatActivity
{
    private Toolbar toolbar;

    private Rak rak;

    private EditText editTextTipeRak;
    private Spinner spinnerStatus;

    private Button buttonEditSimpan;

    private String kodeRak;
    private String tipeRakUpdated;
    private int statusRakUpdated;

    /**
     * Method is called when the activity is first created - normal static set up.
     *
     * @param savedInstanceState Initialization of the activity.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alokasi_rak_edit);

        rak = getIntent().getExtras().getParcelable("rak");

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        kodeRak = rak.getKode();
        ((TextView) findViewById(R.id.kode_rak)).setText(kodeRak);

        String title = "Ubah Rak " + kodeRak;
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        editTextTipeRak = ((EditText) findViewById(R.id.tipe_rak));
        spinnerStatus = (Spinner) findViewById(R.id.spinner_status);

        editTextTipeRak.setText(rak.getTipe());
        spinnerStatus.setSelection(rak.getStatusPenuh());

        buttonEditSimpan = (Button) findViewById(R.id.button_edit_simpan);

        String count = getIntent().getStringExtra("count");
        if(!count.equals("0")) {
            editTextTipeRak.setEnabled(false);
            Toast.makeText(AlokasiRakEditActivity.this, R.string.info_rak_edit,
                    Toast.LENGTH_LONG).show();
        }

        buttonEditSimpan.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(!checkChanges()) {
                    Toast.makeText(AlokasiRakEditActivity.this, R.string.info_rak_edit_cancel,
                            Toast.LENGTH_SHORT).show();
                    finish();
                }
                else {
                    addListenerOnButton("Save");
                }
            }
        });
    }

    public int addListenerOnButton(String type)
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        if(type.equals("Cancel")) {
            alert.setTitle(R.string.dialog_title_cancel);
            alert.setMessage(R.string.dialog_message_rak_edit_batal);

            alert.setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton)
                {
                    editTextTipeRak.setText(rak.getTipe());
                    spinnerStatus.setSelection(rak.getStatusPenuh());
                    showToast(R.string.dialog_next_rak_edit_batal_ya, Toast.LENGTH_SHORT);
                    finish();
                }
            });

            alert.setNegativeButton(R.string.button_no, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton)
                {
                    setRakValueOnDisplay();
                    showToast(R.string.dialog_next_rak_edit_tidak, Toast.LENGTH_SHORT);
                }
            });

            alert.setCancelable(false);
            alert.show();
        }
        else if(type.equals("Save")) {
            alert.setTitle(R.string.dialog_title_save);
            alert.setMessage(R.string.dialog_message_rak_edit_simpan);

            alert.setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton)
                {
                    setRakValueOnDisplay();

                    if(tipeRakUpdated.equals("")) {
                        showToast(R.string.error_RAK10, Toast.LENGTH_LONG);
                    }
                    else {
                        updateRak();
                        showToast(R.string.dialog_next_rak_edit_simpan_ya, Toast.LENGTH_SHORT);

                        Intent intent = new Intent();
                        Bundle bundle = new Bundle();
                        bundle.putParcelable("rak", rak);
                        intent.putExtras(bundle);
                        setResult(RESULT_OK, intent);

                        finish();
                    }
                }
            });

            alert.setNegativeButton(R.string.button_no, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton)
                {
                    showToast(R.string.dialog_next_rak_edit_tidak, Toast.LENGTH_LONG);
                    setRakValueOnDisplay();
                }
            });

            alert.setCancelable(false);
            alert.show();
        }
        else {
            return -1;
        }

        return 1;
    }

    public void showToast(int note, int duration)
    {
        Toast.makeText(AlokasiRakEditActivity.this, note, duration).show();
    }

    private void setRakValueOnDisplay()
    {
        tipeRakUpdated = editTextTipeRak.getText().toString();
        statusRakUpdated = spinnerStatus.getSelectedItemPosition();
    }

    private boolean checkChanges()
    {
        setRakValueOnDisplay();

        if(!rak.getTipe().equals(tipeRakUpdated) || rak.getStatusPenuh() != statusRakUpdated)  {
            return true;
        }

        return false;
    }

    /**
     * Method is called whenever an item in your options menu is selected.
     *
     * @param item The menu item that was selected.
     * @return Return false to allow normal menu processing to proceed, true to consume it here.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home) {
            doCancellation();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        doCancellation();
    }

    private void doCancellation()
    {
        if(!checkChanges()) {
            Toast.makeText(AlokasiRakEditActivity.this, R.string.info_rak_edit_cancel,
                    Toast.LENGTH_SHORT).show();
            finish();
        }
        else {
            addListenerOnButton("Cancel");
        }
    }

    /**
     * Method is called when updating rak.
     */
    public void updateRak()
    {
        rak.setTipe(tipeRakUpdated);
        rak.setStatusPenuh(statusRakUpdated);

        DatabaseController db = DatabaseController.getInstance(AlokasiRakEditActivity.this);
        db.updateRak(rak);
        db.close();

        new Syncronization(AlokasiRakEditActivity.this, rak, "Update");
    }
}
