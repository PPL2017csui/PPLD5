package com.goodangcode.goodang.navdrawer.pindai;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;


/**
 * Class represents all things about QR scanner.
 *
 * @author GoodangCode
 */
public class QRScannerFragment extends Fragment implements ZXingScannerView.ResultHandler
{
    private ZXingScannerView mScannerView;

    /**
     * There is a form of the constructor that are called when the view of barang keluar is created.
     */
    public QRScannerFragment()
    {

    }

    /**
     * Method is called to have the fragment instantiate its user interface view.
     *
     * @param inflater The LayoutInflater object that can be used to inflate any views in the fragment.
     * @param container If non-null, this is the parent view that the fragment's UI should be attached to.
     *                  The fragment should not add the view itself, but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed from a previous saved state as given here.
     * @return Return the view for the fragment's UI, or null.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        mScannerView = new ZXingScannerView(getActivity());

        return mScannerView;
    }

    /**
     * Method is called when the fragment is visible to the user and actively running.
     */
    @Override
    public void onResume()
    {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    /**
     * Method is called when handling the result of scanning.
     *
     * @param rawResult The result of scanning.
     */
    @Override
    public void handleResult(Result rawResult)
    {
        Intent intent = getActivity().getIntent();
        Intent intent2 = null;

        String status = "";
        if(intent != null) {
            status = intent.getStringExtra("status");
        }

        switch(status) {
            case "masuk":
                intent2 = new Intent(getActivity(), KonfirmasiBarangMasukActivity.class);
                intent2.putExtra("result", rawResult.getText());
                intent2.putExtra("format", rawResult.getBarcodeFormat().toString());
                intent2.putExtra("source", "Pindai");
                break;
            case "keluar":
                intent2 = new Intent(getActivity(), KonfirmasiBarangKeluarActivity.class);
                intent2.putExtra("result", rawResult.getText());
                intent2.putExtra("format", rawResult.getBarcodeFormat().toString());
                intent2.putExtra("source", "Pindai");
                break;
        }

        Handler handler = new Handler();
        handler.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                mScannerView.resumeCameraPreview(QRScannerFragment.this);
            }
        }, 2000);

        startActivity(intent2);
    }

    /**
     * Method is called when the fragment is no longer resumed.
     */
    @Override
    public void onPause()
    {
        super.onPause();
        mScannerView.stopCamera();
    }
}
