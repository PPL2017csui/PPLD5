package com.goodangcode.goodang.navdrawer;


import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.goodangcode.goodang.MainActivity;
import com.goodangcode.goodang.R;
import com.goodangcode.goodang.RowItem;
import com.goodangcode.goodang.RowItemAdapter;
import com.goodangcode.goodang.RowItemEmptyAdapter;
import com.goodangcode.goodang.databasehelper.DatabaseController;
import com.goodangcode.goodang.databasemodel.Rak;
import com.goodangcode.goodang.navdrawer.alokasirak.AlokasiRakAddActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


/**
 * Alokasi Rak - the things about rak alocating.
 *
 * @author GoodangCode
 */
public class AlokasiRak extends Fragment
{
    private ArrayList<RowItem> itemList;
    private List<Rak> listRak;

    private RecyclerView recyclerView;
    private RowItemAdapter rowItemAdapter;
    private RowItemEmptyAdapter emptyAdapter;

    /**
     * There is a form of the constructor that are called when the view of alokasi rak is created.
     */
    public AlokasiRak()
    {
        itemList = new ArrayList<RowItem>();
    }

    /**
     * Method is called to have the fragment instantiate its user interface view.
     *
     * @param inflater The LayoutInflater object that can be used to inflate any views in the fragment.
     * @param container If non-null, this is the parent view that the fragment's UI should be attached to.
     *                  The fragment should not add the view itself, but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed from a previous saved state as given here.
     * @return Return the view for the fragment's UI, or null.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_alokasi_rak, container, false);

        createList();

        String title = "Alokasi Rak (" + listRak.size() + ")";
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(title);

        emptyAdapter = new RowItemEmptyAdapter(getClass());
        rowItemAdapter = new RowItemAdapter(getContext(), getClass(), itemList);
        sortList(R.string.kode_asc);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        RecyclerView.ItemDecoration itemDecoration = new
                DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);

        if(itemList.isEmpty()) {
            recyclerView.setAdapter(emptyAdapter);
        }
        else {
            recyclerView.setAdapter(rowItemAdapter);
            recyclerView.addItemDecoration(itemDecoration);
        }

        FloatingActionButton buttonAddRak = (FloatingActionButton) view.findViewById(R.id.button_add_rak);

        DatabaseController db = DatabaseController.getInstance(getContext());
        int kodePetugas = db.getKodePetugas();
        int isAdmin = db.getPetugas(kodePetugas).getIsAdmin();
        if(isAdmin == 0) {
            buttonAddRak.setVisibility(View.GONE);
        }

        buttonAddRak.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(view.getContext(), AlokasiRakAddActivity.class);
                ((Activity) getContext()).startActivityForResult(intent,
                                            getResources().getInteger(R.integer.alokasi_rak_add));
            }
        });

        setHasOptionsMenu(true);

        return view;
    }

    /**
     * Method is called when the list of rak want to created.
     */
    public int createList()
    {
        DatabaseController db = DatabaseController.getInstance(getContext());

        try {
            listRak = db.getAllRak();

            int totalAllItem = listRak.size();

            for(int i = 0; i < totalAllItem; i++) {
                Rak rak = listRak.get(i);
                itemList.add(new RowItem(rak));
            }
        } catch(SQLiteException e) {
            Toast.makeText(getContext(), R.string.error_SQL03, Toast.LENGTH_LONG).show();
            return -1;
        } finally {
            db.close();
            return 1;
        }
    }

    /**
     * Method is called to inflate menu which has been created.
     *
     * @param menu Menu which being inflated.
     * @param inflater The MenuInflater object that can be used to inflate any menus.
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        inflater.inflate(R.menu.options_menu, menu);

        searchList(menu);
        MenuItem menuSort = menu.findItem(R.id.sort);
        SubMenu subMenu = menuSort.getSubMenu();
        inflater.inflate(R.menu.sorting_menu, subMenu);

        subMenu.findItem(R.id.sort1_asc).setTitle(R.string.kode_asc);
        subMenu.findItem(R.id.sort1_dsc).setTitle(R.string.kode_dsc);
        subMenu.findItem(R.id.sort2_asc).setTitle(R.string.tipe_asc);
        subMenu.findItem(R.id.sort2_dsc).setTitle(R.string.tipe_dsc);
        subMenu.findItem(R.id.sort3_asc).setTitle(R.string.status_asc);
        subMenu.findItem(R.id.sort3_dsc).setTitle(R.string.status_dsc);

        subMenu.findItem(R.id.sort1_asc).setChecked(true);

        super.onCreateOptionsMenu(menu, inflater);
    }

    /**
     * Method is called whenever an item in your options menu is selected.
     *
     * @param item The menu item that was selected.
     * @return Return false to allow normal menu processing to proceed, true to consume it here.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        item.setChecked(true);

        switch(item.getItemId()) {
            case R.id.sort:
                return true;
            case R.id.sort1_asc:
                sortList(R.string.string_kode_asc);
                return true;
            case R.id.sort1_dsc:
                sortList(R.string.string_kode_dsc);
                return true;
            case R.id.sort2_asc:
                sortList(R.string.string_tipe_asc);
                return true;
            case R.id.sort2_dsc:
                sortList(R.string.string_tipe_dsc);
                return true;
            case R.id.sort3_asc:
                sortList(R.string.string_status_asc);
                return true;
            case R.id.sort3_dsc:
                sortList(R.string.string_status_dsc);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Method is called to sort the list by given category.
     *
     * @param category Category to sort the list.
     */
    private void sortList(final int category)
    {
        Collections.sort(itemList, new Comparator<RowItem>() {
            @Override
            public int compare(RowItem item1, RowItem item2)
            {
                String kodeRak1 = item1.getRak().getKode().toLowerCase();
                String kodeRak2 = item2.getRak().getKode().toLowerCase();
                String tipeRak1 = item1.getRak().getTipe().toLowerCase();
                String tipeRak2 = item2.getRak().getTipe().toLowerCase();
                int statusPenuh1 = item1.getRak().getStatusPenuh();
                int statusPenuh2 = item2.getRak().getStatusPenuh();

                switch(category) {
                    case R.string.string_kode_asc:
                        return kodeRak1.compareTo(kodeRak2);
                    case R.string.string_kode_dsc:
                        return kodeRak2.compareTo(kodeRak1);
                    case R.string.string_tipe_asc:
                        return tipeRak1.compareTo(tipeRak2);
                    case R.string.string_tipe_dsc:
                        return tipeRak2.compareTo(tipeRak1);
                    case R.string.string_status_asc:
                        return Integer.compare(statusPenuh1, statusPenuh2);
                    case R.string.string_status_dsc:
                        return Integer.compare(statusPenuh2, statusPenuh1);
                    default:
                        return kodeRak1.compareTo(kodeRak2);
                }
            }
        });

        rowItemAdapter.replaceAll(itemList);
    }


    /**
     * Method is called when doing search in the list.
     *
     * @param menu The menu that was selected.
     */
    private void searchList(final Menu menu)
    {
        final MenuItem item = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String query)
            {
                if(!itemList.isEmpty()) {
                    ArrayList<RowItem> filteredList = filter(itemList, query);

                    if(filteredList.isEmpty()) {
                        emptyAdapter.setSearchResult(true);
                        recyclerView.setAdapter(emptyAdapter);
                    }
                    else {
                        recyclerView.setAdapter(rowItemAdapter);
                        rowItemAdapter.replaceAll(filteredList);
                    }
                }

                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query)
            {
                return false;
            }
        });

        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menu.getItem(1).setVisible(false);
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                menu.getItem(1).setVisible(true);
                return false;
            }
        });
    }

    /**
     * Method is called to filter the list with given query.
     *
     * @param items The list which being filtered.
     * @param query String to filter the list.
     * @return Return the filtered list.
     */
    private static ArrayList<RowItem> filter(ArrayList<RowItem> items, String query)
    {
        String lowerCaseQuery = query.toLowerCase();

        ArrayList<RowItem> filteredList = new ArrayList<>();
        for(RowItem item : items) {
            String kodeRak = item.getRak().getKode().toLowerCase();
            String tipeRak = item.getRak().getTipe().toLowerCase();

            int status = item.getRak().getStatusPenuh();
            String statusRak = (status == 0) ? "tersedia" : "penuh";

            if(kodeRak.contains(lowerCaseQuery) || tipeRak.contains(lowerCaseQuery) || statusRak.contains(lowerCaseQuery)) {
                filteredList.add(item);
            }
        }

        return filteredList;
    }
}
