package com.goodangcode.goodang.navdrawer.pindai;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.goodangcode.goodang.R;
import com.goodangcode.goodang.Syncronization;
import com.goodangcode.goodang.databasehelper.DatabaseController;
import com.goodangcode.goodang.databasemodel.Pemesanan;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Konfirmasi Barang Keluar - the things about confirming of barang keluar.
 *
 * @author GoodangCode
 */
public class KonfirmasiBarangKeluarActivity extends AppCompatActivity
{
    private Toolbar toolbar;
    private TextView kodePemesanan;
    private TextView tanggalKeluar;
    private Button buttonPindaiKeluarYa;
    private Button buttonPindaiKeluarTidak;

    private Pemesanan pemesanan;
    private int kodePetugas;
    private String source;
    private String fragment;

    /**
     * Method is called when the activity is first created - normal static set up.
     *
     * @param savedInstanceState Initialization of the activity.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        String contents = getIntent().getStringExtra("result");
        source = getIntent().getStringExtra("source");

        if(!processResult(contents)) {
            finish();
            return;
        }

        setContentView(R.layout.activity_konfirmasi_barang_keluar);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Barang Keluar");

        kodePemesanan = (TextView) findViewById(R.id.kode_pemesanan);
        tanggalKeluar = (TextView) findViewById(R.id.tanggal_keluar);
        buttonPindaiKeluarYa = (Button) findViewById(R.id.button_pindai_keluar_ya);
        buttonPindaiKeluarTidak = (Button) findViewById(R.id.button_pindai_keluar_tidak);

        pemesanan.setKodePetugas(kodePetugas);
        pemesanan.setTanggalKeluar(getDateTime(new Date()));

        kodePemesanan.setText(pemesanan.getKode());
        tanggalKeluar.setText(pemesanan.getTanggalKeluar());

        buttonPindaiKeluarYa.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                updatePemesanan(pemesanan);
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putParcelable("pemesanan", pemesanan);
                intent.putExtras(bundle);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        buttonPindaiKeluarTidak.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Toast.makeText(KonfirmasiBarangKeluarActivity.this, R.string.info_pindai_keluar_cancel,
                        Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    /**
     * Method is called when processing result of scanner.
     *
     * @param contents The contents result of scanner.
     * @return Return true if process result works fine, otherwise,
     */
    public boolean processResult(String contents)
    {
        DatabaseController db = DatabaseController.getInstance(this);
        try {
            JSONObject json = new JSONObject(contents);

            String kode = json.getString("kode");
            kodePetugas = db.getKodePetugas();

            pemesanan = db.getPemesanan(kode);
            db.close();

            if(pemesanan == null) {
                Toast.makeText(KonfirmasiBarangKeluarActivity.this, R.string.error_BARANG03,
                        Toast.LENGTH_LONG).show();
                finish();
                return false;
            }
            else if(pemesanan.getTanggalKeluar() != null) {
                Toast.makeText(KonfirmasiBarangKeluarActivity.this, R.string.error_BARANG02,
                        Toast.LENGTH_LONG).show();
                finish();
            }

            return true;
        }
        catch(JSONException ex) {
            String format = getIntent().getStringExtra("format");

            if(format.equals("QR_CODE")) {
                Toast.makeText(KonfirmasiBarangKeluarActivity.this, R.string.error_QRCODE02,
                        Toast.LENGTH_LONG).show();
            }
            else {
                Toast.makeText(KonfirmasiBarangKeluarActivity.this, R.string.error_QRCODE01,
                        Toast.LENGTH_LONG).show();
            }

            return false;
        }
        finally {
            db.close();
        }
    }

    /**
     * Method is called when getting the time with the format.
     *
     * @param date The current date.
     * @return Return format string of current date.
     */
    public String getDateTime(Date date)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        return dateFormat.format(date);
    }

    /**
     * Method is called when updating pemesanan.
     *
     * @param pemesanan Pemesanan that want to update.
     */
    public void updatePemesanan(Pemesanan pemesanan)
    {
        pemesanan.setTerakhirDiubah(getDateTime(new Date()));

        DatabaseController db = DatabaseController.getInstance(this);
        long res = db.updatePemesanan(pemesanan);

        if(res == 1) {
            Toast.makeText(KonfirmasiBarangKeluarActivity.this, R.string.info_pindai_keluar_success,
                    Toast.LENGTH_SHORT).show();
        }
        else if(res == -1) {
            Toast.makeText(KonfirmasiBarangKeluarActivity.this, R.string.error_SCAN01,
                    Toast.LENGTH_LONG).show();
        }

        db.close();

        new Syncronization(KonfirmasiBarangKeluarActivity.this, pemesanan, "Out");
    }

    @Override
    public void onBackPressed()
    {
        finish();
    }
}
