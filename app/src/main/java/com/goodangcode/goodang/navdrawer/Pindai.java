package com.goodangcode.goodang.navdrawer;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.goodangcode.goodang.MainActivity;
import com.goodangcode.goodang.R;
import com.goodangcode.goodang.navdrawer.pindai.PindaiAdapter;
import com.goodangcode.goodang.navdrawer.pindai.QRScannerFragment;


/**
 * Pindai - the things about scanning barang.
 *
 * @author GoodangCode
 */
public class Pindai extends Fragment
{
    private ViewPager pager;
    private TabLayout tabs;

    /**
     * There is a form of the constructor that are called when the view of daftar barang is created.
     */
    public Pindai()
    {

    }

    /**
     * Method is called to have the fragment instantiate its user interface view.
     *
     * @param inflater The LayoutInflater object that can be used to inflate any views in the fragment.
     * @param container If non-null, this is the parent view that the fragment's UI should be attached to.
     *                  The fragment should not add the view itself, but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed from a previous saved state as given here.
     * @return Return the view for the fragment's UI, or null.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_pindai, container, false);

        ((MainActivity) getActivity()).getSupportActionBar().setTitle("Pindai");

        pager = (ViewPager) view.findViewById(R.id.pager);
        tabs = (TabLayout) view.findViewById(R.id.tabs);

        setPagerToTab(pager, tabs);

        getActivity().getIntent().putExtra("status", "masuk");

        tabs.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(pager)
        {
            @Override
            public void onTabSelected(TabLayout.Tab tab)
            {
                Intent intent = getActivity().getIntent();
                switch(tab.getPosition()) {
                    case 0:
                        intent.putExtra("status", "masuk");
                        break;
                    case 1:
                        intent.putExtra("status", "keluar");
                        break;
                }
            }
        });

        pindaiBarang();

        setHasOptionsMenu(true);

        return view;
    }

    /**
     * Method is called when setting pager to tab.
     *
     * @param pager The pager that set.
     * @param tabs The tab that was set.
     */
    public void setPagerToTab(ViewPager pager, TabLayout tabs)
    {
        pager.setAdapter(new PindaiAdapter(getChildFragmentManager()));

        tabs.setTabTextColors(getResources().getColor(R.color.colorPrimaryDark),
                getResources().getColor(android.R.color.white));

        tabs.setupWithViewPager(pager);

        tabs.setTabGravity(TabLayout.GRAVITY_FILL);
    }

    /**
     * Method is called to inflate menu which has been created.
     *
     * @param menu Menu which being inflated.
     * @param inflater The MenuInflater object that can be used to inflate any menus.
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.options_menu, menu);

        menu.getItem(1).setVisible(false);
        menu.getItem(0).setVisible(false);

        super.onCreateOptionsMenu(menu, inflater);
    }

    /**
     * Method is called when user want to do pindai barang.
     */
    public void pindaiBarang()
    {
        Fragment fragment = null;

        try {
            fragment = new QRScannerFragment();
        } catch (Exception e) {
            e.printStackTrace();
        }

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.qr_code_container, fragment).commit();
    }
}
