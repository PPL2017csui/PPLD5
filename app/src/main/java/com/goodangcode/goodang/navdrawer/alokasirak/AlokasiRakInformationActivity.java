package com.goodangcode.goodang.navdrawer.alokasirak;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.goodangcode.goodang.R;
import com.goodangcode.goodang.Syncronization;
import com.goodangcode.goodang.databasehelper.DatabaseController;
import com.goodangcode.goodang.databasemodel.Barang;
import com.goodangcode.goodang.databasemodel.Rak;

import java.util.List;


/**
 * Alokasi Rak Information - the things about getting detail list of alokasi rak.
 *
 * @author GoodangCode
 */
public class AlokasiRakInformationActivity extends AppCompatActivity
{
    private Context context;

    private Toolbar toolbar;
    private TableLayout tableLayout;

    private Rak rak;
    private List<Barang> listBarang;
    private int count;

    private Button buttonObserveRak;
    private Button buttonEditRak;
    private Button buttonDeleteRak;

    private TextView statusEmpty;
    private TextView statusExist;

    private boolean thingsChanged;

    /**
     * Method is called when the activity is first created - normal static set up.
     *
     * @param savedInstanceState Initialization of the activity.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alokasi_rak_information);
        context = this;

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Informasi Rak");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tableLayout = (TableLayout) findViewById(R.id.table_layout);

        rak = getIntent().getExtras().getParcelable("rak");

        ((TextView) findViewById(R.id.kode_rak)).setText(rak.getKode());

        showRakInformation(rak);

        buttonObserveRak = (Button) findViewById(R.id.button_observe_rak);
        buttonEditRak = (Button) findViewById(R.id.button_edit_rak);
        buttonDeleteRak = (Button) findViewById(R.id.button_delete_rak);

        statusEmpty = (TextView) findViewById(R.id.status_empty);
        statusExist = (TextView) findViewById(R.id.status_exist);

        showRakSnippet();

        DatabaseController db = DatabaseController.getInstance(this);
        int kodePetugas = db.getKodePetugas();
        int isAdmin = db.getPetugas(kodePetugas).getIsAdmin();
        if(isAdmin == 0) {
            buttonEditRak.setVisibility(View.GONE);
            buttonDeleteRak.setVisibility(View.GONE);
        }

        buttonObserveRak.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                addListenerOnButton("Observe", view);
            }
        });

        buttonEditRak.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                addListenerOnButton("Edit", view);
            }
        });

        buttonDeleteRak.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                addListenerOnButton("Delete", view);
            }
        });
    }

    public void showRakSnippet()
    {
        if(count > 0) {
            TableLayout snippet = (TableLayout) findViewById(R.id.snippet);

            int countSnippet = 3;
            countSnippet = (count > 3) ? countSnippet : count;

            String namaBarang;
            for(int i = 0; i < countSnippet; i++) {
                namaBarang = "> " + listBarang.get(i).getNama();

                View rowView = getLayoutInflater().inflate(R.layout.snippet, null);
                ((TextView) rowView.findViewById(R.id.nama_barang)).setText(namaBarang);

                snippet.addView(rowView);
            }

            // Add more sign
            if(count > 3) {
                View rowView = getLayoutInflater().inflate(R.layout.snippet, null);
                ((TextView) rowView.findViewById(R.id.nama_barang)).setText("...");

                snippet.addView(rowView);
            }

            LinearLayout status = (LinearLayout) findViewById(R.id.status);
            status.setVisibility(View.GONE);

            statusEmpty.setVisibility(View.GONE);
            statusExist.setVisibility(View.VISIBLE);
            buttonObserveRak.setVisibility(View.VISIBLE);
        }
        else {
            buttonObserveRak.setVisibility(View.GONE);
        }
    }

    public void addListenerOnButton(String type, View view)
    {
        Intent intent = new Intent(view.getContext(), AlokasiRakEditActivity.class);
        Bundle bundle = new Bundle();

        switch(type) {
            case "Delete":
                if(count > 0) {
                    Toast.makeText(AlokasiRakInformationActivity.this, R.string.error_RAK07,
                            Toast.LENGTH_SHORT).show();
                }
                else {
                    showAlertDialogDelete();
                }
                break;
            case "Edit":
                bundle.putParcelable("rak", rak);
                intent.putExtra("count", String.valueOf(count));
                intent.putExtras(bundle);
                startActivityForResult(intent, getResources().getInteger(R.integer.alokasi_rak_edit));
                break;
            case "Observe":
                if(count > 0) {
                    intent = new Intent(view.getContext(), AlokasiRakDetailActivity.class);
                    bundle.putParcelable("rak", rak);
                    intent.putExtras(bundle);
                    startActivityForResult(intent, getResources().getInteger(R.integer.alokasi_rak_detail));
                }
                break;
        }
    }

    /**
     * Method that prompts the user to make a decision to input barang into that rak or not.
     */
    public void showAlertDialogDelete()
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(R.string.dialog_title_delete);
        alert.setMessage(R.string.dialog_message_rak_delete);

        alert.setNegativeButton(R.string.button_no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton)
            {

            }
        });

        alert.setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton)
            {
                DatabaseController db = DatabaseController.getInstance(context);
                if(db.deleteRak(rak) == 1) {
                    db.close();
                    Toast.makeText(AlokasiRakInformationActivity.this, R.string.dialog_next_rak_delete_ya,
                            Toast.LENGTH_LONG).show();

                    new Syncronization(AlokasiRakInformationActivity.this, rak, "Delete");

                    Intent intent = new Intent();
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("rak", rak);
                    intent.putExtras(bundle);
                    setResult(RESULT_OK, intent);

                    finish();
                }
                else {
                    Toast.makeText(AlokasiRakInformationActivity.this, R.string.error_RAK12,
                            Toast.LENGTH_LONG).show();
                }
            }
        });

        alert.setCancelable(false);
        alert.show();
    }

    /**
     * Method is called whenever an item in your options menu is selected.
     *
     * @param item The menu item that was selected.
     * @return Return false to allow normal menu processing to proceed, true to consume it here.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home) {
            moveActivity();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        moveActivity();
    }

    private void moveActivity()
    {
        if(thingsChanged) {
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
        }

        finish();
    }

    /**
     * Method is called when showing rak data.
     *
     * @param rak
     */
    public void showRakInformation(Rak rak)
    {
        String kodeRak = rak.getKode();
        String tipeRak = rak.getTipe();
        String statusRak = (rak.getStatusPenuh() == 1) ? "Penuh" : "Tersedia";

        DatabaseController db = DatabaseController.getInstance(this);
        listBarang = db.getAllBarangRak(kodeRak);
        db.close();

        count = listBarang.size();

        String[][] attributes = { {"Tipe Rak", tipeRak}, {"Status Rak", statusRak}};

        for(int i = 0; i < attributes.length; i++) {
            String attributeNames = attributes[i][0];
            String attributeValues = attributes[i][1];

            View rowView = getLayoutInflater().inflate(R.layout.table_item, null);
            ((TextView) rowView.findViewById(R.id.column_1)).setText(attributeNames);
            ((TextView) rowView.findViewById(R.id.column_2)).setText(R.string.colon);
            ((TextView) rowView.findViewById(R.id.column_3)).setText(attributeValues);

            tableLayout.addView(rowView);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(resultCode == RESULT_OK && data != null) {
            thingsChanged = true;

            if(requestCode == getResources().getInteger(R.integer.alokasi_rak_edit)) {
                tableLayout.removeAllViews();
                rak = data.getExtras().getParcelable("rak");
                showRakInformation(rak);
            }
            else if(requestCode == getResources().getInteger(R.integer.alokasi_rak_detail)) {
                tableLayout.removeAllViews();
                showRakInformation(rak);
                showRakSnippet();
            }
        }
    }
}
