package com.goodangcode.goodang.navdrawer;

import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.goodangcode.goodang.MainActivity;
import com.goodangcode.goodang.R;
import com.goodangcode.goodang.RowItem;
import com.goodangcode.goodang.RowItemAdapter;
import com.goodangcode.goodang.RowItemEmptyAdapter;
import com.goodangcode.goodang.databasehelper.DatabaseController;
import com.goodangcode.goodang.databasemodel.Barang;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


/**
 * Daftar Barang - the things about getting list of barang in stock point.
 *
 * @author GoodangCode
 */
public class DaftarBarang extends Fragment
{
    private ArrayList<RowItem> itemList;
    private List<Barang> listBarang;

    private RecyclerView recyclerView;
    private RowItemAdapter rowItemAdapter;
    private RowItemEmptyAdapter emptyAdapter;
    /**
     * There is a form of the constructor that are called when the view of daftar barang is created.
     */
    public DaftarBarang()
    {
        itemList = new ArrayList<RowItem>();
    }

    /**
     * Method is called to have the fragment instantiate its user interface view.
     *
     * @param inflater The LayoutInflater object that can be used to inflate any views in the fragment.
     * @param container If non-null, this is the parent view that the fragment's UI should be attached to.
     *                  The fragment should not add the view itself, but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed from a previous saved state as given here.
     * @return Return the view for the fragment's UI, or null.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_daftar_barang, container, false);

        createList();

        String title = "Daftar Barang (" + listBarang.size() + ")";
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(title);

        emptyAdapter = new RowItemEmptyAdapter(getClass());
        rowItemAdapter = new RowItemAdapter(getContext(), getClass(), itemList);
        sortList(R.string.string_nama_asc);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        RecyclerView.ItemDecoration itemDecoration = new
                DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);

        if(itemList.isEmpty()) {
            recyclerView.setAdapter(emptyAdapter);
        }
        else {
            recyclerView.setAdapter(rowItemAdapter);
            recyclerView.addItemDecoration(itemDecoration);
        }

        setHasOptionsMenu(true);

        return view;
    }

    /**
     * Method is called to inflate menu which has been created.
     *
     * @param menu Menu which being inflated.
     * @param inflater The MenuInflater object that can be used to inflate any menus.
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        inflater.inflate(R.menu.options_menu, menu);

        searchList(menu);
        MenuItem menuSort = menu.findItem(R.id.sort);
        SubMenu subMenu = menuSort.getSubMenu();
        inflater.inflate(R.menu.sorting_menu, subMenu);

        subMenu.findItem(R.id.sort1_asc).setTitle(R.string.nama_asc);
        subMenu.findItem(R.id.sort1_dsc).setTitle(R.string.nama_dsc);
        subMenu.findItem(R.id.sort2_asc).setTitle(R.string.kode_asc);
        subMenu.findItem(R.id.sort2_dsc).setTitle(R.string.kode_dsc);
        subMenu.findItem(R.id.sort3_asc).setTitle(R.string.stok_asc);
        subMenu.findItem(R.id.sort3_dsc).setTitle(R.string.stok_dsc);

        subMenu.findItem(R.id.sort1_asc).setChecked(true);

        super.onCreateOptionsMenu(menu, inflater);
    }

    /**
     * Method is called whenever an item in your options menu is selected.
     *
     * @param item The menu item that was selected.
     * @return Return false to allow normal menu processing to proceed, true to consume it here.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        item.setChecked(true);

        switch(item.getItemId()) {
            case R.id.sort:
                return true;
            case R.id.sort1_asc:
                sortList(R.string.string_nama_asc);
                return true;
            case R.id.sort1_dsc:
                sortList(R.string.string_nama_dsc);
                return true;
            case R.id.sort2_asc:
                sortList(R.string.string_kode_asc);
                return true;
            case R.id.sort2_dsc:
                sortList(R.string.string_kode_dsc);
                return true;
            case R.id.sort3_asc:
                sortList(R.string.string_stok_asc);
                return true;
            case R.id.sort3_dsc:
                sortList(R.string.string_stok_dsc);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Method is called when doing search in the list.
     *
     * @param menu The menu that was selected.
     */
    private void searchList(final Menu menu)
    {
        final MenuItem item = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String query)
            {
                if(!itemList.isEmpty()) {
                    ArrayList<RowItem> filteredList = filter(itemList, query);

                    if(filteredList.isEmpty()) {
                        emptyAdapter.setSearchResult(true);
                        recyclerView.setAdapter(emptyAdapter);
                    }
                    else {
                        recyclerView.setAdapter(rowItemAdapter);
                        rowItemAdapter.replaceAll(filteredList);
                    }
                }

                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query)
            {
                return false;

            }
        });

        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menu.getItem(1).setVisible(false);
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                menu.getItem(1).setVisible(true);
                return false;
            }
        });
    }

    /**
     * Method is called to filter the list with given query.
     *
     * @param items The list which being filtered.
     * @param query String to filter the list.
     * @return Return the filtered list.
     */
    private static ArrayList<RowItem> filter(ArrayList<RowItem> items, String query)
    {
        String lowerCaseQuery = query.toLowerCase();

        ArrayList<RowItem> filteredList = new ArrayList<>();
        for(RowItem item : items) {
            String namaBarang = item.getBarang().getNama().toLowerCase();
            String kodeBarang = item.getBarang().getKode().toLowerCase();
            if(namaBarang.contains(lowerCaseQuery) || kodeBarang.contains(lowerCaseQuery)) {
                filteredList.add(item);
            }
        }

        return filteredList;
    }


    /**
     * Method is called to sort the list by given category.
     *
     * @param category Category to sort the list.
     */
    private void sortList(final int category)
    {
        Collections.sort(itemList, new Comparator<RowItem>() {
            @Override
            public int compare(RowItem item1, RowItem item2)
            {
                String namaBarang1 = item1.getBarang().getNama().toLowerCase();
                String namaBarang2 = item2.getBarang().getNama().toLowerCase();
                String kodeBarang1 = item1.getBarang().getKode().toLowerCase();
                String kodeBarang2 = item2.getBarang().getKode().toLowerCase();
                int countBarang1 = 0;
                int countBarang2 = 0;

                if(category == R.string.string_stok_asc || category == R.string.string_stok_dsc) {
                    countBarang1 = item1.countBarang(getContext());
                    countBarang2 = item2.countBarang(getContext());
                }

                switch(category) {
                    case R.string.string_nama_asc:
                        return namaBarang1.compareTo(namaBarang2);
                    case R.string.string_nama_dsc:
                        return namaBarang2.compareTo(namaBarang1);
                    case R.string.string_kode_asc:
                        return kodeBarang1.compareTo(kodeBarang2);
                    case R.string.string_kode_dsc:
                        return kodeBarang2.compareTo(kodeBarang1);
                    case R.string.string_stok_asc:
                        return Integer.compare(countBarang2, countBarang1);
                    case R.string.string_stok_dsc:
                        return Integer.compare(countBarang1, countBarang2);
                    default:
                        return namaBarang1.compareTo(namaBarang2);
                }
            }
        });

        rowItemAdapter.replaceAll(itemList);
    }

    /**
     * Method is called when the list of barang want to created.
     */
    private void createList()
    {
        DatabaseController db = DatabaseController.getInstance(getContext());

        try {
            listBarang = db.getAllBarang();

            int totalAllItem = listBarang.size();

            for(int i = 0; i < totalAllItem; i++) {
                Barang barang = listBarang.get(i);
                itemList.add(new RowItem(barang));
            }
        } catch(SQLiteException e) {
            Toast.makeText(getContext(), R.string.error_SQL01, Toast.LENGTH_LONG).show();
        } finally {
            db.close();
        }
    }
}