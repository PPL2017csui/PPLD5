package com.goodangcode.goodang.navdrawer.pindai;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.goodangcode.goodang.R;
import com.goodangcode.goodang.Syncronization;
import com.goodangcode.goodang.databasehelper.DatabaseController;
import com.goodangcode.goodang.databasemodel.Barang;
import com.goodangcode.goodang.databasemodel.Pemesanan;
import com.goodangcode.goodang.databasemodel.Rak;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Konfirmasi Barang Masuk - the things about confirming of barang masuk.
 *
 * @author GoodangCode
 */
public class KonfirmasiBarangMasukActivity extends AppCompatActivity
{
    private Toolbar toolbar;
    private TextView kodePemesanan;
    private EditText kodeRak;
    private Button buttonPindaiMasukYa;
    private Button buttonPindaiMasukTidak;
    private TextView cekPemesanan;
    private TextView konfirmasiPemesanan;

    private boolean keluar;
    private boolean update;

    private RakRecommender rakRecommender;
    private ImageButton buttonRefreshRecommenderRak;

    private Barang barang;
    private Pemesanan pemesanan;
    private Rak rak;
    private int kodePetugas;

    /**
     * Method is called when the activity is first created - normal static set up.
     *
     * @param savedInstanceState Initialization of the activity.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        String contents = getIntent().getStringExtra("result");

        if(!processResult(contents)) {
            finish();
            return;
        }

        setContentView(R.layout.activity_konfirmasi_barang_masuk);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Barang Masuk");

        cekPemesanan = (TextView) findViewById(R.id.cek_pemesanan);
        konfirmasiPemesanan = (TextView) findViewById(R.id.konfirmasi_pemesanan);
        kodePemesanan = (TextView) findViewById(R.id.kode_pemesanan);
        kodeRak = (EditText) findViewById(R.id.kode_rak);
        buttonPindaiMasukYa = (Button) findViewById(R.id.button_pindai_masuk_ya);
        buttonPindaiMasukTidak = (Button) findViewById(R.id.button_pindai_masuk_tidak);

        buttonRefreshRecommenderRak = (ImageButton) findViewById(R.id.button_refresh_recommender_rak);

        rakRecommender = new RakRecommender(this, pemesanan);
        rak = rakRecommender.recommendRak(null, "");

        if(rak != null) {
            kodeRak.setText(rak.getKode());
            buttonRefreshRecommenderRak.setVisibility(View.VISIBLE);
        }
        else {
            if(!keluar) {
                Toast.makeText(KonfirmasiBarangMasukActivity.this, R.string.error_RAK04,
                        Toast.LENGTH_SHORT).show();
            }
        }

        if(keluar) {
            buttonRefreshRecommenderRak.setVisibility(View.INVISIBLE);
            kodeRak.setVisibility(View.INVISIBLE);
            konfirmasiPemesanan.setText(R.string.konfirmasi_barang_masuk_setelah_keluar_1);
            cekPemesanan.setText(R.string.konfirmasi_barang_masuk_setelah_keluar_2);
        }

        kodePemesanan.setText(pemesanan.getKode());

        buttonRefreshRecommenderRak.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
            refreshRecommenderRak();
            }
        });

        buttonPindaiMasukYa.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                doPindai();
            }
        });

        buttonPindaiMasukTidak.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Toast.makeText(KonfirmasiBarangMasukActivity.this, R.string.info_pindai_masuk_cancel,
                        Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    public void refreshRecommenderRak()
    {
        rak = rakRecommender.recommendRak(rak, "After");

        if(rak == null) {
            Toast.makeText(KonfirmasiBarangMasukActivity.this, R.string.info_rak_other_not_exist,
                    Toast.LENGTH_LONG).show();
            rak = rakRecommender.recommendRak(null, "");
        }

        kodeRak.setText(rak.getKode());
    }

    public void doPindai()
    {
        if(!keluar) {
            String kodeRakInput = kodeRak.getText().toString().toUpperCase();

            if(kodeRakInput.equals("")) {
                Toast.makeText(KonfirmasiBarangMasukActivity.this, R.string.error_RAK02,
                        Toast.LENGTH_LONG).show();
            }
            else {
                DatabaseController db = DatabaseController.getInstance(KonfirmasiBarangMasukActivity.this);
                Rak rakInput = db.getRak(kodeRakInput);
                db.close();

                if(rakInput == null) {
                    Toast.makeText(KonfirmasiBarangMasukActivity.this, R.string.error_RAK01,
                            Toast.LENGTH_LONG).show();
                }
                else if(rakInput.getStatusPenuh() == 1) {
                    Toast.makeText(KonfirmasiBarangMasukActivity.this, R.string.error_RAK05,
                            Toast.LENGTH_LONG).show();
                }
                else if((barang.getIsBeraturan() == 0 && !rakInput.getTipe().equals(pemesanan.getKecamatan())) ||
                        (barang.getIsBeraturan() == 1 && !rakInput.getTipe().equals(barang.getTipe()))) {
                    showAlertDialogRak();
                }
                else {
                    setRak();
                }
            }
        }
        else {
            cekPemesanan.setText(R.string.konfirmasi_barang_masuk);
            konfirmasiPemesanan.setText(R.string.konfirmasi_barang_2);
            kodeRak.setVisibility(View.VISIBLE);
            keluar = false;

            if(rak != null) {
                buttonRefreshRecommenderRak.setVisibility(View.VISIBLE);
            }
            else {
                Toast.makeText(KonfirmasiBarangMasukActivity.this, R.string.error_RAK04,
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * Method that helping to set rak.
     */
    private void setRak()
    {
        pemesanan.setKodeRak(kodeRak.getText().toString().toUpperCase());

        String currentTime = getDateTime(new Date());

        pemesanan.setTanggalMasuk(currentTime);
        pemesanan.setTerakhirDiubah(currentTime);
        pemesanan.setKodePetugas(kodePetugas);

        if (update) {
            updatePemesanan(pemesanan);
        } else {
            addPemesanan(pemesanan);
        }

        finish();
    }

    /**
     * Method that prompts the user to make a decision to input barang into that rak or not.
     */
    public void showAlertDialogRak()
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(R.string.dialog_title_rak);
        alert.setMessage(R.string.dialog_message_rak);

        alert.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton)
            {
                Toast.makeText(KonfirmasiBarangMasukActivity.this, R.string.dialog_next_rak,
                        Toast.LENGTH_LONG).show();
            }
        });

        alert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton)
            {
                setRak();
            }
        });

        alert.setCancelable(false);
        alert.show();
    }

    /**
     * Method is called whenever an item in your options menu is selected.
     *
     * @param item The menu item that was selected.
     * @return Return false to allow normal menu processing to proceed, true to consume it here.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Method is called when processing result of scanner.
     *
     * @param contents The contents result of scanner.
     * @return Return true if process result works fine, otherwise,
     */
    public boolean processResult(String contents)
    {
        DatabaseController db = DatabaseController.getInstance(this);

        try {
            JSONObject json = new JSONObject(contents);

            String kode = json.getString("kode");
            String kodeBarang = json.getString("kode_barang");
            String penerima = json.getString("penerima");
            String alamat = json.getString("alamat");
            String kecamatan = json.getString("kecamatan");

            barang = db.getBarang(kodeBarang);
            if(barang == null) {
                Toast.makeText(KonfirmasiBarangMasukActivity.this, R.string.error_BARANG04,
                        Toast.LENGTH_LONG).show();
                Toast.makeText(KonfirmasiBarangMasukActivity.this, R.string.error_ADMIN01,
                        Toast.LENGTH_LONG).show();
                db.close();
                return false;
            }

            kodePetugas = db.getKodePetugas();
            pemesanan = db.getPemesanan(kode);
            db.close();

            if(pemesanan != null) {
                if(pemesanan.getTanggalKeluar() == null) {
                    Toast.makeText(KonfirmasiBarangMasukActivity.this, R.string.error_BARANG01,
                            Toast.LENGTH_LONG).show();
                    finish();
                }
                else {
                    keluar = true;
                    update = true;
                }
            }
            else {
                keluar = false;
                update = false;
            }

            String kodeGudang = DatabaseController.getInstance(this).getKodeGudang();
            DatabaseController.getInstance(this).close();

            pemesanan = new Pemesanan(kode, kodePetugas, kodeGudang, kodeBarang, penerima, alamat, kecamatan);

            return true;
        }
        catch(JSONException ex) {
            String format = getIntent().getStringExtra("format");

            if(format.equals("QR_CODE")) {
                Toast.makeText(KonfirmasiBarangMasukActivity.this, R.string.error_QRCODE02,
                        Toast.LENGTH_LONG).show();
            }
            else {
                Toast.makeText(KonfirmasiBarangMasukActivity.this, R.string.error_QRCODE01,
                        Toast.LENGTH_LONG).show();
            }

            return false;
        }
        finally {
            db.close();
        }
    }

    /**
     * Method is called when getting the time with the format.
     *
     * @param date The current date.
     * @return Return format string of current date.
     */
    public String getDateTime(Date date)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        return dateFormat.format(date);
    }

    /**
     * Method is called when adding pemesanan.
     *
     * @param pemesanan Pemesanan that want to add.
     */
    public void addPemesanan(Pemesanan pemesanan)
    {
        DatabaseController db = DatabaseController.getInstance(this);
        long res = db.addPemesanan(pemesanan);

        if(res == 1) {
            Toast.makeText(KonfirmasiBarangMasukActivity.this, R.string.info_pindai_masuk_success,
                    Toast.LENGTH_SHORT).show();
        }
        else if(res == -1){
            Toast.makeText(KonfirmasiBarangMasukActivity.this, R.string.error_SCAN01,
                    Toast.LENGTH_LONG).show();
        }

        db.close();

        new Syncronization(KonfirmasiBarangMasukActivity.this, pemesanan, "In");
    }

    /**
     * Method is called when updating pemesanan.
     *
     * @param pemesanan Pemesanan that want to update.
     */
    public void updatePemesanan(Pemesanan pemesanan)
    {
        DatabaseController db = DatabaseController.getInstance(this);
        long res = db.updatePemesanan(pemesanan);

        if(res == 1) {
            Toast.makeText(KonfirmasiBarangMasukActivity.this, R.string.info_pindai_masuk_success,
                    Toast.LENGTH_SHORT).show();
        }
        else if(res == -1){
            Toast.makeText(KonfirmasiBarangMasukActivity.this, R.string.error_SCAN01,
                    Toast.LENGTH_LONG).show();
        }

        db.close();

        new Syncronization(KonfirmasiBarangMasukActivity.this, pemesanan, "Out");
    }
}