package com.goodangcode.goodang.navdrawer.riwayatbarang;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


/**
 * The adapter provides access to the data items.
 *
 * @author GoodangCode
 */
public class RiwayatBarangAdapter extends FragmentPagerAdapter
{
    String[] title;

    /**
     * There is a form of the constructor that are called when the view of riwayat barang adapter is created.
     *
     * @param fm Initialization of the fragment manager.
     */
    public RiwayatBarangAdapter(FragmentManager fm, FragmentActivity fragmentActivity)
    {
        super(fm);
        Intent intent = fragmentActivity.getIntent();
        Bundle bundle = intent.getExtras();
        int totalRiwayatBarangMasuk = bundle.getParcelableArrayList("listRiwayatBarangMasuk").size();
        int totalRiwayatBarangKeluar = bundle.getParcelableArrayList("listRiwayatBarangKeluar").size();
        title = new String[] {"Barang Masuk " + "(" + totalRiwayatBarangMasuk + ")", "Barang Keluar " + "(" + totalRiwayatBarangKeluar + ")"};
    }

    /**
     * Method is called when getting the data item associated with the specified position in the data set.
     *
     * @param position Position of the item whose data we want within the adapter's data set.
     * @return The fragment at the specified position.
     */
    @Override
    public Fragment getItem(int position)
    {
        Fragment fragment = null;


        switch(position){
            case 0:
                fragment = new RiwayatBarangMasukFragment();
                break;
            case 1:
                fragment = new RiwayatBarangKeluarFragment();
                break;
            default:
                fragment = null;
                break;
        }

        return fragment;
    }

    /**
     * Method is called when getting the title of data item associated with the specified position in the data set.
     *
     * @param position Position of the item whose data we want within the adapter's data set.
     * @return The title at the specified position.
     */
    @Override
    public CharSequence getPageTitle(int position)
    {
        return title[position];
    }

    /**
     * Method is called when counting the length of the represented by this Adapter.
     *
     * @return Return the length of title.
     */
    @Override
    public int getCount()
    {
        return title.length;
    }
}
