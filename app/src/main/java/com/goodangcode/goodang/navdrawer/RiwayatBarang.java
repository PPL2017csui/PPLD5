package com.goodangcode.goodang.navdrawer;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.goodangcode.goodang.MainActivity;
import com.goodangcode.goodang.R;
import com.goodangcode.goodang.databasehelper.DatabaseController;
import com.goodangcode.goodang.databasemodel.Pemesanan;
import com.goodangcode.goodang.navdrawer.riwayatbarang.RiwayatBarangAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Riwayat Barang - the things about getting list of riwayat barang in stock point.
 *
 * @author GoodangCode
 */
public class RiwayatBarang extends Fragment
{
    private ViewPager pager;
    private TabLayout tabs;
    private ArrayList<Pemesanan> listRiwayatBarangMasuk;
    private ArrayList<Pemesanan> listRiwayatBarangKeluar;

    private List<Pemesanan> pemesanan;

    /**
     * There is a form of the constructor that are called when the view of riwayat barang is created.
     */
    public RiwayatBarang()
    {

    }

    /**
     * Method is called to have the fragment instantiate its user interface view.
     *
     * @param inflater The LayoutInflater object that can be used to inflate any views in the fragment.
     * @param container If non-null, this is the parent view that the fragment's UI should be attached to.
     *                  The fragment should not add the view itself, but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed from a previous saved state as given here.
     * @return Return the view for the fragment's UI, or null.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {

        View view = inflater.inflate(R.layout.fragment_riwayat_barang, container, false);

        ((MainActivity) getActivity()).getSupportActionBar().setTitle("Riwayat Barang");

        listRiwayatBarangMasuk = new ArrayList<Pemesanan>();
        listRiwayatBarangKeluar = new ArrayList<Pemesanan>();

        pager = (ViewPager) view.findViewById(R.id.riwayat_barang_pager);
        tabs = (TabLayout) view.findViewById(R.id.riwayat_barang_tabs);

        pemesanan = DatabaseController.getInstance(getContext()).getAllPemesanan();

        getRiwayatBarang(pemesanan);

        setPagerToTab(pager, tabs);

        return view;
    }

    /**
     * Method is called when setting pager to tab.
     *
     * @param pager The pager that set.
     * @param tabs The tab that was set.
     */
    public void setPagerToTab(ViewPager pager, TabLayout tabs)
    {
        pager.setAdapter(new RiwayatBarangAdapter(getChildFragmentManager(),getActivity()));

        tabs.setTabTextColors(getResources().getColor(R.color.colorPrimaryDark),
                getResources().getColor(android.R.color.white));

        tabs.setupWithViewPager(pager);

        tabs.setTabGravity(TabLayout.GRAVITY_FILL);
    }

    /**
     * Method is called when getting riwayat barang data.
     *
     * @param pemesanan all of those pemesanan
     * @return Hashmap that contains two list such as list of riwayatBarangMasuk and list of riwayatBarangKeluar
     */
    public HashMap<String,List<Pemesanan>> getRiwayatBarang(List<Pemesanan> pemesanan)
    {
        for(int i=0; i<pemesanan.size(); i++) {
            Pemesanan currentValue = pemesanan.get(i);

            if(currentValue.getTanggalKeluar() != null) {
                listRiwayatBarangKeluar.add(currentValue);
            }

            listRiwayatBarangMasuk.add(currentValue);
        }

        Intent intent = getActivity().getIntent();

        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("listRiwayatBarangMasuk", listRiwayatBarangMasuk);
        bundle.putParcelableArrayList("listRiwayatBarangKeluar", listRiwayatBarangKeluar);
        intent.putExtras(bundle);

        HashMap<String,List<Pemesanan>> map = new HashMap<String,List<Pemesanan>>();
        map.put("listRiwayatBarangMasuk",listRiwayatBarangMasuk);
        map.put("listRiwayatBarangKeluar",listRiwayatBarangKeluar);
        return map;
    }
}
