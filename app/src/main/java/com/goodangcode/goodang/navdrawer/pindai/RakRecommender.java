package com.goodangcode.goodang.navdrawer.pindai;


import android.app.Activity;

import com.goodangcode.goodang.databasehelper.DatabaseController;
import com.goodangcode.goodang.databasemodel.Barang;
import com.goodangcode.goodang.databasemodel.Pemesanan;
import com.goodangcode.goodang.databasemodel.Rak;

import java.util.List;

/**
 * Class respresents recommending rak to the user.
 *
 * @author GoodangCode
 */
public class RakRecommender
{
    private Activity activity;

    private Barang barang;
    private Pemesanan pemesanan;

    private List<Rak> rakList;
    private Rak rak;

    public RakRecommender(Activity activity, Pemesanan pemesanan)
    {
        this.activity = activity;
        this.pemesanan = pemesanan;

        DatabaseController db = DatabaseController.getInstance(activity);

        barang = db.getBarang(pemesanan.getKodeBarang());
        rakList = db.getAllRak();

        db.close();
    }

    public Rak recommendRak(Rak rak, String note)
    {
        int beginPosition = 0;
        if(rak != null) {
            if(note.equals("Before")) {
                beginPosition = rakList.indexOf(rak) - 1;
            }
            else if(note.equals("After")) {
                beginPosition = rakList.indexOf(rak) + 1;
            }
        }

        if(beginPosition < 0) {
            beginPosition = rakList.size() - 1;
        }

        String kodeRak;
        for(int i = beginPosition; i < rakList.size(); i++) {
            kodeRak = rakList.get(i).getKode();
            if(isExistRak(kodeRak) && isFitRak()) {
                return rakList.get(i);
            }
        }

        return null;
    }

    public boolean isExistRak(String kodeRak)
    {
        DatabaseController db = DatabaseController.getInstance(activity);
        this.rak = db.getRak(kodeRak);
        db.close();

        return rak != null;
    }

    public boolean isFitRak()
    {
        if(rak != null && barang != null && rak.getStatusPenuh() == 0) {
            if(barang.getIsBeraturan() == 1) {
                if(rak.getTipe().equals(barang.getTipe())) {
                    return true;
                }
            }
            else {
                if(rak.getTipe().equals(pemesanan.getKecamatan())) {
                    return true;
                }
            }
        }

        return false;
    }
}

