package com.goodangcode.goodang.navdrawer.pindai;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.goodangcode.goodang.R;


/**
 * Barang Keluar - init fragment of barang keluar.
 *
 * @author GoodangCode
 */
public class BarangKeluarFragment extends Fragment
{
    /**
     * There is a form of the constructor that are called when the view of barang keluar is created.
     */
    public BarangKeluarFragment()
    {

    }

    /**
     * Method is called to have the fragment instantiate its user interface view.
     *
     * @param inflater The LayoutInflater object that can be used to inflate any views in the fragment.
     * @param container If non-null, this is the parent view that the fragment's UI should be attached to.
     *                  The fragment should not add the view itself, but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed from a previous saved state as given here.
     * @return Return the view for the fragment's UI, or null.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_barang_keluar, container, false);
    }
}
