package com.goodangcode.goodang.navdrawer.alokasirak;

import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.goodangcode.goodang.R;
import com.goodangcode.goodang.RowItem;
import com.goodangcode.goodang.RowItemAdapter;
import com.goodangcode.goodang.RowItemEmptyAdapter;
import com.goodangcode.goodang.databasehelper.DatabaseController;
import com.goodangcode.goodang.databasemodel.Barang;
import com.goodangcode.goodang.databasemodel.Pemesanan;
import com.goodangcode.goodang.databasemodel.Rak;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


/**
 * Detail Alokasi Rak - the things about getting list of pemesanan in rak.
 *
 * @author GoodangCode
 */
public class AlokasiRakDetailActivity extends AppCompatActivity
{
    private RecyclerView recyclerView;
    private RowItemAdapter rowItemAdapter;
    private RowItemEmptyAdapter emptyAdapter;

    private ArrayList<RowItem> itemList;
    private List<Pemesanan> listPemesanan;
    private Rak rak;

    private boolean thingsChanged;

    /**
     * Method is called when the activity is first created - normal static set up.
     *
     * @param savedInstanceState Initialization of the activity.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alokasi_rak_detail);

        rak = getIntent().getExtras().getParcelable("rak");

        itemList = new ArrayList<RowItem>();
        createList();

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        String title = "Barang di Rak " + rak.getKode() + " (" + listPemesanan.size() + ")";
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        emptyAdapter = new RowItemEmptyAdapter(getClass());
        rowItemAdapter = new RowItemAdapter(this, getClass(), itemList);
        sortList(R.string.string_nama_asc);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        RecyclerView.ItemDecoration itemDecoration = new
                DividerItemDecoration(this, DividerItemDecoration.VERTICAL);

        if(itemList.isEmpty()) {
            recyclerView.setAdapter(emptyAdapter);
        }
        else {
            recyclerView.setAdapter(rowItemAdapter);
            recyclerView.addItemDecoration(itemDecoration);
        }
    }

    /**
     * Method is called to inflate menu which has been created.
     *
     * @param menu Menu which being inflated.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.options_menu, menu);

        searchList(menu);
        MenuItem menuSort = menu.findItem(R.id.sort);
        SubMenu subMenu = menuSort.getSubMenu();
        getMenuInflater().inflate(R.menu.sorting_menu_alokasi_rak_detail, subMenu);

        subMenu.findItem(R.id.sort1_asc).setTitle(R.string.nama_asc);
        subMenu.findItem(R.id.sort1_dsc).setTitle(R.string.nama_dsc);
        subMenu.findItem(R.id.sort2_asc).setTitle(R.string.kode_asc);
        subMenu.findItem(R.id.sort2_dsc).setTitle(R.string.kode_dsc);

        subMenu.findItem(R.id.sort1_asc).setChecked(true);

        return true;
    }

    /**
     * Method is called whenever an item in your options menu is selected.
     *
     * @param item The menu item that was selected.
     * @return Return false to allow normal menu processing to proceed, true to consume it here.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        item.setChecked(true);

        switch(item.getItemId()) {
            case android.R.id.home:
                if(thingsChanged) {
                    Intent intent = new Intent();
                    setResult(RESULT_OK, intent);
                }
                finish();
                return super.onOptionsItemSelected(item);
            case R.id.sort:
                return true;
            case R.id.sort1_asc:
                sortList(R.string.string_nama_asc);
                return true;
            case R.id.sort1_dsc:
                sortList(R.string.string_nama_dsc);
                return true;
            case R.id.sort2_asc:
                sortList(R.string.string_kode_asc);
                return true;
            case R.id.sort2_dsc:
                sortList(R.string.string_kode_dsc);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Method is called when doing search in the list.
     *
     * @param menu The menu that was selected.
     */
    private void searchList(final Menu menu)
    {
        final MenuItem item = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String query)
            {
                if(!itemList.isEmpty()) {
                    ArrayList<RowItem> filteredList = filter(itemList, query);

                    if(filteredList.isEmpty()) {
                        emptyAdapter.setSearchResult(true);
                        recyclerView.setAdapter(emptyAdapter);
                    }
                    else {
                        recyclerView.setAdapter(rowItemAdapter);
                        rowItemAdapter.replaceAll(filteredList);
                    }
                }

                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query)
            {
                return false;
            }
        });

        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menu.getItem(1).setVisible(false);
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                menu.getItem(1).setVisible(true);
                return false;
            }
        });
    }

    /**
     * Method is called to filter the list with given query.
     *
     * @param items The list which being filtered.
     * @param query String to filter the list.
     * @return Return the filtered list.
     */
    private static ArrayList<RowItem> filter(ArrayList<RowItem> items, String query)
    {
        String lowerCaseQuery = query.toLowerCase();

        ArrayList<RowItem> filteredList = new ArrayList<>();
        for(RowItem item : items) {
            String namaBarang = item.getBarang().getNama().toLowerCase();
            String kodePesan = item.getPemesanan().getKode().toLowerCase();
            if(namaBarang.contains(lowerCaseQuery) || kodePesan.contains(lowerCaseQuery)) {
                filteredList.add(item);
            }
        }

        return filteredList;
    }

    /**
     * Method is called to sort the list by given category.
     *
     * @param category Category to sort the list.
     */
    private void sortList(final int category)
    {
        Collections.sort(itemList, new Comparator<RowItem>() {
            @Override
            public int compare(RowItem item1, RowItem item2)
            {
                String namaBarang1 = item1.getBarang().getNama().toLowerCase();
                String namaBarang2 = item2.getBarang().getNama().toLowerCase();
                String kodePenerima1 = item1.getPemesanan().getKode().toLowerCase();
                String kodePenerima2 = item2.getPemesanan().getKode().toLowerCase();

                switch(category) {
                    case R.string.string_nama_asc:
                        return namaBarang1.compareTo(namaBarang2);
                    case R.string.string_nama_dsc:
                        return namaBarang2.compareTo(namaBarang1);
                    case R.string.string_kode_asc:
                        return kodePenerima1.compareTo(kodePenerima2);
                    case R.string.string_kode_dsc:
                        return kodePenerima2.compareTo(kodePenerima1);
                    default:
                        return namaBarang1.compareTo(namaBarang2);
                }
            }
        });

        rowItemAdapter.replaceAll(itemList);
    }

    /**
     * Method is called when the list of barang in rak want to created.
     */
    public ArrayList<RowItem> createList()
    {
        DatabaseController db = DatabaseController.getInstance(this);

        try {
            listPemesanan = db.getAllPemesananRak(rak.getKode());

            int totalAllItem = listPemesanan.size();

            for(int i = 0; i < totalAllItem; i++) {
                Pemesanan pemesanan = listPemesanan.get(i);

                Barang barang = db.getBarang(pemesanan.getKodeBarang());

                itemList.add(new RowItem(pemesanan, barang));
            }
        }
        catch(SQLiteException e) {
            Toast.makeText(this, R.string.error_SQL04, Toast.LENGTH_LONG).show();
        }
        finally {
            db.close();
            return itemList;
        }
    }

    /**
     * Method is called when getting item list.
     *
     * @return Returns item list.
     */
    public ArrayList<RowItem> getItemList()
    {
        return itemList;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode == getResources().getInteger(R.integer.detail_pemesanan)
                && resultCode == RESULT_OK && data != null) {
            thingsChanged = true;

            itemList = new ArrayList<RowItem>();
            createList();
            rowItemAdapter.replaceAll(itemList);
        }
    }

    @Override
    public void onBackPressed()
    {
        if(thingsChanged) {
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
        }

        finish();
    }
}
