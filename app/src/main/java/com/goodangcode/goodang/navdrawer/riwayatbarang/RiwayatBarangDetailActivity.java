package com.goodangcode.goodang.navdrawer.riwayatbarang;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.goodangcode.goodang.R;
import com.goodangcode.goodang.databasehelper.DatabaseController;
import com.goodangcode.goodang.databasemodel.Barang;
import com.goodangcode.goodang.databasemodel.Pemesanan;
import com.goodangcode.goodang.navdrawer.pindai.KonfirmasiBarangKeluarActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Detail Riwayat Barang - the things about getting detail list of riwayat barang.
 *
 * @author GoodangCode
 */
public class RiwayatBarangDetailActivity extends AppCompatActivity
{
    private Toolbar toolbar;
    private TableLayout tableLayout;
    private Button buttonDropBarang;

    private Pemesanan pemesanan;
    private Barang barang;

    private boolean thingsChanged;

    /**
     * Method is called when the activity is first created - normal static set up.
     *
     * @param savedInstanceState Initialization of the activity.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_riwayat_barang_detail);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Detail Pemesanan");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tableLayout = (TableLayout) findViewById(R.id.table_layout);
        buttonDropBarang = (Button) findViewById(R.id.button_drop_barang);

        pemesanan = getIntent().getExtras().getParcelable("pemesanan");
        barang = getIntent().getExtras().getParcelable("barang");

        if(pemesanan.getTanggalKeluar() != null) {
            buttonDropBarang.setEnabled(false);
        }

        buttonDropBarang.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                String contents =
                        "{\"kode\":\"" + pemesanan.getKode() + "\"," +
                                "\"kode_barang\":\"" + pemesanan.getKodeBarang() + "\"," +
                                "\"penerima\":\"" + pemesanan.getPenerima() + "\"," +
                                "\"alamat\":\"" + pemesanan.getAlamat() + "\"," +
                                "\"kecamatan\":\"" + pemesanan.getKecamatan() + "\"}";

                Intent intent = new Intent(view.getContext(), KonfirmasiBarangKeluarActivity.class);
                intent.putExtra("result", contents);
                intent.putExtra("format", "QR_CODE");
                intent.putExtra("source", "Riwayat Barang");
                startActivityForResult(intent, getResources().getInteger(R.integer.daftar_barang_konfirmasi_keluar));
            }
        });

        ((TextView) findViewById(R.id.kode_pemesanan)).setText(pemesanan.getKode());

        showDetailPemesanan(pemesanan, barang);
    }

    /**
     * Method is called whenever an item in your options menu is selected.
     *
     * @param item The menu item that was selected.
     * @return Return false to allow normal menu processing to proceed, true to consume it here.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home) {
            if(thingsChanged) {
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
            }
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Method is called when showing detail pemesanan data.
     *
     * @param pemesanan
     * @param barang
     */
    public void showDetailPemesanan(Pemesanan pemesanan, Barang barang)
    {
        String penerima = pemesanan.getPenerima();
        String tanggalMasuk = getDateFormat(pemesanan.getTanggalMasuk());
        String tanggalKeluar = getDateFormat(pemesanan.getTanggalKeluar());
        String lokasiRak = pemesanan.getKodeRak();

        DatabaseController db = DatabaseController.getInstance(this);
        String namaPetugas = db.getPetugas(pemesanan.getKodePetugas()).getNama();
        db.close();

        if(tanggalKeluar == null) {
            tanggalKeluar = "-";
        }

        String namaBarang = barang.getNama();
        String tipeBarang = barang.getTipe();

        String[][] attributes = { {"Nama Barang", namaBarang},
                {"Tipe Barang", tipeBarang},
                {"Penerima", penerima},
                {"Tanggal Masuk", tanggalMasuk},
                {"Tanggal Keluar", tanggalKeluar},
                {"Lokasi Rak", lokasiRak},
                {"Terakhir Diubah Oleh", namaPetugas} };

        for(int i = 0; i < attributes.length; i++) {
            String attributeNames = attributes[i][0];
            String attributeValues = attributes[i][1];

            View rowView = getLayoutInflater().inflate(R.layout.table_item, null);
            ((TextView) rowView.findViewById(R.id.column_1)).setText(attributeNames);
            ((TextView) rowView.findViewById(R.id.column_2)).setText(R.string.colon);
            ((TextView) rowView.findViewById(R.id.column_3)).setText(attributeValues);

            tableLayout.addView(rowView);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode == getResources().getInteger(R.integer.daftar_barang_konfirmasi_keluar)
                && resultCode == RESULT_OK && data != null) {
            thingsChanged = true;

            tableLayout.removeAllViews();
            pemesanan = data.getExtras().getParcelable("pemesanan");
            showDetailPemesanan(pemesanan, barang);
            buttonDropBarang.setEnabled(false);
        }
    }

    /**
     * Method is called when getting the time with the format.
     *
     * @param dateString The current date.
     * @return Return format string of current date.
     */
    public String getDateFormat(String dateString)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, d MMMM yyyy HH:mm", new Locale("in", "IDN"));
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        if(dateString != null) {
            Date date;
            try
            {
                date = formatter.parse(dateString);
                return dateFormat.format(date);
            }
            catch (ParseException e)
            {
                Toast.makeText(RiwayatBarangDetailActivity.this, R.string.error_PARSE01, Toast.LENGTH_SHORT).show();
            }
        }

        return dateString;
    }

    @Override
    public void onBackPressed()
    {
        if(thingsChanged) {
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
        }
        finish();
    }
}
