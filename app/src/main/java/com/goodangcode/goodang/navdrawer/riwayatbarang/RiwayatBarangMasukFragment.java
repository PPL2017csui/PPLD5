package com.goodangcode.goodang.navdrawer.riwayatbarang;


import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.goodangcode.goodang.R;
import com.goodangcode.goodang.RowItem;
import com.goodangcode.goodang.RowItemAdapter;
import com.goodangcode.goodang.RowItemEmptyAdapter;
import com.goodangcode.goodang.databasehelper.DatabaseController;
import com.goodangcode.goodang.databasemodel.Barang;
import com.goodangcode.goodang.databasemodel.Pemesanan;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

/**
 * Riwayat Barang Masuk - the things about showing of riwayat barang masuk.
 *
 * @author GoodangCode
 */
public class RiwayatBarangMasukFragment extends Fragment
{
    private RecyclerView recyclerView;
    private RowItemAdapter rowItemAdapter;
    private RowItemEmptyAdapter emptyAdapter;

    private ArrayList<RowItem> riwayatBarangMasukList;
    private View view;

    /**
     * There is a form of the constructor that are called when the view of riwayat barang masuk is created.
     */
    public RiwayatBarangMasukFragment()
    {
        riwayatBarangMasukList = new ArrayList<RowItem>();
    }

    /**
     * Method is called to have the fragment instantiate its user interface view.
     *
     * @param inflater The LayoutInflater object that can be used to inflate any views in the fragment.
     * @param container If non-null, this is the parent view that the fragment's UI should be attached to.
     *                  The fragment should not add the view itself, but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed from a previous saved state as given here.
     * @return Return the view for the fragment's UI, or null.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_riwayat_barang_masuk, container, false);

        tampilkanRiwayatBarangMasuk(getActivity());

        emptyAdapter = new RowItemEmptyAdapter(getClass());
        rowItemAdapter = new RowItemAdapter(getContext(), getClass(), riwayatBarangMasukList);
        sortList(R.string.string_tanggal_masuk_dsc);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        RecyclerView.ItemDecoration itemDecoration = new
                DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        if(riwayatBarangMasukList.isEmpty()) {
            recyclerView.setAdapter(emptyAdapter);
        }
        else {
            recyclerView.setAdapter(rowItemAdapter);
            recyclerView.addItemDecoration(itemDecoration);
        }

        setHasOptionsMenu(true);
        return view;
    }

    /**
     * Method is called when the list of riwayat barang want to created.
     *
     * @param activity Activity where the riwayatBarangMasukFragment in
     * @return List of RowItem that contain bunch of riwayatBarangMasuk
     */
    public ArrayList<RowItem> tampilkanRiwayatBarangMasuk(Activity activity)
    {
        DatabaseController db = DatabaseController.getInstance(activity.getBaseContext());

        Intent intent = activity.getIntent();
        Bundle bundle = intent.getExtras();

        ArrayList<Pemesanan> pemesanan = bundle.getParcelableArrayList("listRiwayatBarangMasuk");

        try {
            for (int i = 0; i < pemesanan.size(); i++) {
                Pemesanan currentPemesanan = pemesanan.get(i);
                Barang currentBarang = db.getBarang(currentPemesanan.getKodeBarang());

                riwayatBarangMasukList.add(new RowItem(currentPemesanan, currentBarang));
            }

            return riwayatBarangMasukList;
        }
        catch (SQLiteException ex) {
            ex.printStackTrace();
            return riwayatBarangMasukList;
        }
        finally {
            db.close();
            return riwayatBarangMasukList;
        }
    }

    /**
     * Method is called to inflate menu which has been created.
     *
     * @param menu Menu which being inflated.
     * @param inflater The MenuInflater object that can be used to inflate any menus.
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.options_menu, menu);

        searchList(menu);
        MenuItem menuSort = menu.findItem(R.id.sort);
        SubMenu subMenu = menuSort.getSubMenu();
        inflater.inflate(R.menu.sorting_menu, subMenu);

        subMenu.findItem(R.id.sort1_asc).setTitle(R.string.nama_asc);
        subMenu.findItem(R.id.sort1_dsc).setTitle(R.string.nama_dsc);
        subMenu.findItem(R.id.sort2_asc).setTitle(R.string.kode_asc);
        subMenu.findItem(R.id.sort2_dsc).setTitle(R.string.kode_dsc);
        subMenu.findItem(R.id.sort3_asc).setTitle(R.string.tanggal_masuk_asc);
        subMenu.findItem(R.id.sort3_dsc).setTitle(R.string.tanggal_masuk_dsc);

        subMenu.findItem(R.id.sort3_dsc).setChecked(true);
        sortList(R.string.string_tanggal_masuk_dsc);

        super.onCreateOptionsMenu(menu, inflater);
    }

    /**
     * Method is called whenever an item in your options menu is selected.
     *
     * @param item The menu item that was selected.
     * @return Return false to allow normal menu processing to proceed, true to consume it here.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        item.setChecked(true);

        switch(item.getItemId()) {
            case R.id.sort:
                return true;
            case R.id.sort1_asc:
                sortList(R.string.string_nama_asc);
                return true;
            case R.id.sort1_dsc:
                sortList(R.string.string_nama_dsc);
                return true;
            case R.id.sort2_asc:
                sortList(R.string.string_kode_asc);
                return true;
            case R.id.sort2_dsc:
                sortList(R.string.string_kode_dsc);
                return true;
            case R.id.sort3_asc:
                sortList(R.string.string_tanggal_masuk_asc);
                return true;
            case R.id.sort3_dsc:
                sortList(R.string.string_tanggal_masuk_dsc);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Method is called when doing search in the list.
     *
     * @param menu The menu that was selected.
     */
    public void searchList(final Menu menu)
    {
        rowItemAdapter.replaceAll(riwayatBarangMasukList);

        final MenuItem item = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setIconifiedByDefault(true);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String query)
            {
                if(!riwayatBarangMasukList.isEmpty()) {
                    ArrayList<RowItem> filteredList = filter(riwayatBarangMasukList, query);

                    if(filteredList.isEmpty()) {
                        emptyAdapter.setSearchResult(true);
                        recyclerView.setAdapter(emptyAdapter);
                    }
                    else {
                        recyclerView.setAdapter(rowItemAdapter);
                        rowItemAdapter.replaceAll(filteredList);
                    }
                }

                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query)
            {
                return false;
            }
        });

        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menu.getItem(1).setVisible(false);
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                menu.getItem(1).setVisible(true);
                return false;
            }
        });
    }

    /**
     * Method is called to sort the list by given category.
     *
     * @param category Category to sort the list.
     */
    public void sortList(final int category)
    {
        Collections.sort(riwayatBarangMasukList, new Comparator<RowItem>() {
            @Override
            public int compare(RowItem item1, RowItem item2)
            {
                String namaBarang1 = item1.getBarang().getNama().toLowerCase();
                String namaBarang2 = item2.getBarang().getNama().toLowerCase();
                String kodePemesanan1 = item1.getPemesanan().getKode().toLowerCase();
                String kodePemesanan2 = item2.getPemesanan().getKode().toLowerCase();
                String tanggalMasuk1 = item1.getPemesanan().getTanggalMasuk();
                String tanggalMasuk2 = item2.getPemesanan().getTanggalMasuk();

                switch(category) {
                    case R.string.string_nama_asc:
                        return namaBarang1.compareTo(namaBarang2);
                    case R.string.string_nama_dsc:
                        return namaBarang2.compareTo(namaBarang1);
                    case R.string.string_kode_asc:
                        return kodePemesanan1.compareTo(kodePemesanan2);
                    case R.string.string_kode_dsc:
                        return kodePemesanan2.compareTo(kodePemesanan1);
                    case R.string.string_tanggal_masuk_asc:
                        return tanggalMasuk1.compareTo(tanggalMasuk2);
                    case R.string.string_tanggal_masuk_dsc:
                        return tanggalMasuk2.compareTo(tanggalMasuk1);
                    default:
                        return namaBarang1.compareTo(namaBarang2);
                }
            }
        });

        rowItemAdapter.replaceAll(riwayatBarangMasukList);
    }

    /**
     * Method is called to filter the list with given query.
     *
     * @param items The list which being filtered.
     * @param query String to filter the list.
     * @return Return the filtered list.
     */
    private ArrayList<RowItem> filter(ArrayList<RowItem> items, String query) {
        String lowerCaseQuery = query.toLowerCase();

        ArrayList<RowItem> filteredList = new ArrayList<>();

        for(RowItem item : items) {
            String namaBarang = item.getBarang().getNama().toLowerCase();
            String kodePemesanan = item.getPemesanan().getKode().toLowerCase();
            String tanggalMasuk = item.getPemesanan().getTanggalMasuk();
            String tanggalMasukFormat = getDateFormat(tanggalMasuk).toLowerCase();
            String waktuMasukFormat = getTimeFormat(tanggalMasuk);

            if(namaBarang.contains(lowerCaseQuery) || kodePemesanan.contains(lowerCaseQuery) ||
                    tanggalMasukFormat.contains(lowerCaseQuery) || waktuMasukFormat.contains(lowerCaseQuery)) {
                filteredList.add(item);
            }
        }
        return filteredList;
    }

    /**
     * Method is called when getting the time with the format.
     *
     * @param dateString The current date.
     * @return Return format string of the date.
     */
    public String getDateFormat(String dateString)
    {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy", new Locale("in", "IDN"));

        if(dateString != null) {
            Date date;
            try
            {
                date = formatter.parse(dateString);
                return dateFormat.format(date);
            }
            catch (ParseException e)
            {
                Toast.makeText(getContext(), R.string.error_PARSE01, Toast.LENGTH_SHORT).show();
            }
        }

        return dateString;
    }

    /**
     * Method is called when getting the time with the format.
     *
     * @param dateString The current date.
     * @return Return format string of the time.
     */
    public String getTimeFormat(String dateString)
    {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

        if(dateString != null) {
            Date date;
            try
            {
                date = formatter.parse(dateString);
                return timeFormat.format(date);
            }
            catch (ParseException e)
            {
                Toast.makeText(getContext(), R.string.error_PARSE01, Toast.LENGTH_SHORT).show();
            }
        }

        return dateString;
    }
}
