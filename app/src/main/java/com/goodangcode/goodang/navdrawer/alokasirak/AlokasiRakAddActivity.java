package com.goodangcode.goodang.navdrawer.alokasirak;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.goodangcode.goodang.R;
import com.goodangcode.goodang.Syncronization;
import com.goodangcode.goodang.databasehelper.DatabaseController;
import com.goodangcode.goodang.databasemodel.Rak;


/**
 * Alokasi Rak Tambah - the things about adding alokasi rak.
 *
 * @author GoodangCode
 */
public class AlokasiRakAddActivity extends AppCompatActivity
{
    private Toolbar toolbar;

    private String kodeRak;
    private String tipeRak;

    private AlertDialog.Builder alert;

    public EditText editTextKodeRak;
    public EditText editTextTipeRak;

    public Button buttonAddSimpan;

    /**
     * Method is called when the activity is first created - normal static set up.
     *
     * @param savedInstanceState Initialization of the activity.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alokasi_rak_add);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Tambah Rak");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        addListenerOnButton();
    }

    public void addListenerOnButton()
    {
        editTextKodeRak = ((EditText) findViewById(R.id.kode_rak));
        editTextTipeRak = ((EditText) findViewById(R.id.tipe_rak));

        buttonAddSimpan = (Button) findViewById(R.id.button_add_simpan);

        alert = new AlertDialog.Builder(this);
        alert.setCancelable(false);

        buttonAddSimpan.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(checkInputValue()) {
                    alert.setTitle(R.string.dialog_title_add);
                    alert.setMessage(R.string.dialog_message_rak_add_tambah);

                    alert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton)
                        {
                            DatabaseController db = DatabaseController.getInstance(AlokasiRakAddActivity.this);
                            String kodeGudang = db.getKodeGudang();
                            Rak rak = new Rak(kodeRak, kodeGudang, tipeRak, 0);
                            db.addRak(rak);
                            db.close();

                            Toast.makeText(AlokasiRakAddActivity.this, R.string.dialog_next_rak_add_tambah_ya,
                                    Toast.LENGTH_SHORT).show();

                            new Syncronization(AlokasiRakAddActivity.this, rak, "Add");

                            Intent intent = new Intent();
                            Bundle bundle = new Bundle();
                            intent.putExtras(bundle);
                            setResult(RESULT_OK, intent);

                            finish();
                        }
                    });

                    alert.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton)
                        {
                            Toast.makeText(AlokasiRakAddActivity.this, R.string.dialog_next_rak_add_tidak,
                                    Toast.LENGTH_LONG).show();
                        }
                    });

                    alert.show();
                }
            }
        });
    }

    public boolean checkInputValue()
    {
        kodeRak = editTextKodeRak.getText().toString().toUpperCase();
        tipeRak = editTextTipeRak.getText().toString();

        if(kodeRak.equals("") && tipeRak.equals("")) {
            Toast.makeText(AlokasiRakAddActivity.this, R.string.error_RAK08,
                    Toast.LENGTH_LONG).show();
        }
        else if(kodeRak.equals("")) {
            Toast.makeText(AlokasiRakAddActivity.this, R.string.error_RAK09,
                    Toast.LENGTH_LONG).show();
        }
        else if(tipeRak.equals("")) {
            Toast.makeText(AlokasiRakAddActivity.this, R.string.error_RAK10,
                    Toast.LENGTH_LONG).show();
        }
        else if(!kodeRak.equals("")) {
            DatabaseController db = DatabaseController.getInstance(AlokasiRakAddActivity.this);
            boolean rakIsExist = db.getRak(kodeRak) != null;
            db.close();

            if(!rakIsExist) {
                return true;
            }
            else {
                Toast.makeText(AlokasiRakAddActivity.this, R.string.error_RAK11,
                        Toast.LENGTH_LONG).show();
            }
        }

        return false;
    }

    /**
     * Method is called whenever an item in your options menu is selected.
     *
     * @param item The menu item that was selected.
     * @return Return false to allow normal menu processing to proceed, true to consume it here.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home) {
            doCancellation();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        doCancellation();
    }

    private void doCancellation()
    {
        alert.setTitle(R.string.dialog_title_cancel);
        alert.setMessage(R.string.dialog_message_rak_add_batal);

        alert.setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton)
            {
                Toast.makeText(AlokasiRakAddActivity.this, R.string.dialog_next_rak_add_batal_ya,
                        Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        alert.setNegativeButton(R.string.button_no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton)
            {
                Toast.makeText(AlokasiRakAddActivity.this, R.string.dialog_next_rak_add_tidak,
                        Toast.LENGTH_SHORT).show();
            }
        });

        alert.show();
    }
}

