package com.goodangcode.goodang;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.goodangcode.goodang.databasehelper.DatabaseController;
import com.goodangcode.goodang.databasemodel.Barang;
import com.goodangcode.goodang.databasemodel.Pemesanan;
import com.goodangcode.goodang.navdrawer.AlokasiRak;
import com.goodangcode.goodang.navdrawer.DaftarBarang;
import com.goodangcode.goodang.navdrawer.alokasirak.AlokasiRakDetailActivity;
import com.goodangcode.goodang.navdrawer.alokasirak.AlokasiRakInformationActivity;
import com.goodangcode.goodang.navdrawer.daftarbarang.DaftarBarangDetailActivity;
import com.goodangcode.goodang.navdrawer.riwayatbarang.RiwayatBarangDetailActivity;
import com.goodangcode.goodang.navdrawer.riwayatbarang.RiwayatBarangKeluarFragment;
import com.goodangcode.goodang.navdrawer.riwayatbarang.RiwayatBarangMasukFragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * An Adapter object acts as a bridge between an AdapterView and the underlying data for that view.
 * The Adapter provides access to the data items. The Adapter is also responsible for making a View
 * for each item in the data set.
 *
 * @author GoodangCode
 */
public class RowItemAdapter extends RecyclerView.Adapter<RowItemAdapter.ViewHolder>
{
    private Context context;
    private Class c;
    private ArrayList<RowItem> itemList;

    /**
     * There is a form of the constructor that are called when the view of adapter is created.
     *
     * @param context The initialization of the context.
     * @param itemList The initialization of the item list.
     */
    public RowItemAdapter(Context context, Class c, ArrayList<RowItem> itemList)
    {
        this.context = context;
        this.c = c;
        this.itemList = itemList;
    }

    /**
     * A ViewHolder describes an item view and metadata about its place.
     */
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        public TextView namaBarang;
        public TextView kode;
        public TextView keterangan1;
        public TextView keterangan2;

        public TextView column1;
        public TextView column2;
        public TextView column3;

        public View itemView;

        /**
         * There is a form of the constructor that are called when the view of adapter is created.
         *
         * @param itemView The initialization of the view.
         */
        public ViewHolder(View itemView)
        {
            super(itemView);

            this.itemView = itemView;

            if(c.equals(DaftarBarangDetailActivity.class) || c.equals(AlokasiRak.class)) {
                column1 = (TextView) itemView.findViewById(R.id.column_1);
                column2 = (TextView) itemView.findViewById(R.id.column_2);
                column3 = (TextView) itemView.findViewById(R.id.column_3);
            }
            else {
                namaBarang = (TextView) itemView.findViewById(R.id.nama_barang);
                kode = (TextView) itemView.findViewById(R.id.kode);
                keterangan1 = (TextView) itemView.findViewById(R.id.keterangan1);
                keterangan2 = (TextView) itemView.findViewById(R.id.keterangan2);
            }

            itemView.setOnClickListener(this);
        }

        /**
         * Method is called when each row item is clicked.
         *
         * @param view View of row item that is clicked.
         */
        @Override
        public void onClick(View view)
        {
            int position = getAdapterPosition();

            if(position != RecyclerView.NO_POSITION) {
                Intent intent;
                Bundle bundle = new Bundle();

                if(c.equals(DaftarBarang.class)) {
                    int count = getItem(position).countBarang(context);
                    if(count == 0) {
                        Toast.makeText(context, R.string.barang_no_pemesanan, Toast.LENGTH_SHORT).show();
                    }
                    else {
                        intent = new Intent(view.getContext(), DaftarBarangDetailActivity.class);
                        bundle.putParcelable("barang", getItem(position).getBarang());
                        intent.putExtras(bundle);

                        ((Activity) context).startActivityForResult(intent,
                                context.getResources().getInteger(R.integer.daftar_barang_detail));
                    }
                }
                else if(c.equals(AlokasiRak.class)) {
                    intent = new Intent(view.getContext(), AlokasiRakInformationActivity.class);
                    bundle.putParcelable("rak", getItem(position).getRak());
                    intent.putExtras(bundle);

                    ((Activity) context).startActivityForResult(intent,
                            context.getResources().getInteger(R.integer.alokasi_rak));
                }
                else if(c.equals(AlokasiRakDetailActivity.class)) {
                    Activity activity = (Activity) context;

                    DatabaseController db = DatabaseController.getInstance(activity);
                    Pemesanan pemesanan = getItem(position).getPemesanan();
                    Barang barang = db.getBarang(pemesanan.getKodeBarang());
                    db.close();

                    intent = new Intent(view.getContext(), RiwayatBarangDetailActivity.class);
                    bundle.putParcelable("pemesanan", pemesanan);
                    bundle.putParcelable("barang", barang);
                    intent.putExtra("source", "Alokasi Rak Detail");
                    intent.putExtras(bundle);

                    activity.startActivityForResult(intent,
                            context.getResources().getInteger(R.integer.detail_pemesanan));
                }
                else {
                    intent = new Intent(view.getContext(), RiwayatBarangDetailActivity.class);
                    bundle.putParcelable("pemesanan", getItem(position).getPemesanan());
                    bundle.putParcelable("barang", getItem(position).getBarang());
                    intent.putExtras(bundle);

                    if(c.equals(DaftarBarangDetailActivity.class)) {
                        intent.putExtra("source", "Daftar Barang Detail");

                        ((Activity) context).startActivityForResult(intent,
                                context.getResources().getInteger(R.integer.detail_pemesanan));
                    }
                    else {
                        intent.putExtra("source", "Riwayat Barang");

                        ((Activity) context).startActivityForResult(intent,
                                context.getResources().getInteger(R.integer.detail_pemesanan));
                    }
                }
            }
        }

        /**
         * Method is called when getting row item in particular item list position.
         *
         * @param position Position of row item in item list.
         * @return Return object that represents row item.
         */
        public RowItem getItem(int position)
        {
            return itemList.get(position);
        }
    }

    /**
     * Method is called to crete initialization of view holder.
     *
     * @param parent Parent of view to inflate other specific view.
     * @param viewType Type of the view.
     * @return View holder class.
     */
    @Override
    public RowItemAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View rowItemView;

        if(c.equals(DaftarBarang.class)) {
            rowItemView = inflater.inflate(R.layout.row_item_view_daftar_barang, parent, false);
        }
        else if(c.equals(DaftarBarangDetailActivity.class)) {
            rowItemView = inflater.inflate(R.layout.row_item_view_daftar_barang_detail, parent, false);
        }
        else if(c.equals(AlokasiRak.class)) {
            rowItemView = inflater.inflate(R.layout.row_item_view_alokasi_rak, parent, false);
        }
        else if(c.equals(AlokasiRakDetailActivity.class)) {
            rowItemView = inflater.inflate(R.layout.row_item_view_alokasi_rak_detail, parent, false);
        }
        else {
            rowItemView = inflater.inflate(R.layout.row_item_view, parent, false);
        }

        ViewHolder viewHolder = new ViewHolder(rowItemView);
        return viewHolder;
    }


    /**
     * Method is called to bind each view of row item to view holder.
     *
     * @param viewHolder View holder which will be used.
     * @param position Position of row item.
     */
    @Override
    public void onBindViewHolder(RowItemAdapter.ViewHolder viewHolder, int position)
    {
        RowItem item = itemList.get(position);

        String namaBarang = "";
        String kode = "";
        String keterangan1 = "";
        String keterangan2 = "";

        String column1 = "";
        String column2 = "";
        String column3 = "";

        if(c.equals(DaftarBarangDetailActivity.class)) {
            column1 = item.getPemesanan().getKode();
            column2 = item.getPemesanan().getPenerima();
            column3 = item.getPemesanan().getKodeRak();

            viewHolder.column1.setText(column1);
            viewHolder.column2.setText(column2);
            viewHolder.column3.setText(column3);
        }
        else if(c.equals(AlokasiRak.class)) {
            column1 = item.getRak().getKode();
            column2 = item.getRak().getTipe();
            column3 = (item.getRak().getStatusPenuh() == 1) ? "Penuh" : "Tersedia";

            viewHolder.column1.setText(column1);
            viewHolder.column2.setText(column2);
            viewHolder.column3.setText(column3);
        }
        else {
            if(c.equals(DaftarBarang.class)) {
                Activity activity = (Activity) context;

                namaBarang = item.getBarang().getNama();
                kode = item.getBarang().getKode();

                DatabaseController db = DatabaseController.getInstance(activity);
                int count = db.getBarangCount(kode);
                db.close();

                keterangan1 = String.valueOf(count);
                viewHolder.keterangan1.setText(keterangan1);
            }
            else if(c.equals(AlokasiRakDetailActivity.class)) {
                kode = item.getPemesanan().getKode();
                namaBarang = item.getBarang().getNama();
            }
            else {
                namaBarang = item.getBarang().getNama();
                kode = item.getPemesanan().getKode();

                if(c.equals(RiwayatBarangMasukFragment.class)) {
                    keterangan1 = getDateFormat(item.getPemesanan().getTanggalMasuk());
                    keterangan2 = getTimeFormat(item.getPemesanan().getTanggalMasuk());
                }
                else if(c.equals(RiwayatBarangKeluarFragment.class)) {
                    keterangan1 = getDateFormat(item.getPemesanan().getTanggalKeluar());
                    keterangan2 = getTimeFormat(item.getPemesanan().getTanggalKeluar());
                }

                viewHolder.keterangan1.setText(keterangan1);
                viewHolder.keterangan2.setText(keterangan2);
            }

            viewHolder.namaBarang.setText(namaBarang);
            viewHolder.kode.setText(kode);
        }
    }

    /**
     * Method is called when search is running to replace current item list with filtered item list.
     *
     * @param items Filtered item list.
     */
    public void replaceAll(ArrayList<RowItem> items)
    {
        itemList = new ArrayList<RowItem>();
        itemList.addAll(items);
        notifyDataSetChanged();
    }

    /**
     * Method is called when counting the items in the data set represented by this Adapter.
     *
     * @return Return count of items.
     */
    @Override
    public int getItemCount()
    {
        return itemList.size();
    }

    /**
     * Method is called when getting the time with the format.
     *
     * @param dateString The current date.
     * @return Return format string of the date.
     */
    public String getDateFormat(String dateString)
    {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy", new Locale("in", "IDN"));

        if(dateString != null) {
            Date date;
            try
            {
                date = formatter.parse(dateString);
                return dateFormat.format(date);
            }
            catch (ParseException e)
            {
                Toast.makeText(context, R.string.error_PARSE01, Toast.LENGTH_SHORT).show();
            }
        }

        return dateString;
    }

    /**
     * Method is called when getting the time with the format.
     *
     * @param dateString The current date.
     * @return Return format string of the time.
     */
    public String getTimeFormat(String dateString)
    {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

        if(dateString != null) {
            Date date;
            try
            {
                date = formatter.parse(dateString);
                return dateFormat.format(date);
            }
            catch (ParseException e)
            {
                Toast.makeText(context, R.string.error_PARSE02, Toast.LENGTH_SHORT).show();
            }
        }

        return dateString;
    }
}
