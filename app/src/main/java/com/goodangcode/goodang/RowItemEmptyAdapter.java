package com.goodangcode.goodang;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.goodangcode.goodang.navdrawer.AlokasiRak;
import com.goodangcode.goodang.navdrawer.DaftarBarang;
import com.goodangcode.goodang.navdrawer.riwayatbarang.RiwayatBarangKeluarFragment;
import com.goodangcode.goodang.navdrawer.riwayatbarang.RiwayatBarangMasukFragment;

/**
 * An Adapter object acts as a bridge between an AdapterView and the underlying data for that view.
 * The Adapter provides access to the data items. The Adapter is also responsible for making a View
 * for each item in the data set.
 *
 * @author GoodangCode
 */
public class RowItemEmptyAdapter extends RecyclerView.Adapter<RowItemEmptyAdapter.ViewHolder>
{
    private Class c;
    private boolean isSearchResult;

    /**
     * There is a form of the constructor that are called when the view of adapter is created.
     *
     * @param c The class accessed this adapter
     */
    public RowItemEmptyAdapter(Class c)
    {
        this.c = c;
        this.isSearchResult = false;
    }

    /**
     * A ViewHolder describes an item view and metadata about its place.
     */
    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView centerText;
        public View itemView;

        /**
         * There is a form of the constructor that are called when the view of adapter is created.
         *
         * @param itemView The initialization of the view.
         */
        public ViewHolder(View itemView)
        {
            super(itemView);
            this.itemView = itemView;
            centerText = (TextView) itemView.findViewById(R.id.center_text);
        }
    }

    /**
     * Method is called to crete initialization of view holder.
     *
     * @param parent Parent of view to inflate other specific view.
     * @param viewType Type of the view.
     * @return View holder class.
     */
    @Override
    public RowItemEmptyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View rowItemView = inflater.inflate(R.layout.row_item_no_item_view, parent, false) ;
        ViewHolder viewHolder = new ViewHolder(rowItemView);

        return viewHolder;
    }

    /**
     * Method is called to bind each view of row item to view holder.
     *
     * @param viewHolder View holder which will be used.
     * @param position Position of row item.
     */
    @Override
    public void onBindViewHolder(RowItemEmptyAdapter.ViewHolder viewHolder, int position)
    {
        if(isSearchResult) {
            viewHolder.centerText.setText(R.string.search_no_result);
        }
        else {
            if(c.equals(RiwayatBarangMasukFragment.class)) {
                viewHolder.centerText.setText(R.string.riwayat_masuk_no_item);
            }
            else if(c.equals(RiwayatBarangKeluarFragment.class)) {
                viewHolder.centerText.setText(R.string.riwayat_keluar_no_item);
            }
            else if(c.equals(DaftarBarang.class)) {
                viewHolder.centerText.setText(R.string.barang_no_item);
            }
            else if(c.equals(AlokasiRak.class)) {
                viewHolder.centerText.setText(R.string.rak_no_item);
            }
        }
    }

    /**
     * Method is called when counting the items in the data set represented by this Adapter.
     *
     * @return Return count of items.
     */
    @Override
    public int getItemCount()
    {
        return 1;
    }

    /**
     * Gets search status, on searching or not.
     *
     * @return Return search status.
     */
    public boolean isSearchResult()
    {
        return isSearchResult;
    }

    /**
     * Sets search status with the specified parameter.
     */
    public void setSearchResult(boolean searchResult)
    {
        isSearchResult = searchResult;
    }
}
