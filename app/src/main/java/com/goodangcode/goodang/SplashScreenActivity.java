package com.goodangcode.goodang;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.goodangcode.goodang.databasehelper.DatabaseController;
import com.crashlytics.android.Crashlytics;
import com.goodangcode.goodang.login.LoginActivity;

import io.fabric.sdk.android.Fabric;


/**
 * Implementation of splash screen than can be displayed at application startup.
 *
 * @author GoodangCode
 */
public class SplashScreenActivity extends Activity
{
    /**
     * Method is called when the activity is first created - normal static set up.
     *
     * @param savedInstanceState Initialization of the activity.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash_screen);

        Thread timeThread = new Thread()
        {
            /**
             * If this thread was constructed using a separate Runnable run object, then that Runnable object's
             * run method is called. Otherwise, this method does nothing and returns.
             */
            public void run()
            {
                try {
                    sleep(1000);
                }
                catch(InterruptedException e){
                    e.printStackTrace();
                }
                finally {
                    Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                    if(SessionManager.getInstance(getApplicationContext()).isLoggedIn()){
                        intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                    }
                    startActivity(intent);
                }
            }
        };

        int countBarang = DatabaseController.getInstance(getApplicationContext()).countBarang();
        int countRak = DatabaseController.getInstance(getApplicationContext()).countRak();
        int countGudang = DatabaseController.getInstance(getApplicationContext()).countGudang();
        DatabaseController.getInstance(getApplicationContext()).close();

        if(countBarang != 0 && countGudang != 0 && countRak != 0){
            timeThread.start();
        }
        else {
            TextView textViewNotif = (TextView) findViewById(R.id.text_splash_notify);
            if(!NetworkMonitor.checkNetworkConnection(SplashScreenActivity.this)) {

                textViewNotif.setText("Hubungkan perangkat anda ke internet!");
            }
            final Handler handler = new Handler();
            Thread sync = new Thread() {
                @Override
                public void run() {
                    try {
                        while (!NetworkMonitor.checkNetworkConnection(SplashScreenActivity.this)) {
                            Thread.sleep(1000);
                        }
                        handler.post(new Runnable() {
                            public void run() {
                                TextView textViewNotif = (TextView) findViewById(R.id.text_splash_notify);
                                textViewNotif.setText("Sinkronisasi data server");
                            }
                        });
                        Thread.sleep(1000);

                        DatabaseController.getInstance(getApplicationContext())
                                .syncDatabase(getApplicationContext());
                        DatabaseController.getInstance(getApplicationContext()).close();

                        int checkBarang = DatabaseController.getInstance(getApplicationContext()).countBarang();
                        int checkRak = DatabaseController.getInstance(getApplicationContext()).countRak();
                        int checkGudang = DatabaseController.getInstance(getApplicationContext()).countGudang();
                        DatabaseController.getInstance(getApplicationContext()).close();
                        while (checkBarang != 0 && checkGudang != 0 && checkRak != 0){
                            checkBarang = DatabaseController.getInstance(getApplicationContext()).countBarang();
                            checkRak = DatabaseController.getInstance(getApplicationContext()).countRak();
                            checkGudang = DatabaseController.getInstance(getApplicationContext()).countGudang();
                            DatabaseController.getInstance(getApplicationContext()).close();
                            Thread.sleep(1000);
                        }

                        Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                        if(SessionManager.getInstance(getApplicationContext()).isLoggedIn()){
                            intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                        }
                        startActivity(intent);

                    } catch (Exception e) {
//                        Toast.makeText(SplashScreenActivity.this, R.string.error_SYNC01, Toast.LENGTH_SHORT).show();
                        Log.e("Exception ",e.getMessage());
                        e.printStackTrace();
                    }
                }
            };
            sync.start();
        }
    }

    /**
     * Method is called when the system is about to start resuming a previous activity.
     */
    @Override
    protected void onPause()
    {
        super.onPause();
        finish();
    }
}
