package com.goodangcode.goodang.databasemodel;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Class Pemesanan is the model of database.
 *
 * @author GoodangCode
 */
public class Pemesanan implements Parcelable
{
    private String kode;
    private int kodePetugas;
    private String kodeGudang;
    private String kodeBarang;
    private String kodeRak;
    private String penerima;
    private String alamat;
    private String kecamatan;
    private String tanggalMasuk;
    private String tanggalKeluar;
    private int sinkronisasi;
    private String terakhirDiubah;

    /**
     * There is a form of the constructor that are called when the object of pemesanan is created
     * with the specified parameter.
     *
     * @param kode Kode pemesanan.
     * @param kodePetugas Kode petugas.
     * @param kodeGudang Kode gudang.
     * @param kodeBarang Kode barang.
     * @param kodeRak Kode rak.
     * @param penerima Penerima.
     * @param alamat Alamat penerima.
     * @param kecamatan Kecamatan penerima.
     * @param tanggalMasuk Tanggal masuk barang.
     * @param tanggalKeluar Tanggal keluar barang.
     */
    public Pemesanan(String kode, int kodePetugas, String kodeGudang, String kodeBarang, String kodeRak, String penerima,
                     String alamat, String kecamatan, String tanggalMasuk, String tanggalKeluar)
    {
        this.kode = kode;
        this.kodePetugas = kodePetugas;
        this.kodeGudang = kodeGudang;
        this.kodeBarang = kodeBarang;
        this.kodeRak = kodeRak;
        this.penerima = penerima;
        this.alamat = alamat;
        this.kecamatan = kecamatan;
        this.tanggalMasuk = tanggalMasuk;
        this.tanggalKeluar = tanggalKeluar;
        this.sinkronisasi = 0;
        this.terakhirDiubah = tanggalMasuk;
    }

    /**
     * There is a form of the constructor that are called when the object of pemesanan is created
     * with the specified parcel.
     *
     * @param parcel Parcel pemesanan.
     */
    public Pemesanan (Parcel parcel)
    {
        this.kode = parcel.readString();
        this.kodePetugas = parcel.readInt();
        this.kodeBarang = parcel.readString();
        this.kodeRak = parcel.readString();
        this.penerima = parcel.readString();
        this.alamat = parcel.readString();
        this.kecamatan = parcel.readString();
        this.tanggalMasuk = parcel.readString();
        this.tanggalKeluar = parcel.readString();
        this.sinkronisasi = parcel.readInt();
        this.terakhirDiubah = parcel.readString();
    }

    /**
     * There is a form of the constructor that are called when the object of pemesanan is created
     * with the specified parameter.
     *
     * @param kode Kode pemesanan.
     * @param kodePetugas Kode petugas.
     * @param kodeGudang Kode gudang.
     * @param kodeBarang Kode barang.
     * @param penerima Penerima.
     * @param alamat Alamat penerima.
     * @param kecamatan Kecamatan penerima.
     */
    public Pemesanan(String kode, int kodePetugas, String kodeGudang, String kodeBarang, String penerima, String alamat, String kecamatan)
    {
        this(kode, kodePetugas, kodeGudang, kodeBarang, null, penerima, alamat, kecamatan, null, null);
    }

    /**
     * There is a form of the constructor that are called when the object of pemesanan is created.
     */
    public Pemesanan()
    {
        this(null, 0, null, null, null, null, null, null, null, null);
    }

    /**
     * Gets kode pemesanan.
     *
     * @return Return kode pemesanan.
     */
    public String getKode()
    {
        return kode;
    }

    /**
     * Sets kode pemesanan with the specified parameter.
     */
    public void setKode(String kode)
    {
        this.kode = kode;
    }

    /**
     * Gets kode petugas.
     *
     * @return Return kode petugas.
     */
    public int getKodePetugas()
    {
        return kodePetugas;
    }

    /**
     * Sets kode petugas with the specified parameter.
     */
    public void setKodePetugas(int kodePetugas)
    {
        this.kodePetugas = kodePetugas;
    }

    /**
     * Gets kode gudang.
     *
     * @return Return kode gudang.
     */
    public String getKodeGudang()
    {
        return kodeGudang;
    }

    /**
     * Sets kode gudang with the specified parameter.
     */
    public void setKodeGudang(String kodeGudang)
    {
        this.kodeGudang = kodeGudang;
    }

    /**
     * Gets kode barang.
     *
     * @return Return kode barang.
     */
    public String getKodeBarang()
    {
        return kodeBarang;
    }

    /**
     * Sets kode barang with the specified parameter.
     */
    public void setKodeBarang(String kodeBarang)
    {
        this.kodeBarang = kodeBarang;
    }

    /**
     * Gets kode rak.
     *
     * @return Return kode rak.
     */
    public String getKodeRak()
    {
        return kodeRak;
    }

    /**
     * Sets kode rak with the specified parameter.
     */
    public void setKodeRak(String kodeRak)
    {
        this.kodeRak = kodeRak;
    }

    /**
     * Gets penerima.
     *
     * @return Return penerima.
     */
    public String getPenerima()
    {
        return penerima;
    }

    /**
     * Sets penerima with the specified parameter.
     */
    public void setPenerima(String penerima)
    {
        this.penerima = penerima;
    }

    /**
     * Gets alamat penerima.
     *
     * @return Return alamat penerima.
     */
    public String getAlamat()
    {
        return alamat;
    }

    /**
     * Sets alamat penerima with the specified parameter.
     */
    public void setAlamat(String alamat)
    {
        this.alamat = alamat;
    }

    /**
     * Gets kecamatan penerima.
     *
     * @return Return kecamatan penerima.
     */
    public String getKecamatan()
    {
        return kecamatan;
    }

    /**
     * Sets kecamatan penerima with the specified parameter.
     */
    public void setKecamatan(String kecamatan)
    {
        this.kecamatan = kecamatan;
    }

    /**
     * Gets the time of tanggal masuk.
     *
     * @return Return tanggal masuk.
     */
    public String getTanggalMasuk()
    {
        return tanggalMasuk;
    }

    /**
     * Sets the time of tanggal masuk with the specified parameter.
     */
    public void setTanggalMasuk(String tanggalMasuk)
    {
        this.tanggalMasuk = tanggalMasuk;
    }

    /**
     * Gets the time of tanggal keluar.
     *
     * @return Return tanggal keluar.
     */
    public String getTanggalKeluar()
    {
        return tanggalKeluar;
    }

    /**
     * Sets the time of tanggal keluar with the specified parameter.
     */
    public void setTanggalKeluar(String tanggalKeluar)
    {
        this.tanggalKeluar = tanggalKeluar;
    }

    /**
     * Gets the flag of sinkronisasi.
     *
     * @return Return the flag of tanggal masuk.
     */
    public int getSinkronisasi()
    {
        return sinkronisasi;
    }

    /**
     * Sets the flag of sinkronisasi with the specified parameter.
     */
    public void setSinkronisasi(int sinkronisasi)
    {
        this.sinkronisasi = sinkronisasi;
    }

    /**
     * Gets the time of terakhir diubah.
     *
     * @return Return the time of tanggal masuk.
     */
    public String getTerakhirDiubah()
    {
        return terakhirDiubah;
    }

    /**
     * Sets the time of terakhir diubah with the specified parameter.
     */
    public void setTerakhirDiubah(String terakhirDiubah)
    {
        this.terakhirDiubah = terakhirDiubah;
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param o The reference object with which to compare.
     * @return Return true if this object is the same as the obj argument, false otherwise.
     */
    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Pemesanan)) return false;

        Pemesanan pemesanan = (Pemesanan) o;

        if (kodePetugas != pemesanan.kodePetugas) return false;
        if (sinkronisasi != pemesanan.sinkronisasi) return false;
        if (!kode.equals(pemesanan.kode)) return false;
        if (!kodeGudang.equals(pemesanan.kodeGudang)) return false;
        if (!kodeBarang.equals(pemesanan.kodeBarang)) return false;
        if (!kodeRak.equals(pemesanan.kodeRak)) return false;
        if (!penerima.equals(pemesanan.penerima)) return false;
        if (!alamat.equals(pemesanan.alamat)) return false;
        if (!kecamatan.equals(pemesanan.kecamatan)) return false;
        if (!tanggalMasuk.equals(pemesanan.tanggalMasuk)) return false;

        return terakhirDiubah.equals(pemesanan.terakhirDiubah);
    }

    /**
     * Describe the kinds of special objects contained in this Parcelable instance's marshaled representation.
     *
     * @return Return a bitmask indicating the set of special object types marshaled by this Parcelable object instance.
     */
    @Override
    public int describeContents()
    {
        return 0;
    }

    /**
     * Flatten this object in to a Parcel.
     *
     * @param dest The Parcel in which the object should be written.
     * @param flags Additional flags about how the object should be written. May be 0 or PARCELABLE_WRITE_RETURN_VALUE.
     */
    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(kode);
        dest.writeInt(kodePetugas);
        dest.writeString(kodeBarang);
        dest.writeString(kodeRak);
        dest.writeString(penerima);
        dest.writeString(alamat);
        dest.writeString(kecamatan);
        dest.writeString(tanggalMasuk);
        dest.writeString(tanggalKeluar);
        dest.writeInt(sinkronisasi);
        dest.writeString(terakhirDiubah);
    }

    public static Creator<Pemesanan> CREATOR = new Creator<Pemesanan>()
    {
        @Override
        public Pemesanan createFromParcel(Parcel source)
        {
            return new Pemesanan(source);
        }

        @Override
        public Pemesanan[] newArray(int size)
        {
            return new Pemesanan[size];
        }
    };
}
