package com.goodangcode.goodang.databasemodel;


/**
 * Class Gudang is the model of database.
 *
 * @author GoodangCode
 */
public class Gudang
{
    private String kode;
    private String lokasi;

    /**
     * There is a form of the constructor that are called when the object of gudang is created
     * with the specified parameter.
     *
     * @param kode Kode gudang.
     * @param lokasi Lokasi gudang.
     */
    public Gudang(String kode, String lokasi)
    {
        this.kode = kode;
        this.lokasi = lokasi;
    }

    /**
     * Gets kode gudang.
     *
     * @return Return kode gudang.
     */
    public String getKode()
    {
        return kode;
    }

    /**
     * Sets kode gudang with the specified parameter.
     */
    public void setKode(String kode)
    {
        this.kode = kode;
    }

    /**
     * Gets lokasi gudang.
     *
     * @return Return lokasi gudang.
     */
    public String getLokasi()
    {
        return lokasi;
    }

    /**
     * Sets lokasi gudang with the specified parameter.
     */
    public void setLokasi(String lokasi)
    {
        this.lokasi = lokasi;
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param o The reference object with which to compare.
     * @return Return true if this object is the same as the obj argument, false otherwise.
     */
    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Gudang)) return false;

        Gudang gudang = (Gudang) o;

        if (!kode.equals(gudang.kode)) return false;
        return lokasi.equals(gudang.lokasi);
    }
}
