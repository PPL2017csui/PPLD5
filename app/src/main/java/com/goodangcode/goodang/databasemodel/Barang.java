package com.goodangcode.goodang.databasemodel;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Class Barang is the model of database.
 *
 * @author GoodangCode
 */
public class Barang implements Parcelable
{
    private String kode;
    private String nama;
    private String tipe;
    private int isBeraturan;

    /**
     * There is a form of the constructor that are called when the object of barang is created.
     */
    public Barang()
    {
        this(null, null, null, 0);
    }

    /**
     * There is a form of the constructor that are called when the object of barang is created
     * with the specified parameter.
     *
     * @param kode Kode barang.
     * @param nama Nama barang.
     * @param tipe Tipe barang.
     * @param isBeraturan Integer that representing the status barang is beraturan or not.
     */
    public Barang(String kode, String nama, String tipe, int isBeraturan)
    {
        this.kode = kode;
        this.nama = nama;
        this.tipe = tipe;
        this.isBeraturan = isBeraturan;
    }

    /**
     * There is a form of the constructor that are called when the object of barang is created
     * with the specified parcel.
     *
     * @param parcel Parcel barang.
     */
    public Barang (Parcel parcel)
    {
        this.kode = parcel.readString();
        this.nama = parcel.readString();
        this.tipe = parcel.readString();
        this.isBeraturan = parcel.readInt();
    }

    /**
     * Gets kode barang.
     *
     * @return Return kode barang.
     */
    public String getKode()
    {
        return kode;
    }

    /**
     * Sets kode barang with the specified parameter.
     */
    public void setKode(String kode)
    {
        this.kode = kode;
    }

    /**
     * Gets nama barang.
     *
     * @return Return nama barang.
     */
    public String getNama()
    {
        return nama;
    }

    /**
     * Sets nama barang with the specified parameter.
     */
    public void setNama(String nama)
    {
        this.nama = nama;
    }

    /**
     * Gets tipe barang.
     *
     * @return Return tipe barang.
     */
    public String getTipe()
    {
        return tipe;
    }

    /**
     * Sets tipe barang with the specified parameter.
     */
    public void setTipe(String tipe)
    {
        this.tipe = tipe;
    }

    /**
     * Gets status barang, beraturan or not.
     *
     * @return Return status barang that representing barang is beraturan or not.
     */
    public int getIsBeraturan()
    {
        return isBeraturan;
    }

    /**
     * Sets status barang with the specified parameter, 1 for beraturan and 0 for not.
     */
    public void setIsBeraturan(int isBeraturan)
    {
        this.isBeraturan = isBeraturan;
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param o The reference object with which to compare.
     * @return Return true if this object is the same as the obj argument, false otherwise.
     */
    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Barang)) return false;

        Barang barang = (Barang) o;

        if (isBeraturan != barang.isBeraturan) return false;
        if (!kode.equals(barang.kode)) return false;
        if (!nama.equals(barang.nama)) return false;
        return tipe.equals(barang.tipe);
    }

    /**
     * Describe the kinds of special objects contained in this Parcelable instance's marshaled representation.
     *
     * @return Return a bitmask indicating the set of special object types marshaled by this Parcelable object instance.
     */
    @Override
    public int describeContents()
    {
        return 0;
    }

    /**
     * Flatten this object in to a Parcel.
     *
     * @param dest The Parcel in which the object should be written.
     * @param flags Additional flags about how the object should be written. May be 0 or PARCELABLE_WRITE_RETURN_VALUE.
     */
    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(kode);
        dest.writeString(nama);
        dest.writeString(tipe);
        dest.writeInt(isBeraturan);
    }

    public static Parcelable.Creator<Barang> CREATOR = new Creator<Barang>()
    {
        @Override
        public Barang createFromParcel(Parcel source)
        {
            return new Barang(source);
        }

        @Override
        public Barang[] newArray(int size)
        {
            return new Barang[size];
        }
    };
}