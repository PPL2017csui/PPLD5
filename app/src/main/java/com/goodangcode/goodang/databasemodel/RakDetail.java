package com.goodangcode.goodang.databasemodel;


/**
 * Class Rak Detail is the model of database.
 *
 * @author GoodangCode
 */
public class RakDetail
{
    private String kode;
    private String digit_alfabet;
    private String digit_vertikal;
    private String digit_horizontal;

    /**
     * There is a form of the constructor that are called when the object of rak detail is created
     * with the specified parameter.
     *
     * @param kode Kode rak detail.
     * @param digit_alfabet Digit that represents rak position.
     * @param digit_vertikal Digit that represents rak position from up to down.
     * @param digit_horizontal Digit that represents rak position from left to right.
     */
    public RakDetail(String kode, String digit_alfabet,
                     String digit_vertikal, String digit_horizontal)
    {
        this.kode = kode;
        this.digit_alfabet = digit_alfabet;
        this.digit_vertikal = digit_vertikal;
        this.digit_horizontal = digit_horizontal;
    }

    /**
     * Gets kode rak.
     *
     * @return Return kode rak.
     */
    public String getKode()
    {
        return kode;
    }

    /**
     * Sets kode rak with the specified parameter.
     */
    public void setKode(String kode)
    {
        this.kode = kode;
    }

    /**
     * Gets digit that represents rak position.
     *
     * @return Return rak position.
     */
    public String getDigitAlfabet()
    {
        return digit_alfabet;
    }

    /**
     * Sets alphabet digit with the specified parameter.
     */
    public void setDigitAlfabet(String digit_alfabet)
    {
        this.digit_alfabet = digit_alfabet;
    }

    /**
     * Gets digit that represents rak position from up to down.
     *
     * @return Return rak position.
     */
    public String getDigitVertikal()
    {
        return digit_vertikal;
    }

    /**
     * Sets vertical digit with the specified parameter.
     */
    public void setDigitVertikal(String digit_vertikal)
    {
        this.digit_vertikal = digit_vertikal;
    }

    /**
     * Gets digit that represents rak position from left to right.
     *
     * @return Return rak position.
     */
    public String getDigitHorizontal()
    {
        return digit_horizontal;
    }

    /**
     * Sets horizontal digit with the specified parameter.
     */
    public void setDigitHorizontal(String digit_horizontal)
    {
        this.digit_horizontal = digit_horizontal;
    }
}
