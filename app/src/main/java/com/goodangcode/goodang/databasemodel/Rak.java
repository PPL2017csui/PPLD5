package com.goodangcode.goodang.databasemodel;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Class Rak is the model of database.
 *
 * @author GoodangCode
 */
public class Rak implements Parcelable
{
    private String kode;
    private String kodeGudang;
    private String tipe;
    private int statusPenuh;
    private int sinkronisasi;

    /**
     * There is a form of the constructor that are called when the object of rak is created.
     */
    public Rak()
    {
        this(null, null, null, 0);
    }

    /**
     * There is a form of the constructor that are called when the object of rak is created
     * with the specified parameter.
     *
     * @param kode Kode rak.
     * @param kodeGudang Kode gudang.
     * @param tipe Tipe rak.
     * @param statusPenuh Integer status penuh, 1 penuh, 0 otherwise.
     */
    public Rak(String kode, String kodeGudang, String tipe, int statusPenuh)
    {
        this.kode = kode;
        this.kodeGudang = kodeGudang;
        this.tipe = tipe;
        this.statusPenuh = statusPenuh;
        this.sinkronisasi = 0;
    }

    /**
     * There is a form of the constructor that are called when the object of rak is created
     * with the specified parcel.
     *
     * @param parcel Parcel rak.
     */
    public Rak (Parcel parcel)
    {
        this.kode = parcel.readString();
        this.kodeGudang = parcel.readString();
        this.tipe = parcel.readString();
        this.statusPenuh = parcel.readInt();
        this.sinkronisasi = parcel.readInt();
    }

    /**
     * Gets kode rak.
     *
     * @return Return kode rak.
     */
    public String getKode()
    {
        return kode;
    }

    /**
     * Sets kode rak with the specified parameter.
     */
    public void setKode(String kode)
    {
        this.kode = kode;
    }

    /**
     * Gets kode gudang.
     *
     * @return Return kode gudang.
     */
    public String getKodeGudang()
    {
        return kodeGudang;
    }

    /**
     * Sets kode rak with the specified parameter.
     */
    public void setKodeGudang(String kodeGudang)
    {
        this.kodeGudang = kodeGudang;
    }

    /**
     * Gets tipe rak.
     *
     * @return Return tipe rak.
     */
    public String getTipe()
    {
        return tipe;
    }

    /**
     * Sets tipe rak with the specified parameter.
     */
    public void setTipe(String tipe)
    {
        this.tipe = tipe;
    }

    /**
     * Gets status penuh rak.
     *
     * @return Return status penuh rak.
     */
    public int getStatusPenuh()
    {
        return statusPenuh;
    }

    /**
     * Sets status penuh rak with the specified parameter.
     */
    public void setStatusPenuh(int statusPenuh)
    {
        this.statusPenuh = statusPenuh;
    }

    /**
     * Gets the flag of sinkronisasi.
     *
     * @return Return the flag of sinkronisasi.
     */
    public int getSinkronisasi()
    {
        return sinkronisasi;
    }

    /**
     * Sets the flag of sinkronisasi with the specified parameter.
     */
    public void setSinkronisasi(int sinkronisasi)
    {
        this.sinkronisasi = sinkronisasi;
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param o The reference object with which to compare.
     * @return Return true if this object is the same as the obj argument, false otherwise.
     */
    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Rak)) return false;

        Rak rak = (Rak) o;

        if (statusPenuh != rak.statusPenuh) return false;
        if (!kode.equals(rak.kode)) return false;
        if (!kodeGudang.equals(rak.kodeGudang)) return false;

        return tipe.equals(rak.tipe);
    }

    /**
     * Describe the kinds of special objects contained in this Parcelable instance's marshaled representation.
     *
     * @return Return a bitmask indicating the set of special object types marshaled by this Parcelable object instance.
     */
    @Override
    public int describeContents()
    {
        return 0;
    }

    /**
     * Flatten this object in to a Parcel.
     *
     * @param dest The Parcel in which the object should be written.
     * @param flags Additional flags about how the object should be written. May be 0 or PARCELABLE_WRITE_RETURN_VALUE.
     */
    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(kode);
        dest.writeString(kodeGudang);
        dest.writeString(tipe);
        dest.writeInt(statusPenuh);
        dest.writeInt(sinkronisasi);
    }

    public static Creator<Rak> CREATOR = new Creator<Rak>()
    {
        @Override
        public Rak createFromParcel(Parcel source)
        {
            return new Rak(source);
        }

        @Override
        public Rak[] newArray(int size)
        {
            return new Rak[size];
        }
    };
}
