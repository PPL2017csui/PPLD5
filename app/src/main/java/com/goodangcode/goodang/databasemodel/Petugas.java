package com.goodangcode.goodang.databasemodel;


/**
 * Class Petugas is the model of database.
 *
 * @author GoodangCode
 */
public class Petugas
{
    private int kode;
    private String nama;
    private int isAdmin;
    private String username;
    private String kataSandi;
    private String kodeGudang;

    /**
     * There is a form of the constructor that are called when the object of petugas is created.
     */
    public Petugas()
    {
        this(0, null, 0, null, null, null);
    }

    /**
     * There is a form of the constructor that are called when the object of petugas is created
     * with the specified parameter.
     *
     * @param kode Kode petugas.
     * @param nama Nama petugas.
     * @param username Username petugas.
     * @param kataSandi Kata sandi petugas.
     * @param kodeGudang Kode gudang where petugas is placed.
     */
    public Petugas(int kode, String nama, int isAdmin, String username, String kataSandi, String kodeGudang)
    {
        this.kode = kode;
        this.nama = nama;
        this.isAdmin = isAdmin;
        this.username = username;
        this.kataSandi = kataSandi;
        this.kodeGudang = kodeGudang;
    }

    /**
     * Gets kode petugas.
     *
     * @return Kode petugas.
     */
    public int getKode()
    {
        return kode;
    }

    /**
     * Sets kode petugas with the specified parameter.
     *
     * @param kode Kode petugas.
     */
    public void setKode(int kode)
    {
        this.kode = kode;
    }

    /**
     * Gets nama petugas.
     *
     * @return Nama petugas.
     */
    public String getNama()
    {
        return nama;
    }

    /**
     * Sets nama petugas with the specified parameter.
     *
     * @param nama Nama petugas.
     */
    public void setNama(String nama)
    {
        this.nama = nama;
    }

    /**
     * Gets status admin.
     *
     * @return Status admin.
     */
    public int getIsAdmin()
    {
        return isAdmin;
    }

    /**
     * Sets status admin with the specified parameter.
     *
     * @param isAdmin Status admin.
     */
    public void setIsAdmin(int isAdmin)
    {
        this.isAdmin = isAdmin;
    }

    /**
     * Gets username petugas.
     *
     * @return Username petugas.
     */
    public String getUsername()
    {
        return username;
    }

    /**
     * Sets username petugas with the specified parameter.
     *
     * @param username Username petugas.
     */
    public void setUsername(String username)
    {
        this.username = username;
    }

    /**
     * Gets kata sandi petugas.
     *
     * @return Kata sandi petugas.
     */
    public String getKataSandi()
    {
        return kataSandi;
    }

    /**
     * Sets kata sandi petugas with the specified parameter.
     *
     * @param kataSandi Kata sandi petugas.
     */
    public void setKataSandi(String kataSandi)
    {
        this.kataSandi = kataSandi;
    }

    /**
     * Gets kode gudang.
     *
     * @return Kode gudang where petugas is placed.
     */
    public String getKodeGudang()
    {
        return kodeGudang;
    }

    /**
     * Sets kode gudang with the specified parameter.
     *
     * @param kodeGudang Kode gudang where petugas is placed.
     */
    public void setKodeGudang(String kodeGudang)
    {
        this.kodeGudang = kodeGudang;
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param o The reference object with which to compare.
     * @return Return true if this object is the same as the obj argument, false otherwise.
     */
    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Petugas)) return false;

        Petugas petugas = (Petugas) o;

        if (kode != petugas.kode) return false;
        if (!nama.equals(petugas.nama)) return false;
        if (!username.equals(petugas.username)) return false;
        if (!kataSandi.equals(petugas.kataSandi)) return false;
        return kodeGudang.equals(petugas.kodeGudang);

    }
}
