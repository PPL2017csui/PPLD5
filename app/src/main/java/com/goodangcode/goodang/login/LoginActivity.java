package com.goodangcode.goodang.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.goodangcode.goodang.MainActivity;
import com.goodangcode.goodang.MySingleton;
import com.goodangcode.goodang.NetworkMonitor;
import com.goodangcode.goodang.R;
import com.goodangcode.goodang.SessionManager;
import com.goodangcode.goodang.databasehelper.DatabaseController;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * Login - Activity related to user login.
 *
 * @author GoodangCode
 */
public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";

    EditText namaPenggunaText;
    EditText kataSandiText;
    Button loginButton;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        namaPenggunaText = (EditText) findViewById(R.id.input_nama_pengguna);
        kataSandiText = (EditText) findViewById(R.id.input_kata_sandi);
        loginButton = (Button) findViewById(R.id.btn_login);

        loginButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                login();
            }
        });

        pd = new ProgressDialog(LoginActivity.this);
        pd.setMessage("Autentikasi...");
    }

    /**
     * Login by username and password
     * Validate username and password
     * Check to server
     */
    public void login() {
        Log.d(TAG, "Login");

        if (!validate()) {
            onLoginFailed();
            return;
        }

        pd.show();
        loginButton.setEnabled(false);

        String namaPengguna = namaPenggunaText.getText().toString();
        String kataSandi = kataSandiText.getText().toString();

        if (NetworkMonitor.checkNetworkConnection(getApplicationContext())) {
            loginToServer(namaPengguna, kataSandi);
        } else {
            Toast.makeText(LoginActivity.this, R.string.error_NET01, Toast.LENGTH_SHORT).show();
            onLoginFailed();
            pd.dismiss();
            return;
        }
    }

    /**
     * Enable Button and start activity to MainActivity
     */
    public void onLoginSuccess() {
        loginButton.setEnabled(true);
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        finish();
        startActivity(intent);
    }

    /**
     * Enable Button
     */
    public void onLoginFailed() {
        loginButton.setEnabled(true);
    }

    /**
     * Validate EditText namaPengguna and kataSandi
     * If empty, show a warning
     * @return Boolean valid
     */
    public boolean validate() {
        boolean valid = true;

        String namaPengguna = namaPenggunaText.getText().toString();
        String kataSandi = kataSandiText.getText().toString();

        try{
            if (namaPengguna.isEmpty()) {
                valid = false;
                throw new Exception();
            } else {
                namaPenggunaText.setError(null);
            }
        }
        catch (Exception e){
            namaPenggunaText.setError("Masukkan Nama Pengguna!");
        }


        try{
            if (kataSandi.isEmpty()) {
                valid = false;
                throw new Exception();
            } else {
                kataSandiText.setError(null);
            }
        }
        catch (Exception e){
            kataSandiText.setError("Masukkan Kata Sandi!");
        }


        return valid;
    }

    /**
     * check to server
     * @param namaPengguna
     * @param kataSandi
     */
    private void loginToServer(String namaPengguna, String kataSandi)
    {
        JSONObject user = new JSONObject();

        try {
            user.put("username",namaPengguna);
            user.put("kata_sandi",kataSandi);
        }
        catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(LoginActivity.this, e.toString(), Toast.LENGTH_LONG).show();
        }

        final String requestBody = user.toString();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                DatabaseController.SERVER_URL + "/login",
                new Response.Listener<String>()
                {
                    /**
                     * Called when a response is received.
                     *
                     * @param bodyResponse
                     */
                    @Override
                    public void onResponse(String bodyResponse)
                    {
                        JSONObject jsonObject = null;
                        JSONObject userJSON = null;
                        String response= "";
                        String message ="";

                        try {
                            jsonObject = new JSONObject(bodyResponse);
                            response = jsonObject.getString("response");
                            message = jsonObject.getString("message");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (response.equals("OK")) {
                            try {
                                userJSON = jsonObject.getJSONObject("user");
                                SessionManager.getInstance(getApplicationContext()).createLoginSession(userJSON.getString("username"), userJSON.getString("nama"), userJSON.getString("kode"), userJSON.getString("isadmin"), userJSON.getString("kode_gudang"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            onLoginSuccess();

                        } else {
                            onLoginFailed();
                        }
                        pd.dismiss();
                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener()
        {
            /**
             * Callback method that an error has been occurred with the provided error code and optional user-readable message.
             *
             * @param error
             */
            @Override
            public void onErrorResponse(VolleyError error)
            {
                error.printStackTrace();
                pd.dismiss();
                Toast.makeText(LoginActivity.this, R.string.error_NET01, Toast.LENGTH_LONG).show();
                onLoginFailed();
            }
        }
        )
        {
            /**
             * Gets the content type of body.
             *
             * @return Returns the body content type.
             */
            @Override
            public String getBodyContentType()
            {
                return "application/json; charset=utf-8";
            }

            /**
             * Gets the raw body.
             *
             * @return Returns the raw POST or PUT body to be sent.
             * @throws AuthFailureError
             */
            @Override
            public byte[] getBody() throws AuthFailureError
            {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };

        MySingleton.getInstance(LoginActivity.this).addToRequestQue(stringRequest);
    }
}
